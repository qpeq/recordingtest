*** Settings ***
Documentation   Here are the keywords for verifying video frame data of recording clip.
...             It is needed to import your own library: VASTController, VideoParser in your suite or test case.
Library         OperatingSystem
Library         robot.libraries.DateTime
Library         String

*** Variables ***
${test_verification_time_increment}             0
${save_file_list}                               ${False}
${view_while_recording}                         ${False}

*** Keywords ***
Get Recording Folder Name
    ${date} =           Get Current Date        result_format=%Y-%m-%d
    Set Suite Variable   ${rec_folder_date}      ${date}

Get Long-term Recording Folder Name
    ${date} =           Get Current Date        result_format=%Y-%m-%d          increment=${test_verification_time_increment}
    Set Test Variable   ${rec_folder_date}      ${date}

Recording Path "${storage_gp_rec_path}" Should Be Empty
    ${rec_path} = 	                Join Path           ${storage_gp_rec_path}          ${rec_folder_date}
    log     ${rec_path}   console=yes
    Directory Should Not Exist      ${rec_path}

Get "${camera}" GAEA DB Path In "${storage_gp_rec_path}"
    ${gaea_db_path} =       Join Path                       ${storage_gp_rec_path}      ${rec_folder_date}
    ...                     ${camera}                       ${rec_folder_date}.db
    Log                     GAEA DB PATH: ${gaea_db_path}   console=yes
    [Return]                ${gaea_db_path}

"${camera}" In "${storage_gp_rec_path}" Video Clip "${clip_num}" Duration Should Be "${duration}" Second And Continuous With "${frequency}"
    ${duration_with_err_range} =    Evaluate                ${duration} - ${rec_time_err_range}
    Run Keyword If                  ${duration} != -1       Set Test Variable    ${adjust_duration}     ${duration_with_err_range}
    ...                             ELSE                    Set Test Variable    ${adjust_duration}     ${duration}
    ${video_path} =                 Join Path               ${storage_gp_rec_path}                      ${rec_folder_date}
    ...                             ${camera}
    Run Keyword                     ${remote_video_parser_lib}.Set File List    ${video_path}           ${clip_num}
    ${result} =                     Run Keyword                     ${remote_video_parser_lib}.Is Video Frame Expected
    ...                             duration=${adjust_duration}     frequency=${frequency}              save_file_list=${save_file_list}            view_while_recording=${view_while_recording}
    Should Be True                  ${result}

Video File "@{file_list}" Duration Should Be "${duration}" And Continuous With "${frequency}"
    ${interval_duration_list} =     Split String            ${duration}
    :FOR                            ${idx}                  IN RANGE            0                       ${time_interval_cnt}
    \                               log   File list: ${file_list[${idx}]}       console=yes
    \                               log   Duration: ${interval_duration_list[${idx}]}                   console=yes
    \                               Run Keyword                     ${remote_video_parser_lib}.Set Given File List          @{file_list}[${idx}]
    \                               @{one_file_list} =              Split String        @{file_list}[${idx}]
    \                               ${file_count} =                 Get Length          ${one_file_list}
    \                               ${duration_with_err_range} =    Evaluate            @{interval_duration_list}[${idx}] - ${file_count}
    \                               Run Keyword If                  @{interval_duration_list}[${idx}] != -1                 Set Test Variable           ${adjust_duration}      ${duration_with_err_range}
    ...                             ELSE                            Set Test Variable           ${adjust_duration}          ${-1}
    \                               ${result} =                     Run Keyword         ${remote_video_parser_lib}.Is Video Frame Expected
    ...                             duration=${adjust_duration}     frequency=${frequency}                  save_file_list=${save_file_list}            view_while_recording=${view_while_recording}
    \                               Should Be True                              ${result}

"${camera}" In "${storage_gp_rec_path}" Video Clip "${clip_num}" Video Codec Should Be "${video_codec}"
    ${video_path} =     Join Path               ${storage_gp_rec_path}                  ${rec_folder_date}
    ...                 ${camera}
    Run Keyword         ${remote_video_parser_lib}.Set File List       ${video_path}    ${clip_num}
    ${result} =         Run Keyword             ${remote_video_parser_lib}.Is Video Codec Expected     ${video_codec}
    Should Be True      ${result}

"${camera}" In Many Path "${many_rec_path}" Video Clip Duration Should Be "${duration}" Second And Continuous With "${frequency}"
    ${duration_with_err_range} =        Evaluate                ${duration} - ${rec_time_err_range}
    Run Keyword If                      ${duration} != -1       Set Test Variable               ${adjust_duration}      ${duration_with_err_range}
    ...                                 ELSE                    Set Test Variable               ${adjust_duration}      ${duration}
    ${rec_path} =                       Split String            ${many_rec_path}
    ${1st_video_path} =                 Join Path               ${rec_path[0]}                  ${rec_folder_date}
    ...                                 ${camera}
    ${2nd_video_path} =                 Join Path               ${rec_path[1]}                  ${rec_folder_date}
    ...                                 ${camera}
    Run Keyword                         ${remote_video_parser_lib}.Set Specific File List       ${1st_video_path} ${2nd_video_path}
    ${result} =                         Run Keyword             ${remote_video_parser_lib}.Is Video Frame Expected
    ...                                 duration=${adjust_duration}     frequency=${frequency}
    Should Be True                      ${result}

VAST "${rec_path}" Should Not Exist
    ${is_rec_path_exist} =      Run Keyword                 ${remote_vast_controller_lib}.Is VAST Rec Path Exist          ${rec_path}
    Should Not Be True          ${is_rec_path_exist}        RecPathDeleteError: recording path "${rec_path}" should not exists but still there

Recording Data In "${rec_data_path}" Should Exist
    ${is_rec_path_exist} =      Run Keyword                 ${remote_vast_controller_lib}.Is VAST Rec Path Exist          ${rec_data_path}
    Should Be True              ${is_rec_path_exist}        RecPathDeleteError: recording path "${rec_data_path}" should exists but not
