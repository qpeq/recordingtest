*** Settings ***
Documentation   Here are the keywords for verifying database value of GAEA.db and VAST.db.
...             It is needed to import your own library: VASTDBUtility, GAEADBUtiliy in your suite or test case.
Resource        VideoDataVerification.robot
Library         Collections
Library         String
Variables       ../TestLibrary/Error.py

*** Variables ***
${rec_mode_none}                ${0}
${rec_mode_continuous}          ${1}
${rec_mode_event}               ${2}
${tag_pir}                      ${2}
${tag_tampering}                ${3}
${tag_pptz_trigger}             ${4}
${tag_stop_until_di_falling}    ${5}
${tag_stop_until_di_rising}     ${6}
${tag_line_detection}           ${7}
${tag_field_detection}          ${8}
${di_trigger_state_value}       ${1}
${di_normal_state_value}        ${2}
${single_day_type}              ${1}
${period_day_type}              ${2}
${daily_repeat_mode}            ${2}
${weekly_repeat_mode}           ${3}
${event_cnt_err_range}          ${0}

*** Keywords ***
Storage Group "${new_storage_gp}" Should Be Added
    ${row_cnt} =        Run Keyword         ${remote_vast_db_lib}.Get Row Count By Value
    ...                 RecordDBInfo        RecGroupName        ${new_storage_gp}
    Should Be Equal     ${1}                ${row_cnt}
    ...                 VASTDBError: storage gp "${new_storage_gp}" should be added in 'RecordDBInfo' table         values=False

Storage Group "${new_storage_gp}" Should Be Deleted
    ${row_cnt} =        Run Keyword         ${remote_vast_db_lib}.Get Row Count By Value
    ...                 RecordDBInfo        RecGroupName        ${new_storage_gp}
    Should Be Equal     ${0}                ${row_cnt}
    ...                 VASTDBError: storage gp "${new_storage_gp}" should be deleted in 'RecordDBInfo' table       values=False

"${storage_gp_id}" Recording Path "${rec_path}" Should Be Added
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Recording Path Info
    ...                 RecDBID=${storage_gp_id} and Path='${rec_path}'
    Length Should Be    ${result}           ${1}
    ...                 VASTDBError: recording path "${rec_path}" should be added in 'RecordPath' table

"${storage_gp_id}" Recording Path "${rec_path}" Should Be Deleted
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Recording Path Info
    ...                 RecDBID=${storage_gp_id} and Path='${rec_path}'
    Length Should Be    ${result}           ${0}
    ...                 VASTDBError: recording path "${rec_path}" should be deleted in 'RecordPath' table

"${storage_gp_id}" Recovery Path "${rec_path}" Should Be Added
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Recording Path Info
    ...                 RecDBID=${storage_gp_id} and Path='${rec_path}'
    ${len} =            Get Length                                  ${result}
    Should Be True      ${len}>0                                    VASTDBError: VAST DB is empty in 'RecordPath' table
    Should Be Equal     ${result[0]['RecoveryMode']}                ${1}
    ...                 VASTDBError: recovery path "${rec_path}" should be added in 'RecordPath' table              values=False

"${storage_gp_id}" Recording Path "${rec_path}" Should Be Reserved "${size}" Bytes
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Recording Path Info
    ...                 RecDBID=${storage_gp_id} and Path='${rec_path}'
    ${len} =            Get Length                                  ${result}
    Should Be True      ${len}>0                                    VASTDBError: VAST DB is empty in 'RecordPath' table
    Should Be Equal     ${result[0]['ReserveSpace']}                ${size}
    ...                 VASTDBError: recording path "${rec_path}" reserve space should be "${size}" but not "${result[0]['ReserveSpace']}" in 'RecordPath' table
    ...                 values=False

NAS Server "${nas_server}" Should Be Added
    ${result} =         Run Keyword     ${remote_vast_db_lib}.Get NAS Server Info       Host="${nas_server}"
    Length Should Be    ${result}       ${1}
    ...                 VASTDBError: NAS server should be added in 'NASServer' table

"${camera_name}" Recording Stream Index Should Be "${stream_idx}"
    ${result} =                 Run Keyword                         ${remote_vast_db_lib}.Get CameraMng Info    CamName='${camera_name}'
    ${rec_stream_idx} =         Convert To Integer                  ${stream_idx}
    ${len} =                    Get Length                          ${result}
    Should Be True              ${len}>0                            VASTDBError: VAST DB is empty in 'CameraMng' table
    Should Be Equal             ${result[0]['RecStream']}           ${rec_stream_idx}
    ...                         VASTDBError: camera "${camera_name}" recording stream index should be "${rec_stream_idx}" but not "${result[0]['RecStream']}" in 'CameraMng' table
    ...                         values=False

"${camera_name}" Pre/Post-Event Time Should Be "${event_time}"
    ${result} =         Run Keyword                     ${remote_vast_db_lib}.Get CameraMng Info    CamName='${camera_name}'
    ${event_time} =     Split String                    ${event_time}
    ${pre_event} =      Convert To Integer              ${event_time[0]}
    ${len} =            Get Length                      ${result}
    Should Be True      ${len}>0                        VASTDBError: VAST DB is empty in 'CameraMng' table
    Should Be Equal     ${result[0]['PreEvent']}        ${pre_event}
    ...                 VASTDBError: camera "${camera_name}" pre-event time should be "${pre_event}" but not "${result[0]['PreEvent']}" in 'CameraMng' table
    ...                 values=False
    ${post_event} =     Convert To Integer                  ${event_time[1]}
    Should Be Equal     ${result[0]['PostEvent']}           ${post_event}
    ...                 VASTDBError: camera "${camera_name}" post-event time should be "${post_event}" but not "${result[0]['PostEvent']}" in 'CameraMng' table
    ...                 values=False

"${camera_name}" Should Be Added To Storage Group "${storage_gp_id}"
    ${result} =         Run Keyword                         ${remote_vast_db_lib}.Get Channel Info      RefAlias='${camera_name}'
    ${gp_id} =          Convert To Integer                  ${storage_gp_id}
    ${len} =            Get Length                          ${result}
    Should Be True      ${len}>0                            VASTDBError: VAST DB is empty in 'ChannelInfo' table
    Should Be Equal     ${result[0]['RecDBID']}             ${gp_id}
    ...                 VASTDBError: camera "${camera_name}" storage group id should be "${storage_gp_id}" but not "${result[0]['RecDBID']}" in 'ChannelInfo' table
    ...                 values=False

"${camera_name}" Should Be Deleted From Storage Group "${storage_gp_id}"
    ${result} =         Run Keyword                         ${remote_vast_db_lib}.Get Channel Info
    ...                 RefAlias='${camera_name}' and RecDBID=${storage_gp_id}
    ${gp_id} =          Convert To Integer                  ${default_storage_gp_id}
    Length Should Be    ${result}           ${0}
    ...                 VASTDBError: camera "${camera_name}" should be deleted from storage group "${storage_gp_id}" in 'ChannelInfo' table

Schedule List "${schedule_list_name}" Should Be Added
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Timecard Info        TcName='${schedule_list_name}'
    Length Should Be    ${result}           ${1}
    ...                 VASTDBError: schedule list "${schedule_list_name}" should be added in 'ScheTimecard' table

Schedule List "${schedule_list_name}" Should Be Deleted
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Timecard Info        TcName='${schedule_list_name}'
    Length Should Be    ${result}           ${0}
    ...                 VASTDBError: schedule list "${schedule_list_name}" should be deleted in 'ScheTimecard' table

Timeframe ID "${time_frame_id}" Recording Mode Should Be "${rec_mode}"
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Rec Timeframe Info   TimeframeID=${time_frame_id}
    ${len} =            Get Length          ${result}
    Should Be True      ${len}>0            VASTDBError: VAST DB is empty in 'ScheRecTimeframe' table
    Should Be Equal     ${result[0]['RecMode']}                         ${rec_mode}
    ...                 VASTDBError: rec mode should be "${rec_mode}" but not "${result[0]['RecMode']}" in 'ScheRecTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" Motion Trigger Should Be "${mo_trigger}"
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Rec Timeframe Info   TimeframeID=${time_frame_id}
    ${len} =            Get Length          ${result}
    Should Be True      ${len}>0            VASTDBError: VAST DB is empty in 'ScheRecTimeframe' table
    Should Be Equal     ${result[0]['MoTrigger']}                       ${mo_trigger}
    ...                 VASTDBError: recording mode should be "${mo_trigger}" but not "${result[0]['MoTrigger']}" in 'ScheRecTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" Event Type Should Be "${event_type}"
    ${event_tag} =      Run Keyword         ${remote_vast_db_lib}.Get Rec Event Type                TimeframeID=${time_frame_id}
    Should Be Equal     ${event_tag[${event_type}]}                   ${1}
    ...                 VASTDBError: event type should be "${1}" but not "${event_tag[${event_type}]}" in 'ScheRecTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" DI Trigger Type Should Be "${di_trigger_type}"
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Rec Timeframe Info   TimeframeID=${time_frame_id}
    ${len} =            Get Length          ${result}
    Should Be True      ${len}>0            VASTDBError: VAST DB is empty in 'ScheRecTimeframe' table
    Should Be Equal     ${result[0]['DigiInput']}                       ${di_trigger_type}
    ...                 VASTDBError: DI trigger type should be "${di_trigger_type}" but not "${result[0]['DigiInput']}" in 'ScheRecTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" Recording Repeat Frequency Should Be Weekly Day-based
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Timeframe Info   TimeframeID=${time_frame_id}
    ${len} =            Get Length          ${result}
    Should Be True      ${len}>0            VASTDBError: VAST DB is empty in 'ScheTimeframe' table
    Should Be Equal     ${result[0]['DayLstType']}                  ${single_day_type}
    ...                 VASTDBError: timeframe day type should be "${single_day_type}" but not "${result[0]['DayLstType']}" in 'ScheTimeframe' table
    ...                 values=False
    Should Be Equal     ${result[0]['RepeatMode']}                  ${weekly_repeat_mode}
    ...                 VASTDBError: timeframe repeat mode should be "${weekly_repeat_mode}" but not "${result[0]['RepeatMode']}" in 'ScheTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" Recording Repeat Frequency Should Be Weekly Period Settings
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Timeframe Info   TimeframeID=${time_frame_id}
    ${len} =            Get Length          ${result}
    Should Be True      ${len}>0            VASTDBError: VAST DB is empty in 'ScheTimeframe' table
    Should Be Equal     ${result[0]['DayLstType']}                  ${period_day_type}
    ...                 VASTDBError: timeframe day type should be "${period_day_type}" but not "${result[0]['DayLstType']}" in 'ScheTimeframe' table
    ...                 values=False
    Should Be Equal     ${result[0]['RepeatMode']}                  ${weekly_repeat_mode}
    ...                 VASTDBError: timeframe repeat mode should be "${weekly_repeat_mode}" but not "${result[0]['RepeatMode']}" in 'ScheTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" Recording Repeat Frequency Should Be Daily Settings
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Timeframe Info   TimeframeID=${time_frame_id}
    ${len} =            Get Length          ${result}
    Should Be True      ${len}>0            VASTDBError: VAST DB is empty in 'ScheTimeframe' table
    Should Be Equal     ${result[0]['DayLstType']}                  ${single_day_type}
    ...                 VASTDBError: timeframe day type should be "${single_day_type}" but not "${result[0]['DayLstType']}" in 'ScheTimeframe' table
    ...                 values=False
    Should Be Equal     ${result[0]['RepeatMode']}                  ${daily_repeat_mode}
    ...                 VASTDBError: timeframe repeat mode should be "${daily_repeat_mode}" but not "${result[0]['RepeatMode']}" in 'ScheTimeframe' table
    ...                 values=False

Timeframe ID "${time_frame_id}" Recording Day Should Be "${day_list}"
    ${result} =         Run Keyword     ${remote_vast_db_lib}.Get Day Entry Info                    TimeframeID=${time_frame_id}
    ${day_list} =       Split String 	${day_list}
    ${len} =            Get Length      ${day_list}
    Length Should Be    ${result}       ${len}
    ...                 VASTDBError: # of timeframe recording day should be "${len}" in 'ScheDayEntryInfo' table
    :FOR                ${itm}          IN              @{result}
    \                   List Should Contain Value       ${day_list}         ${itm['StartDay']}
    ...                 VASTDBError: timeframe day entry should not contain "${itm['StartDay']}" in 'ScheDayEntryInfo' table

Timeframe ID "${time_frame_id}" Recording Time Interval Should Be "${time_interval}"
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Time Interval Info       TimeframeID=${time_frame_id}
    ${timeinterval} = 	Split String 	    ${time_interval}
    ${len} =            Get Length          ${timeinterval}
    ${index} =          Set Variable        ${0}
    Length Should Be    ${result}           ${len/2}
    ...                 VASTDBError: time interval count should be "${len/2}" in 'ScheTimeInterval' table
    :FOR                ${itm}              IN                                  @{result}
    \                   Should Be Equal     ${${timeinterval[${index}]}}        ${itm['StartTime']}
    ...                 VASTDBError: time interval start time should be "${itm['StartTime']}" but not "${${timeinterval[${index}]}}" in 'ScheTimeInterval' table
    ...                 values=False
    \                   ${index} =          Evaluate                            ${index}+${1}
    \                   Should Be Equal     ${${timeinterval[${index}]}}        ${itm['EndTime']}
    ...                 VASTDBError: time interval end time should be "${itm['EndTime']}" but not "${${timeinterval[${index}]}}" in 'ScheTimeInterval' table
    ...                 values=False
    \                   ${index} =          Evaluate                            ${index}+${1}

"${camera_channel_id}" Should Be Added To Schedule List "${time_card_id}"
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Rec Camera List Info   TimecardID=${time_card_id} and ChannelID=${camera_channel_id}
    Length Should Be    ${result}           ${1}
    ...                 VASTDBError: camera should be added to schedule list in 'ScheRecCamList' table

"${camera_channel_id}" Should Be Deleted From Schedule List "${time_card_id}"
    ${result} =         Run Keyword         ${remote_vast_db_lib}.Get Schedule Rec Camera List Info   TimecardID=${time_card_id} and ChannelID=${camera_channel_id}
    Length Should Be    ${result}           ${0}
    ...                 VASTDBError: camera should be deleted from schedule list in 'ScheRecCamList' table

"${cam_folder_name}" In "${storage_rec_path}" Recording Time Interval Should Be "${time_interval_expected}"
    Get Recording Folder Name
    Wait Until Keyword Succeeds             30 sec   0.5 sec
    ...                                     Until "${cam_folder_name}" In "\${storage_rec_path}" Recording Time Interval Should Be "${time_interval_expected}"

Until "${cam_folder_name}" In "${storage_rec_path}" Recording Time Interval Should Be "${time_interval_expected}"
    ${gaea_db_path} =       Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword             ${remote_gaea_db_lib}.Connect To Database       ${gaea_db_path}
    ${time_interval} =      Split String            ${time_interval_expected}
    ${len} =                Get Length              ${time_interval}
    "${gaea_db_path}" Time Interval Count Shoule Be "${len/2}"
    ${result} =             Run Keyword             ${remote_gaea_db_lib}.Get Time Interval Info
    ${date} =               Get Current Date        result_format=%Y-%m-%d          increment=${test_verification_time_increment}
    ${curr_date_epoch} =    Convert Date            ${date} 00:00:00                epoch
    ${index} =              Set Variable            ${0}
    :FOR                    ${itm}                  IN                      @{result}
    \                       ${s_time} =             Get Epoch Time          ${curr_date_epoch}      ${${time_interval[${index}]}}
    \                       ${e_time} =             Get Epoch Time          ${curr_date_epoch}      ${${time_interval[${index+1}]}}
    \                       Log                     Time Interval Settings: ${s_time} - ${e_time}               console=yes
    \                       Log                     GAEA DB Recording Time: ${itm['startrecordtime']} - ${itm['endrecordtime']}     console=yes
    \                       Should Be True          ${itm['startrecordtime']} <= ${s_time} + ${rec_time_err_range} < ${e_time} - ${rec_time_err_range} <= ${itm['endrecordtime']}
    ...                     GAEADBError: expected time interval should be "${s_time} + ${rec_time_err_range} ~ ${e_time} - ${rec_time_err_range}" but gaea db time interval is "${itm['startrecordtime']} ~ ${itm['endrecordtime']}" in 'timeinterval' table
    \                       ${index} =              Evaluate                ${index}+${2}
    \                       Run Keyword If 	        ${len/2} <= ${index}    Exit For Loop  # sometimes gaea db may record more 10 seconds since yesterday time schedule start time is from 00:00

"${cam_folder_name}" In "${storage_rec_path_list}" Recording Time Interval Should Be Continuous Within "${time_interval_expected}"
    ${rec_path} =               Split String                    ${storage_rec_path_list}
    ${gaea_db_path_A} =         Get "${cam_folder_name}" GAEA DB Path In "${rec_path[0]}"
    ${time_interval_A} =        Get Recording Time From GAEA DB For One Continuous Recording Time Interval      ${gaea_db_path_A}
    ${gaea_db_path_B} =         Get "${cam_folder_name}" GAEA DB Path In "${rec_path[1]}"
    ${time_interval_B} =        Get Recording Time From GAEA DB For One Continuous Recording Time Interval      ${gaea_db_path_B}
    ${time_interval} =          Split String                    ${time_interval_expected}
    ${date} =                   Get Current Date                result_format=%Y-%m-%d          increment=${test_verification_time_increment}
    ${curr_date_epoch} =        Convert Date                    ${date} 00:00:00                epoch
    ${s_time} =                 Get Epoch Time                  ${curr_date_epoch}              ${${time_interval[0]}}
    ${e_time} =                 Get Epoch Time                  ${curr_date_epoch}              ${${time_interval[1]}}
    ${time_interval_A} =        Split String                    ${time_interval_A}
    ${time_interval_B} =        Split String                    ${time_interval_B}
    Should Be True              ${time_interval_A[1]} <= ${time_interval_B[0]} <= ${time_interval_A[1]} + ${rec_time_err_range}
    ...                         GAEADBError: recording end time in rec path "${rec_path[0]}" should be equal to recording start time in rec path "${rec_path[1]}"
    Should Be True              ${time_interval_A[0]} <= ${s_time} + ${rec_time_err_range}
    ...                         GAEADBError: recording start time "${s_time} + ${rec_time_err_range}" should be equal or later to gaea db start time "${time_interval_A[0]}"
    Should Be True              ${e_time} - ${rec_time_err_range} <= ${time_interval_B[1]}
    ...                         GAEADBError: recording end time "${e_time} - ${rec_time_err_range}" should be equal or early to gaea db end time "${time_interval_B[1]}"

"${cam_folder_name}" In "${storage_rec_path}" Event Conunt Should Be "${count}"
    ${gaea_db_path} =           Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword                 ${remote_gaea_db_lib}.Connect To Database          ${gaea_db_path}
    ${result} =                 Run Keyword                 ${remote_gaea_db_lib}.Get Row Count By Table        Event
    Set Test Variable           ${event_cnt_in_gaea_db}     ${result}
    Should Be True              ${count} - ${event_cnt_err_range} <= ${result} <= ${count} + ${event_cnt_err_range}
    ...                         GAEADBError: event count sould be "${count}" but not "${result}" in 'event' table

"${cam_folder_name}" In "${storage_rec_path}" "${event_serial}" Event ID Should Be "${event_id}"
    ${gaea_db_path} =           Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword                 ${remote_gaea_db_lib}.Connect To Database   ${gaea_db_path}
    ${result} =                 Run Keyword         ${remote_gaea_db_lib}.Get Event Info            eventserial=${event_serial}
    ${len} =                    Get Length          ${result}
    Should Be True              ${len}>0            GAEADBError: GAEA DB is empty in 'event' table
    Should Be Equal             ${result[0]['eventid']}                               ${event_id}
    ...                         GAEADBError: event id should be "${event_id}" but not "${result[0]['eventid']}" in 'event' table
    ...                         values=False

"${cam_folder_name}" In "${storage_rec_path}" Each Recording Time Duration Should Be "${count}" sec
    ${gaea_db_path} =           Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword                 ${remote_gaea_db_lib}.Connect To Database   ${gaea_db_path}
    ${result} =                 Run Keyword         ${remote_gaea_db_lib}.Get Time Interval Info
    :FOR                        ${itm}                  IN                      @{result}
    \                           ${difference_time} =    Evaluate                ${itm['endrecordtime']} - ${itm['startrecordtime']}
    \                           Should Be True          ${difference_time} >= ${count} - ${rec_time_err_range}
    ...                         GAEADBError: event recording time duration should be "${count}" seconds but not "${difference_time}" in 'timeinterval' table

Get Recording Time From GAEA DB For One Continuous Recording Time Interval
    [Arguments]                 ${gaea_db_path}
    Run Keyword                 ${remote_gaea_db_lib}.Connect To Database   ${gaea_db_path}
    "${gaea_db_path}" Time Interval Count Shoule Be "${1}"
    ${result} =                 Run Keyword         ${remote_gaea_db_lib}.Get Time Interval Info
    [Return]                    ${result[0]['startrecordtime']} ${result[0]['endrecordtime']}

Get Epoch Time
    [Arguments]                 ${curr_epoch}               ${duration_sec}
    ${rec_epoch_time} =         Add Time To Time            ${curr_epoch}           ${duration_sec}
    ${epoch_time} =             Convert To Integer          ${rec_epoch_time}
    [Return]                    ${epoch_time}

"${db_path}" Time Interval Count Shoule Be "${count_expected}"
    ${count} =                  Run Keyword                 ${remote_gaea_db_lib}.Get Row Count By Table    timeinterval
    Should Be Equal             ${count}                    ${count_expected}
    ...                         GAEADBError: time interval count should be "${count_expected}" but not "${count}" in 'timeinterval' table
    ...                         values=False

"${camera_recording}" Should Be Recorded When Motion Trigger "${camera_trigger}" In "${alarm_tmcard_id}"
    ${result} =                 Run Keyword                 ${remote_vast_db_lib}.Get Schedule Event Trigger Info
    ...                         TimecardID=${alarm_tmcard_id}
    ${len} =                    Get Length                  ${result}
    Should Be True              ${len}>0                    VASTDBError: VAST DB is empty in 'ScheEvtTrigger' table
    Should Be Equal             ${result[0]['DeviceName']}                          ${camera_trigger}
    ...                         VASTDBError: event trigger camera should be "${camera_trigger}" but not "${result[0]['DeviceName']}" in 'ScheEvtTrigger' table
    ...                         values=False
    ${result} =                 Run Keyword         ${remote_vast_db_lib}.Get Alarm Related Device Info
    ...                         TimecardID=${alarm_tmcard_id}
    Should Be Equal             ${result[0]['Device']}                              ${camera_recording}
    ...                         VASTDBError: recording camera should be "${camera_recording}" but not "${result[0]['Device']}" in 'AlarmRelatedDevice' table
    ...                         values=False

Get All Interval Recording Video Files List
    [Arguments]                 ${interval_cnt}         ${storage_rec_path}         ${cam_folder_name}
    ${gaea_db_path} =           Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword                 ${remote_gaea_db_lib}.Connect To Database           ${gaea_db_path}
    @{file_list} =              Run Keyword             ${remote_gaea_db_lib}.Get All Interval Recording Video File List
    ...                         ${interval_cnt}         ${storage_rec_path}\\${rec_folder_date}     ${cam_folder_name}
    [Return]                    @{file_list}

Get "${cam_folder_name}" In "${storage_rec_path}" Video Clip Count
    ${gaea_db_path} =           Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword                 ${remote_gaea_db_lib}.Connect To Database           ${gaea_db_path}
    ${count} =                  Run Keyword             ${remote_gaea_db_lib}.Get Row Count By Table         mediafilediskmap
    [Return]                    ${count}

Connect To VAST Database
    Run Keyword                 ${remote_vast_db_lib}.Connect To Database

Disconnect To VAST Database
    Run Keyword                 ${remote_vast_db_lib}.Disconnect To Database

Disconnect To Database
    Run Keyword                 ${remote_vast_db_lib}.Disconnect To Database
    Run Keyword                 ${remote_gaea_db_lib}.Disconnect To Database