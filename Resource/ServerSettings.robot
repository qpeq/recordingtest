*** Settings ***
Library                             Process
Library                             OperatingSystem
Library                             String
Variables                           ../TestLibrary/RemoteServerSettings.py

*** Variable ***
${VAST_SETUP_PATH}                  \\\\172.18.101.4\\upload\\RecordingServerTest\\VAST\\VAST_setup.exe

${default_storage_gp_id}            1
${default_storage_rec_path}         D:\\recording
${temp_storage_rec_path}            D:\\temp_rec
${new_storage_gp}                   New Storage Group
${new_storage_rec_path}             C:\\recording
${new_storage_gp_id}                3

${nas_host}                         172.18.141.7
${nas_host_domain}                  172.18.141.7
${nas_user}                         working station
${nas_psw}                          7061737377307264
${nas_path}                         \\\\${nas_host}\\shareA\\recording

${camera_ip}                        127.0.0.1
${cam_name_prefix}                  c_
${first_camera_channel_id}          1
${cam_name}                         IP8330
${cam_ref_name}                     ${cam_name_prefix}${cam_name}

${default_time_card_id}             1
${default_time_frame_id}            2
${time_frame_name}                  TmFrame

${gaea_event_motion_id}             ${1}
${di_stop_until_rising_id}          ${11}


${wait_for_recording_timeout}       5 min
${retry_interval}                   5 sec
${wait_for_db_query}                1 min
${db_retry_interval}                1 sec

${recording_duration}               60
${frame_frequency}                  0.04    # 1/30
${event_frame_frequency}            0.08    # 1/15

${default_motion_recording_time}    20
${default_post_event_time}          10
${lt_general_storage_rec_path}      E:\\recording

${camera_1_rec_name}                1-IP8330-9001
${camera_2_rec_name}                2-IP8330-9002
${camera_3_rec_name}                3-IP8330-9003
${camera_4_rec_name}                4-IP8330-9004
${camera_5_rec_name}                5-IP8330-9005
${camera_6_rec_name}                6-IP8330-9006
${camera_7_rec_name}                7-IP8330-9007
${camera_8_rec_name}                8-IP8330-9008
${real_cam_1}                       9-CAM_IP8330_1
${real_cam_2}                       10-CAM_IP8330_2
${real_cam_3}                       11-CAM_IP8162_1
${real_cam_4}                       12-CAM_FE8182_1

${rec_time_err_range}               ${3}                # unit: seconds

${remote_vast_db_lib}               VASTDBUtility
${remote_gaea_db_lib}               GAEADBUtility
${remote_vast_controller_lib}       VASTController
${remote_video_parser_lib}          VideoParser

*** Keywords ***
Copy Recording Data If Test Failed
    [Arguments]         ${storage_path}
    ${rec_path} =       Split String                    ${storage_path}
    ${dst_path} =       Find Fail Data Backup Dir       ${CURDIR}\\..\\Fail_Rec_Data\\${rec_folder_date}\\${TEST_NAME}
    :FOR                ${path}                         IN                      @{rec_path}
    \                   Run Keyword If Test Failed      Copy Rec Data           ${path}             ${dst_path}

Synchronize Server Time
#    Log                 sync time between RF server and vast server             console=yes
#    Run Process         net         time        \\\\${REMOTE_SERVER_IP}         /set        /yes
    Log             Testing host and client should be co-existed under a VM host. No other sync machanism is needed.

Import VAST DB Remote Library
    [Arguments]             ${vast_db_lib_ip}           ${vast_db_lib_name}
    Import Library          Remote                      ${vast_db_lib_ip}:${VAST_DB_UTILITY_REMOTE_SERVER_PORT}
    ...                     WITH NAME                   ${vast_db_lib_name}
    Set Test Variable       ${remote_vast_db_lib}       ${vast_db_lib_name}

Import GAEA DB Remote Library
    [Arguments]             ${gaea_db_lib_ip}           ${gaea_db_lib_name}
    Import Library          Remote                      ${gaea_db_lib_ip}:${GAEA_DB_UTILITY_REMOTE_SERVER_PORT}
    ...                     WITH NAME                   ${gaea_db_lib_name}
    Set Test Variable       ${remote_gaea_db_lib}       ${gaea_db_lib_name}

Import Video Parser Remote Library
    [Arguments]             ${video_parser_lib_ip}      ${video_parser_lib_name}
    Import Library          Remote                      ${video_parser_lib_ip}:${VIDEO_PARSER_EXECUTOR}
    ...                     WITH NAME                   ${video_parser_lib_name}
    Set Test Variable       ${remote_video_parser_lib}  ${video_parser_lib_name}

Import VAST Controller Remote Library
    [Arguments]             ${vast_controller_lib_ip}       ${vast_controller_lib_name}
    Import Library          Remote                          ${vast_controller_lib_ip}:${VAST_CONTROLLER_PORT}
    ...                     WITH NAME                       ${vast_controller_lib_name}
    Set Test Variable       ${remote_vast_controller_lib}   ${vast_controller_lib_name}

Delete Time Frame In Schedule List
    [Documentation]     Delete time frame in default schedule list to avoid redundant recording files when adding cameras
    ...                 to default strorage group since it will be also added to default schedule list and start to record.
    Run Keyword         ${remote_vast_controller_lib}.Set Time Frame Info             delete_time_frame=true
    Run Keyword         ${remote_vast_controller_lib}.Edit Recording Time Frame

Wait Util Recording Timeout
    [Arguments]         ${e_time}
    Synchronize Server Time
    ${date} =           Get Current Date                    result_format=%H:%M:%S
    ${timedelta} =      Convert Time                        ${date}                     timedelta
    ${curr_time} =      Convert To Integer                  ${timedelta.total_seconds()}
    Log                 recording end time: ${e_time} s     console=yes
    Log                 current time: ${curr_time} s        console=yes
    Should Be True 	    ${e_time} < ${curr_time}

Set "${type}" Time Interval
    ${date} =           Get Current Date                            result_format=datetime          exclude_millis=yes
    Run Keyword if      35 <= ${date.second} <= 59
    ...                 Sleep                       25 sec          Avoid add 1 more minutes when get current time has already rounded
    Run Keyword If      '${type}' == 'Individual'
    ...                 Set Individual Time Interval
    Run Keyword If      '${type}' == 'Disconnect'
    ...                 Set Disconnect Time Interval
    Run Keyword If      '${type}' == 'Continuous'
    ...                 Set Continous Time Interval
    Run Keyword If      '${type}' == 'Overlapping'
    ...                 Set Overlapping Time Interval
    Log                 Set time interval: ${time_interval}         console=yes

Set Start Time For Time Interval
    ${hour_min} =           Get Current Date        result_format=%H:%M     increment=${recording_duration} sec     exclude_millis=yes
    ${timedelta} =          Convert Time            ${hour_min}:00          timedelta
    ${s_time} =             Convert To Integer      ${timedelta.total_seconds()}
    Log                     start recording time: ${hour_min} (${s_time} s)                         console=yes             level=DEBUG
    Set Test Variable       ${start_hour_min}       ${hour_min}
    Set Test Variable       ${start_time}           ${s_time}

Set Individual Time Interval
    [Arguments]             ${duration}=${recording_duration}
    Set Start Time For Time Interval
    ${e_time} =             Add Time To Time        ${duration} sec       ${start_hour_min}:00
    ${e_time} =             Convert To Integer      ${e_time}
    Set Test Variable       ${end_time}             ${e_time}
    Set Test Variable       ${time_interval}        ${start_time} ${end_time}

Set Disconnect Time Interval
    Set Start Time For Time Interval
    ${e_time} =             Add Time To Time        1 minute                ${start_hour_min}:00
    ${e_time_1} =           Convert To Integer      ${e_time}
    ${s_time} =             Add Time To Time        2 minute                ${start_hour_min}:00
    ${s_time_2} =           Convert To Integer      ${s_time}
    ${e_time} =             Add Time To Time        3 minute                ${start_hour_min}:00
    ${e_time_2} =           Convert To Integer      ${e_time}
    Set Test Variable       ${end_time}             ${e_time_2}
    Set Test Variable       ${time_interval}        ${start_time} ${e_time_1} ${s_time_2} ${e_time_2}

Set Continous Time Interval
    Set Start Time For Time Interval
    ${e_time} =             Add Time To Time        1 minute                ${start_hour_min}:00
    ${e_time_1} =           Convert To Integer      ${e_time}
    ${s_time_2} =           Set Variable            ${e_time_1}
    ${e_time} =             Add Time To Time        2 minute                ${start_hour_min}:00
    ${e_time_2} =           Convert To Integer      ${e_time}
    Set Test Variable       ${end_time}             ${e_time_2}
    Set Test Variable       ${time_interval}        ${start_time} ${e_time_1} ${s_time_2} ${e_time_2}

Set Overlapping Time Interval
    Set Start Time For Time Interval
    ${e_time} =             Add Time To Time        2 minute                ${start_hour_min}:00
    ${e_time_1} =           Convert To Integer      ${e_time}
    ${s_time} =             Add Time To Time        1 minute                ${start_hour_min}:00
    ${s_time_2} =           Convert To Integer      ${s_time}
    ${e_time} =             Add Time To Time        3 minute                ${start_hour_min}:00
    ${e_time_2} =           Convert To Integer      ${e_time}
    Set Test Variable       ${end_time}             ${e_time_2}
    Set Test Variable       ${time_interval}        ${start_time} ${e_time_1} ${s_time_2} ${e_time_2}
