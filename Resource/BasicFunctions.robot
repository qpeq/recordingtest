*** Settings ***
Resource        ServerSettings.robot
Resource        DBValueVerification.robot
Resource        VideoDataVerification.robot
Library         ../TestLibrary/VideoParser/VideoParser.py                        WITH NAME       ${remote_video_parser_lib}
Library         ../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Library         ../TestLibrary/CmdRunner/CmdRunner.py
Library         ../TestLibrary/CmdRunner/ScheduleMgr.py
Library         ../TestLibrary/Utility/Utility.py


*** Keywords ***
The keyword is for testing only
    ${timecard} =           GetRecTmcard
    ${timecard} =           Set recording mode to 1st timeframe of timecard             ${timecard}             None
    UpdRecTmcard            ${timecard}
    ${timecard} =           GetRecTmcard
    ${timecard} =           Set recording mode to 1st timeframe of timecard             ${timecard}             Continuous
    UpdRecTmcard            ${timecard}
    ${timecard} =           GetRecTmcard

Set recording mode as ${mode}
    # The mode can be "None", "Continuous", or "Event"
    ${timecard} =           GetRecTmcard
    ${timecard} =           Set recording mode to 1st timeframe of timecard             ${timecard}             ${mode}
    UpdRecTmcard            ${timecard}

Set continuous recording from next minute for 1 minute long
    ${timecard} =           GetRecTmcard
    ${timecard} =           Set recording mode to 1st timeframe of timecard             ${timecard}             Continuous
    ${timecard} =           Set daily recording period                                  ${timecard}             1
    UpdRecTmcard            ${timecard}

Add alarm for camera 1's motion to trigger camera 1 recording
    InsEvtTmcard            CMD_INS_EVT_TMCARD_CAM1_TRIGGERS_ITSELF

Insert camera simulator port ${port}
    InsertCam               Port=${port}

Camera ${no} is recording
    ${result} =             IsCamRecording                      C_${no}
    Should be equal         ${result}                           ${TRUE}

Camera ${no} is not recording
    ${result} =             IsCamRecording                      C_${no}
    Should be equal         ${result}                           ${FALSE}

Wait until Camera ${no} starts recording
    Wait until keyword succeeds     3 min                       10 sec
    ...                             Camera ${no} is recording

Wait until Camera ${no} stops recording
    Wait until keyword succeeds     2 min                       10 sec
    ...                             Camera ${no} is not recording

Wait for Camera 1 finishes recording
    Wait until Camera 1 starts recording
    Wait until Camera 1 stops recording

Restore VAST to the default settings
    Restore VAST To Default Settings

Install VAST From The Default Location
    Run Keyword     Utility.Install VAST          ${VAST_SETUP_PATH}

Copy VAST Default Settings
    Run Keyword     ${remote_vast_controller_lib}.Force Stop VAST Server
    Sleep           5 sec
    Run Keyword     ${remote_vast_controller_lib}.Copy Default Database
    Run Keyword     ${remote_vast_controller_lib}.Start VAST Server

Set 1 min continuous recording that starts from next minute for the default camera
    Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Continuous"

Wait until recording finishes
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}

Copy recording date if test case fails
    Get Recording Folder Name
    Copy Recording Data If Test Failed              ${default_storage_rec_path}

Copy data if fails and restore to the default settings
    Restore VAST to the default settings
    Copy recording date if test case fails

Settings For "${type}" Time Interval Daily Time Frame With Recording Mode "${rec_mode}"
    Delete Time Frame In Schedule List
    Insert Camera                                   ref_name=${cam_ref_name}                        host_ip=${camera_ip}
    ...                                             rec_gp_id=${default_storage_gp_id}
    Set "${type}" Time Interval
    Set Time Frame Info                             tf_name=TimeFrame1          repeat_mode=Daily   rec_mode=${rec_mode}
    ...                                             interval_list=${time_interval}
    Edit Recording Time Frame                       revision=3                                      camera_name=C_1         group_id=${default_storage_gp_id}

Check if the default camera's recording data is 1 min long
    Connect To VAST Database
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    Disconnect To Database

Gently Start VAST services
    start VAST services

Gently stop VAST services
    Stop VAST services              ${FALSE}

Recording server should not crash
    Recording server should not have any crash dump
