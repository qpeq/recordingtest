@ECHO OFF

REM Activate required Python v-environment for test execution
REM Uncomment next line and adjust NAME if you use "virtualenv"
REM and "virtualenvwrapper-win" 
REM call workon NAME_OF_YOUR_VIRTUALENV

REM Clean up Results folder before you start!
REM /Q options ensures that no confirmation by user is required
REM Uncomment next line if you use a "Results" folder
del /Q Results\*

ECHO _
ECHO First test iteration
REM This will run every test case which robot can find in "TESTS" folder
REM NOTE: If you have your test in a single .robot file (or .txt or whatever)
REM       then replace "TESTS" at the end of this block by your target file.
REM NOTE: The "^" at the end of each line (except the last in the block) is required
REM       because the command is broken down in multiple lines
call robot  --log log_1.html                ^
            --report NONE                   ^
            --output output_1.xml           ^
            --outputdir Results             ^
            --loglevel TRACE                ^
            .\TestCase\ShortTermTest

ECHO _
ECHO Second test iteration
REM Rerun only the failed test cases from first iteration
call robot  --rerunfailed Results\output_1.xml ^
            --runemptysuite                    ^
            --log log_2.html                   ^
            --report NONE                      ^
            --output output_2.xml              ^
            --outputdir Results                ^
            --loglevel TRACE                   ^
            .\TestCase\ShortTermTest


ECHO _
ECHO Third test iteration
REM Rerun only the failed test cases from second iteration
call robot  --rerunfailed Results\output_2.xml ^
            --runemptysuite                    ^
            --log log_3.html                   ^
            --report NONE                      ^
            --output output_3.xml              ^
            --outputdir Results                ^
            --loglevel TRACE                   ^
            .\TestCase\ShortTermTest


ECHO _
ECHO FINAL POST PROCESSING
REM Merge test result from all iterations and generate a final Report & Log
call rebot  --processemptysuite        ^
            --log    log.html          ^
            --report report.html       ^
            --output output.xml        ^
            --outputdir Results        ^
            --merge                    ^
            Results\*.xml

REM Clean up Results folder before you go!
del /Q Results\output_*.xml

