*** Settings ***
Documentation       Here are the test cases for long-term test.
Resource            ../../Resource/DBValueVerification.robot
Resource            ../../Resource/VideoDataVerification.robot
Resource            ../../Resource/ServerSettings.robot
Variables           ../../TestLibrary/RemoteServerSettings.py
Test Teardown       Disconnect To Database

*** Variables ***
${test_verification_time_increment}         -24 hours
${save_file_list}                           ${True}
${camera_under_test}                        ${camera_1_rec_name} ${camera_2_rec_name} ${camera_3_rec_name} ${camera_4_rec_name} ${camera_5_rec_name} ${camera_6_rec_name} ${camera_7_rec_name} ${camera_8_rec_name}
${real_cam_under_test}                      ${real_cam_1} ${real_cam_2} ${real_cam_3} ${real_cam_4}
${reserve_video_day}                        3 days
${nas_recording_path}                       \\\\172.18.141.14\\recording
${event_ecptected_cnt}                      ${1426}
${event_cnt_err_range}                      ${5}

*** Test Cases ***
Long-term Continuous Recording For Daily Time Frame With Many Cameras
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_CONTINUOUS_REC_IP}         VASTDBUtility12
    ...         AND                 Import GAEA DB Remote Library               ${LT_CONTINUOUS_REC_IP}         GAEADBUtility12
    ...         AND                 Import Video Parser Remote Library          ${LT_CONTINUOUS_REC_IP}         VideoParser12
    ...         AND                 Set Recording Time Interval Info            0 86400
    ...         AND                 Connect To VAST Database
    ...         AND                 Get Long-term Recording Folder Name
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"
#    "${real_cam_2} ${real_cam_3}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "-1" And Continuous With "1"

Delete Record Data Older Than # Days For Continous Recording
    [Setup]     Run Keywords        Import VAST Controller Remote Library       ${LT_DELETE_REC_DATE}           VASTController13
    ...         AND                 Get Long-term Recording Folder Name
    Set Delete Recording Video Folder Name
    VAST "${lt_general_storage_rec_path}\\${delete_rec_date}" Should Not Exist
    [Teardown]  Log                 Do Nothing...

Continuous Recording For Daily Time Frame With Many Cameras To Network Storage Path
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_NETWORK_STORAGE_REC_PATH}         VASTDBUtility7
    ...         AND                 Import GAEA DB Remote Library               ${LT_NETWORK_STORAGE_REC_PATH}         GAEADBUtility7
    ...         AND                 Import Video Parser Remote Library          ${LT_NETWORK_STORAGE_REC_PATH}         VideoParser7
    ...         AND                 Set Recording Time Interval Info       0 86400
    ...         AND                 Connect To VAST Database
    ...         AND                 Get Long-term Recording Folder Name
    "${camera_under_test}" Video Recording Time Interval In "${nas_recording_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${nas_recording_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

Continuous Recording For Weekly Day-Based Time Frame With Many Cameras (00~08 + 18~24)
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_WEEKLY_DAY_BASED}          VASTDBUtility15
    ...         AND                 Import GAEA DB Remote Library               ${LT_WEEKLY_DAY_BASED}          GAEADBUtility15
    ...         AND                 Import Video Parser Remote Library          ${LT_WEEKLY_DAY_BASED}          VideoParser15
    ...         AND                 Set Recording Time Interval Info            0 28800 64800 86400
    ...         AND                 Connect To VAST Database
    ...         AND                 Get Long-term Recording Folder Name
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storageLT_ALARM_TRIGGER_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

Continuous Recording For Weekly Time Frame With Many Cameras (12~24 + 00~18 + 00~12)
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_WEEKLY_PERIOD_A_WEEK}      VASTDBUtility22
    ...         AND                 Import GAEA DB Remote Library               ${LT_WEEKLY_PERIOD_A_WEEK}      GAEADBUtility22
    ...         AND                 Import Video Parser Remote LibraryLT_ALARM_TRIGGER          ${LT_WEEKLY_PERIOD_A_WEEK}      VideoParser22
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Set Weekly Period-In-Week Recording Time Interval       43200 86400     0 64800     0 43200
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

Motion Event Recording For Daily Time Frame With Many Cameras
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_MOTION_EVENT}      VASTDBUtility23
    ...         AND                 Import GAEA DB Remote Library               ${LT_MOTION_EVENT}      GAEADBUtility23
    ...         AND                 Import Video Parser Remote Library          ${LT_MOTION_EVENT}      VideoParser23
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Recording Event Conunt Should Be "${event_ecptected_cnt}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Event Recording Duration Should Be "${default_motion_recording_time}" Seconde And Continuous With "${frame_frequency}"

Motion Event Recording By Alarm Triggered For Daily Time Frame With Many Cameras
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_ALARM_TRIGGER}      VASTDBUtility24
    ...         AND                 Import GAEA DB Remote Library               ${LT_ALARM_TRIGGER}      GAEADBUtility24
    ...         AND                 Import Video Parser Remote Library          ${LT_ALARM_TRIGGER}      VideoParser24
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Connect To VAST Database
    Set Test Variable               ${event_cnt_in_gaea_db}                     ${event_ecptected_cnt}
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Event Recording Duration Should Be "${default_post_event_time}" Seconde And Continuous With "${frame_frequency}"

Continous Recording For Weekly Time Frame Overlapped With No Recording For Daily Time Frame
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_WEEKY_OVERLAP_DAILY}      VASTDBUtility25
    ...         AND                 Import GAEA DB Remote Library               ${LT_WEEKY_OVERLAP_DAILY}      GAEADBUtility25
    ...         AND                 Import Video Parser Remote Library          ${LT_WEEKY_OVERLAP_DAILY}      VideoParser25
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Set Weekly Period-In-Week Recording Time Interval
    ...         43190 43200 64800 86400         28800 43200             0 43200
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

2 Weekly Day-Based Time Frame Overlapped With Continous Recording Mode And No Recording Mode
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_2_WEEKLY_DAY_BASED_OVERLAP}      VASTDBUtility32
    ...         AND                 Import GAEA DB Remote Library               ${LT_2_WEEKLY_DAY_BASED_OVERLAP}      GAEADBUtility32
    ...         AND                 Import Video Parser Remote Library          ${LT_2_WEEKLY_DAY_BASED_OVERLAP}      VideoParser32
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Set Recording Time Interval Info            0 21600 72000 86400
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

2 Weekly Time Frame Overlapped With Continous Recording Mode And No Recording Mode
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_2_WEEKLY_OVERLAP}      VASTDBUtility33
    ...         AND                 Import GAEA DB Remote Library               ${LT_2_WEEKLY_OVERLAP}      GAEADBUtility33
    ...         AND                 Import Video Parser Remote Library          ${LT_2_WEEKLY_OVERLAP}      VideoParser33
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Set Weekly Period-In-Week Recording Time Interval
    ...         43200 64800         28800 43200             0 21600
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

Continuous Recording For Weekly Day-Based Overlapped With No Recording For Weekly Time Frame
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_WEEKLY_DAY_BASED_OVERLAP_WEEKLY}      VASTDBUtility34
    ...         AND                 Import GAEA DB Remote Library               ${LT_WEEKLY_DAY_BASED_OVERLAP_WEEKLY}      GAEADBUtility34
    ...         AND                 Import Video Parser Remote Library          ${LT_WEEKLY_DAY_BASED_OVERLAP_WEEKLY}      VideoParser34
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Set Weekly Period-In-Week Recording Time Interval
    ...         64800 86400         0 21600 72000 86400    0 28800
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

Continuous Recording For Weekly Day-Based Overlapped With No Recording For Daily Time Frame
    [Setup]     Run Keywords        Import VAST DB Remote Library               ${LT_WEEKLY_DAY_BASED_OVERLAP_DAILY}      VASTDBUtility35
    ...         AND                 Import GAEA DB Remote Library               ${LT_WEEKLY_DAY_BASED_OVERLAP_DAILY}      GAEADBUtility35
    ...         AND                 Import Video Parser Remote Library          ${LT_WEEKLY_DAY_BASED_OVERLAP_DAILY}      VideoParser35
    ...         AND                 Get Long-term Recording Folder Name
    ...         AND                 Set Recording Time Interval Info            0 21600 64800 86400
    ...         AND                 Connect To VAST Database
    "${camera_under_test}" Video Recording Time Interval In "${lt_general_storage_rec_path}" Should Be "${time_interval}"
    "${camera_under_test}" In "${lt_general_storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${rec_duration_second}" And Continuous With "${frame_frequency}"

*** Keywords ***
Set Recording Time Interval Info
    [Arguments]                 ${time_interval_expected}
    Set Test Variable           ${time_interval}            ${time_interval_expected}
    ${time_interval_list} =     Split String                ${time_interval_expected}
    ${len} =                    Get Length                  ${time_interval_list}
    ${duration} =               Set Variable                ${EMPTY}
    ${index} =                  Set Variable                ${0}
    :FOR                        ${interval_idx}             IN RANGE                0       ${len/2}
    \                           ${diff} =                   Evaluate                ${${time_interval_list[${index}+${1}]}} - ${${time_interval_list[${index}]}}
    \                           ${duration} =               Catenate                ${duration}             ${diff}
    \                           ${index} =                  Evaluate                ${index}+${2}
    Log                         Recording Duration: "${duration}"                   console=yes
    Set Test Variable           ${rec_duration_second}      ${duration}
    Set Test Variable           ${time_interval_cnt}        ${len/2}

Set Weekly Period-In-Week Recording Time Interval
    [Arguments]        ${sunday}                ${workday}                          ${saturday}
    ${week_day} =      Convert Date             ${rec_folder_date}                  date_format=%Y-%m-%d        result_format=%A
    Run Keyword If     '${week_day}'=='Sunday'  Set Recording Time Interval Info    ${sunday}
    ...                ELSE IF                 '${week_day}'=='Monday' or '${week_day}'=='Tuesday' or '${week_day}'=='Wednesday' or '${week_day}'=='Thursday' or '${week_day}'=='Friday'        Set Recording Time Interval Info        ${work_day}
    ...                ELSE IF                 '${week_day}'=='Saturday'            Set Recording Time Interval Info    ${saturday}

"${cameras}" Video Recording Time Interval In "${storage_rec_path}" Should Be "${time_interval}"
    Log                     time interval: ${time_interval}     console=yes
    ${camera} =             Split String                        ${cameras}
    :FOR                    ${itm}                              IN                      @{camera}
    \                       "${itm}" In "${storage_rec_path}" Recording Time Interval Should Be "${time_interval}"

"${cam_folder_name}" In "${storage_rec_path}" Recording Time Interval Should Be "${time_interval_expected}"
    [Documentation]         Sometimes recording server will record more 10 seconds time interval since schedule start time is at 00:00,
    ...                     so we dont check the db time interval row count.
    ${gaea_db_path} =       Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    Run Keyword             ${remote_gaea_db_lib}.Connect To Database               ${gaea_db_path}
    ${time_interval} =      Split String            ${time_interval_expected}
    ${result} =             Run Keyword             ${remote_gaea_db_lib}.Get Time Interval Info
    ${date} =               Get Current Date        result_format=%Y-%m-%d          increment=${test_verification_time_increment}
    ${curr_date_epoch} =    Convert Date            ${date} 00:00:00                epoch
    ${index} =              Set Variable            ${0}
    :FOR                    ${itm}                  IN                              @{result}
    \                       ${s_time} =             Get Epoch Time                  ${curr_date_epoch}          ${${time_interval[${index}]}}
    \                       ${e_time} =             Get Epoch Time                  ${curr_date_epoch}          ${${time_interval[${index+1}]}}
    \                       Log                     Time Interval Settings: ${s_time} - ${e_time}               console=yes
    \                       Log                     GAEA DB Recording Time: ${itm['startrecordtime']} - ${itm['endrecordtime']}     console=yes
    \                       Should Be True          ${itm['startrecordtime']} <= ${s_time} + ${rec_time_err_range} < ${e_time} - ${rec_time_err_range} <= ${itm['endrecordtime']}
    ...                     GAEADBError: expected time interval should be "${s_time} + ${rec_time_err_range} ~ ${e_time} - ${rec_time_err_range}" but gaea db time interval is "${itm['startrecordtime']} ~ ${itm['endrecordtime']}" in 'timeinterval' table
    \                       ${index} =              Evaluate                                ${index}+${2}
    \                       Run Keyword If 	        ${time_interval_cnt} <= ${index}        Exit For Loop

"${cameras}" In "${storage_rec_path}" Video File Multiple Time Interval Duration Should Be "${duration}" And Continuous With "${frequency}"
    ${camera} =             Split String            ${cameras}
    :FOR                    ${itm}                  IN                      @{camera}
    \                       @{file_list} =          Get All Interval Recording Video Files List       ${time_interval_cnt}
    ...                     ${storage_rec_path}     ${itm}
    \                       Video File "@{file_list}" Duration Should Be "${duration}" And Continuous With "${frequency}"

Set Delete Recording Video Folder Name
    ${date} =               subtract_time_from_date         ${rec_folder_date}              ${reserve_video_day}        result_format=%Y-%m-%d
    Set Test Variable       ${delete_rec_date}              ${date}

"${cameras}" In "${storage_rec_path}" Recording Event Conunt Should Be "${evt_cnt}"
    ${camera} =             Split String                    ${cameras}
    :FOR                    ${itm}                          IN                      @{camera}
    \                       "${itm}" In "${storage_rec_path}" Event Conunt Should Be "${evt_cnt}"

"${cameras}" In "${storage_rec_path}" Event Recording Duration Should Be "${rec_time}" Seconde And Continuous With "${frequency}"
    ${camera} =             Split String                    ${cameras}
    :FOR                    ${itm}                          IN                      @{camera}
    \                       "${itm}" In ${storage_rec_path}" Event Recording "${event_cnt_in_gaea_db}" Duration Should Be "${rec_time}" Second And Continuous With "${frequency}"

"${camera}" In ${storage_rec_path}" Event Recording "${count}" Duration Should Be "${duration}" Second And Continuous With "${frequency}"
    Set Test Variable       ${save_file_list}               ${False}
    :FOR                    ${idx}                          IN RANGE                2               ${count}
    \                       ${str_idx} =                    Convert To String       ${idx}
    \                       "${camera}" In "${storage_rec_path}" Video Clip "${str_idx}" Duration Should Be "${duration}" Second And Continuous With "${frequency}"