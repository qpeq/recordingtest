*** Settings ***
Variables       ../../TestLibrary/RemoteServerSettings.py
Resource        ../../Resource/ServerSettings.robot
Library         ../../TestLibrary/VideoParser/VideoParser.py                        WITH NAME       ${remote_video_parser_lib}
Library         ../../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Resource        ../../Resource/DBValueVerification.robot
Resource        ../../Resource/VideoDataVerification.robot
Test Timeout    10 min
Suite Setup     Get Recording Folder Name
Test Setup      Run Keywords        Synchronize Server Time
...             AND                 Connect To VAST Database
Test Teardown   Run Keywords        Disconnect To Database
...             AND                 Copy Recording Data If Test Failed      ${default_storage_rec_path}
...             AND                 Restore To Default Settings             ${default_storage_rec_path}

*** Variables ***
@{camera_trigger}           C_1                 ${cam_ref_name}-trigger
@{camera_recording}         C_2                 ${cam_ref_name}-recording
${alarm_tmcard_name}        Alarm Time Card
${alarm_tmcard_id}          ${4}
${post_event_time}          10

*** Test Cases ***
Add Alarm List
    Set Alarm List For Recording "@{camera_recording}[1]" When Motion Trigger "@{camera_trigger}[1]"
    Schedule List "${alarm_tmcard_name}" Should Be Added
    Wait Until Keyword Succeeds     5 sec       0.5 sec
    ...                             "@{camera_recording}[0]" Should Be Recorded When Motion Trigger "@{camera_trigger}[0]" In "${alarm_tmcard_id}"

Edit Alarm List
    [Setup]                 Run Keywords        Connect To VAST Database
    ...                     AND                 Set Alarm List For Recording "@{camera_recording}[1]" When Motion Trigger "@{camera_trigger}[1]"
    ...                     AND                 Schedule List "${alarm_tmcard_name}" Should Be Added
    Set Time Frame Info     timecard_id=4       time_frame_type=alarm
    ${list} =               Create List
    ${cam_entry} =          Create List
    Append To List          ${cam_entry}        C_1
    Append To List          ${cam_entry}        ${default_storage_gp_id}
    Append To List          ${list}             ${cam_entry}
    Update Alarm List       timecard_id=4       revision=1                  tmcard_name=${alarm_tmcard_name}
    ...                     event_trigger_cam_name=C_1              recording_cam=${list}
    ...                     related_devices=C_1                     device_ref_name=${cam_name}-trigger
    "@{camera_trigger}[0]" Should Be Recorded When Motion Trigger "@{camera_trigger}[0]" In "${alarm_tmcard_id}"

Delete Alarm List
    [Setup]                 Run Keywords        Connect To VAST Database
    ...                     AND                 Set Alarm List For Recording "@{camera_recording}[1]" When Motion Trigger "@{camera_trigger}[1]"
    ...                     AND                 Schedule List "${alarm_tmcard_name}" Should Be Added
    Delete Alarm List       timecard_id=4
    Schedule List "${alarm_tmcard_name}" Should Be Deleted

Recording While Alarm Was Triggered By Motion Event With Schedule Mode Is No Recording
    [Setup]                 Run Keywords        Connect To VAST Database
    ...                     AND                 Synchronize Server Time
    ...                     AND                 Settings For No Recording Schedule
    Set Alarm List For Recording "@{camera_recording}[1]" When Motion Trigger "@{camera_trigger}[1]"
    Wait Until Keyword Succeeds                 ${wait_for_recording_timeout}           ${retry_interval}
    ...                                         Wait Util Recording Timeout             ${end_time}
    Sleep                   10 s                sleep more if the motion trigger right on the end time
    "2-${cam_name}-recording" In "${default_storage_rec_path}" Each Recording Time Duration Should Be "${post_event_time}" sec
    "2-${cam_name}-recording" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "${post_event_time}" Second And Continuous With "${frame_frequency}"

*** Keywords ***
Settings For No Recording Schedule
    Set Time Frame Info             rec_mode=None           tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     5 sec       0.5 sec
    ...                             Timeframe ID "4" Recording Mode Should Be "\${rec_mode_none}"

Set Alarm List For Recording "${camera_1}" When Motion Trigger "${camera_2}"
    Insert Camera           ref_name=${camer a_2}                   port=9002                          host_ip=${camera_ip}
    ...                     rec_gp_id=${default_storage_gp_id}
    Insert Camera           ref_name=${camera_1}                    port=9001                            host_ip=${camera_ip}
    ...                     rec_gp_id=${default_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info     timecard_id=0                           time_frame_type=alarm               repeat_mode=Daily
    ...                     rec_mode=Continuous                     interval_list=${time_interval}
    ${list} =               Create List
    ${cam_entry} =          Create List
    Append To List          ${cam_entry}        C_2
    Append To List          ${cam_entry}        ${default_storage_gp_id}
    Append To List          ${list}             ${cam_entry}
    Add Alarm List          timecard_id=0                           tmcard_name=${alarm_tmcard_name}    event_trigger_cam_name=C_1      recording_cam=${list}
    ...                     related_devices=C_2                     device_ref_name=${cam_name}-recording
