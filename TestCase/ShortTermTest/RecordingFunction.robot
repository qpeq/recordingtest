*** Settings ***
Variables       ../../TestLibrary/RemoteServerSettings.py
Resource        ../../Resource/ServerSettings.robot
Library         ../../TestLibrary/VideoParser/VideoParser.py                        WITH NAME       ${remote_video_parser_lib}
Library         ../../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Resource        ../../Resource/DBValueVerification.robot
Resource        ../../Resource/VideoDataVerification.robot
Test Timeout    10 min
Suite Setup     Get Recording Folder Name
Test Setup      Run Keywords        Synchronize Server Time
...             AND                 Connect To VAST Database
Test Teardown   Run Keywords        Disconnect To Database
...             AND                 Copy Recording Data If Test Failed              ${default_storage_rec_path}
...             AND                 VASTController.Restore To Default Settings      ${default_storage_rec_path}

*** Variables ***
${motion_di_event_port}             9003
${di_normal_to_activated_port}      9004

*** Test Cases ***
No Recording
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "None"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    Recording Path "${default_storage_rec_path}" Should Be Empty

Continuous Recording For Daily Time Frame
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"

# Continuous Recording To Network Storage Path For Daily Time Frame
#     [Setup]     Run Keywords        Connect To VAST Database
#     ...         AND                 Synchronize Server Time
#     ...         AND                 Settings For Recording To Network Storage Path
#     Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
#     ...                             Wait Util Recording Timeout         ${end_time}
#     "1-${cam_name}" In "${nas_path}" Recording Time Interval Should Be "${time_interval}"
#     "1-${cam_name}" In "${nas_path}" Video Clip "all" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
#     [Teardown]  Run Keywords        Disconnect To Database
#     ...         AND                 Copy Recording Data If Test Failed  ${nas_path}
#     ...         AND                 Restore To Default Settings         ${nas_path}

Continuous Recording For Daily Time Frame With Disconnect Time Interval
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Disconnect" Time Interval Daily Time Frame With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             "1-${cam_name}" In "\${default_storage_rec_path}" Video Clip "2" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"

Continuous Recording For Many Daily Time Frame With Continous Time Interval
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For 2 Daily Time Frame With "Continuous" Time Interval
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    ${time_interval} =              Split String 	                    ${time_interval}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval[0]} ${time_interval[3]}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "120" Second And Continuous With "${frame_frequency}"

Continuous Recording For Many Daily Time Frame With Overlapping Time Interval
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For 2 Daily Time Frame With "Overlapping" Time Interval
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}           ${retry_interval}
    ...                             Wait Util Recording Timeout             ${end_time}
    ${time_interval} =              Split String 	                        ${time_interval}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval[0]} ${time_interval[3]}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "180" Second And Continuous With "${frame_frequency}"

Continuous Recording For Many Daily Time Frame With Disconnected Time Interval
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For 2 Daily Time Frame With "Disconnect" Time Interval
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             "1-${cam_name}" In "\${default_storage_rec_path}" Video Clip "2" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"

2 Daily Time Frame Overlapped With Continous Recording Mode And No Recording Mode
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For 2 Daily Time Frame "Overlapping" With "None" and "Continuous" Recording Mode
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    ${time_interval} =              Split String 	                    ${time_interval}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval[1]} ${time_interval[3]}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"

Continuous Recording For Daily Time Frame With Many Cameras
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For Many Cameras In Default Storage Group
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    "2-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "2-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"

Continuous Recording For Daily Time Frame With 2 Cameras In Different Schedule List
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For Many Cameras In Different Schedule List
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    ${time_interval} =              Split String 	                    ${time_interval}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval[0]} ${time_interval[1]}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    "2-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval[2]} ${time_interval[3]}"
    "2-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"

Continuous Recording For Daily Time Frame With Many Recording Path In Storage Group
    [Setup]     Run keywords            Connect To VAST Database
    ...         AND                     Synchronize Server Time
    ...         AND                     Settings For Many Recording Path In Storage Group
    ${create_redundant_file_time} =     Add Time To Time        20 sec      ${start_hour_min}:00
    ${create_redundant_file_time} =     Convert To Integer                  ${create_redundant_file_time}
    Wait Until Keyword Succeeds         ${wait_for_recording_timeout}       ${retry_interval}
    ...                                 Wait Util Recording Timeout         ${create_redundant_file_time}
    Log                                 Create redundant file ...           console=yes
    Stuff Full Storage Rec Path         ${new_storage_rec_path}
    Wait Until Keyword Succeeds         ${wait_for_recording_timeout}       ${retry_interval}
    ...                                 Wait Util Recording Timeout         ${end_time}
    Wait Until Keyword Succeeds         ${wait_for_recording_timeout}       ${retry_interval}
    ...                                 "1-${cam_name}" In "\${new_storage_rec_path} \${temp_storage_rec_path}" Recording Time Interval Should Be Continuous Within "${time_interval}"
    "1-${cam_name}" In Many Path "${new_storage_rec_path} ${temp_storage_rec_path}" Video Clip Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    [Teardown]      Run Keywords        Disconnect To Database
    ...             AND                 Copy Recording Data If Test Failed      ${new_storage_rec_path} ${temp_storage_rec_path}
    ...             AND                 Restore To Default Settings             ${new_storage_rec_path} ${temp_storage_rec_path}
    ...             AND                 Clear Storage Rec Path

Continuous Recording For Daily Time Frame With 2 Cameras In Different Storage Group
    [Setup]     Run keywords            Connect To VAST Database
    ...         AND                     Synchronize Server Time
    ...         AND                     Settings For Cameras In Different Storage Group
    Wait Until Keyword Succeeds         ${wait_for_recording_timeout}       ${retry_interval}
    ...                                 Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    "2-${cam_name}" In "${new_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "2-${cam_name}" In "${new_storage_rec_path}" Video Clip "1" Duration Should Be "${recording_duration}" Second And Continuous With "${frame_frequency}"
    [Teardown]      Run Keywords        Disconnect To Database
    ...             AND                 Copy Recording Data If Test Failed      ${default_storage_rec_path} ${new_storage_rec_path}
    ...             AND                 Restore To Default Settings             ${default_storage_rec_path} ${new_storage_rec_path}

Data Should Be Deleted When Delete Storage Group
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For Camera In New Storage Group
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}               ${retry_interval}
    ...                             Wait Util Recording Timeout                 ${end_time}
    Remove Storage Group            gp_id=${new_storage_gp_id}                  revision=4
    Wait Until Keyword Succeeds     30 sec       3 sec
    ...                             VAST "\${new_storage_rec_path}" Should Not Exist
    [Teardown]      Run Keywords    Disconnect To Database
    ...             AND             Restore To Default Settings             ${new_storage_rec_path}

Data Should Be Deleted When Delete Paths In Storage Group
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    Remove Path In Storage Group    path=${default_storage_rec_path}    gp_id=${default_storage_gp_id}      revision=2
    Wait Until Keyword Succeeds     30 sec       3 sec
    ...                             VAST "\${default_storage_rec_path}" Should Not Exist

Data Should Be Saved When Delete Paths In Storage Group
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    Remove Path In Storage Group    path=${default_storage_rec_path}    gp_id=${default_storage_gp_id}      revision=2
    ...                             method=RemoveEntry
    Recording Data In "${default_storage_rec_path}\\${rec_folder_date}" Should Exist

Data Should Be Deleted When Delete Camera In Storage Group
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    Remove Camera                   cam_name=C_1                        method=RemoveMedia          revision=2
    Wait Until Keyword Succeeds     30 sec       3 sec
    ...                             VAST "\${default_storage_rec_path}\\${rec_folder_date}\\1-${cam_name}" Should Not Exist

Data Should Be Saved When Delete Camera In Storage Group
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    Remove Camera                   cam_name=C_1                        method=RemoveEntry          revision=2
    Recording Data In "${default_storage_rec_path}\\${rec_folder_date}\\1-${cam_name}" Should Exist

Motion Event Recording With Daily Time Frame
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "Individual" Time Interval Daily Time Frame With Recording Mode "Event"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Event Conunt Should Be "${1}"
    "1-${cam_name}" In "${default_storage_rec_path}" "1" Event ID Should Be "${gaea_event_motion_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" Each Recording Time Duration Should Be "${20}" sec
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "20" Second And Continuous With "${frame_frequency}"

2 Different Events Trigger Continuously With Daily Time Frame
    [Setup]         Run Keywords                Connect To VAST Database
    ...             AND                         Synchronize Server Time
    ...             AND                         Settings For 2 Different Events Trigger Continuously With Daily Time Frame
    Sleep           ${recording_duration} sec   it is proved event should be occured in first place
    Remove Camera   cam_name=C_1                method=RemoveEntry
    "1-${cam_name}" In "${default_storage_rec_path}" "1" Event ID Should Be "${gaea_event_motion_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" "2" Event ID Should Be "${di_stop_until_rising_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" "3" Event ID Should Be "${di_stop_until_rising_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "20" Second And Continuous With "${event_frame_frequency}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "2" Duration Should Be "20" Second And Continuous With "${event_frame_frequency}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "3" Duration Should Be "10" Second And Continuous With "${event_frame_frequency}"

Recording From DI normal to DI activated
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For DI Normal To Activated
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" "1" Event ID Should Be "${di_stop_until_rising_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" "2" Event ID Should Be "${di_stop_until_rising_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "30" Second And Continuous With "${event_frame_frequency}"

DI Normal Recording With Daily Time Frame
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Set Individual Daily Time Frame With Event Recording And DigiInput "3"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Event Conunt Should Be "2"
    "1-${cam_name}" In "${default_storage_rec_path}" "1" Event ID Should Be "${di_stop_until_rising_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" Each Recording Time Duration Should Be "20" sec
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "20" Second And Continuous With "${event_frame_frequency}"

DI Activated Recording With Daily Time Frame
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Set Individual Daily Time Frame With Event Recording And DigiInput "3"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${cam_name}" In "${default_storage_rec_path}" Event Conunt Should Be "2"
    "1-${cam_name}" In "${default_storage_rec_path}" "1" Event ID Should Be "${di_stop_until_rising_id}"
    "1-${cam_name}" In "${default_storage_rec_path}" Each Recording Time Duration Should Be "20" sec
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "1" Duration Should Be "20" Second And Continuous With "${event_frame_frequency}"

*** Keywords ***
Settings For Recording To Network Storage Path
    Delete Time Frame In Schedule List
    Remove Path In Storage Group            path=${default_storage_rec_path}        gp_id=${default_storage_gp_id}      revions=2
    Wait Until Keyword Succeeds             10 sec       0.5 sec
    ...                                     "${default_storage_gp_id}" Recording Path "\${default_storage_rec_path}" Should Be Deleted
    Insert Nas Server                       host=${nas_host}        host_domain=${nas_host_domain}      user=${nas_user}    pwd=${nas_psw}      revision=3
    Wait Until Keyword Succeeds             ${wait_for_db_query}    ${db_retry_interval}
    ...                                     NAS Server "${nas_host}" Should Be Added
    Add Path In Storage Group               path=${nas_path}        gp_id=${default_storage_gp_id}      type=NetDomain     revision=2
    Wait Until Keyword Succeeds             1 min       0.5 sec

    ...                                     "${default_storage_gp_id}" Recording Path "\${nas_path}" Should Be Added
    Insert Camera                           ref_name=${cam_ref_name}                        host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1          repeat_mode=Daily   rec_mode=Continuous
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=3                                      camera_name=C_1                 group_id=${default_storage_gp_id}

Settings For "${type}" Time Interval Daily Time Frame With Recording Mode "${rec_mode}"
    Delete Time Frame In Schedule List
    Insert Camera                                   ref_name=${cam_ref_name}                        host_ip=${camera_ip}
    ...                                             rec_gp_id=${default_storage_gp_id}
    Set "${type}" Time Interval
    Set Time Frame Info                             tf_name=TimeFrame1          repeat_mode=Daily   rec_mode=${rec_mode}
    ...                                             interval_list=${time_interval}
    Edit Recording Time Frame                       revision=3                                      camera_name=C_1         group_id=${default_storage_gp_id}

Set Individual Daily Time Frame With Event Recording And DigiInput "${digi_input}"
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}            host_ip=${camera_ip}        port=${di_normal_to_activated_port}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1                  repeat_mode=Daily           rec_mode=Event
    ...                                     interval_list=${time_interval}      digi_input=${digi_input}
    Edit Recording Time Frame               revision=3                          camera_name=C_1             group_id=${default_storage_gp_id}

Settings For 2 Daily Time Frame With "${type}" Time Interval
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "${type}" Time Interval
    ${time_interval} =                      Split String 	                        ${time_interval}
    Set Time Frame Info                     tf_name=TimeFrame1                      repeat_mode=Daily
    ...                                     interval_list=${time_interval[0]} ${time_interval[1]}
    Set Time Frame Info                     tf_name=TimeFrame2                      repeat_mode=Daily
    ...                                     interval_list=${time_interval[2]} ${time_interval[3]}
    Edit Recording Time Frame               revision=3                              camera_name=C_1         group_id=${default_storage_gp_id}

Settings For 2 Daily Time Frame "${type}" With "${rec_mode_1}" and "${rec_mode_2}" Recording Mode
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "${type}" Time Interval
    ${time_interval} =                      Split String 	                        ${time_interval}
    Set Time Frame Info                     tf_name=TimeFrame1                      repeat_mode=Daily       rec_mode=${rec_mode_1}
    ...                                     interval_list=${time_interval[0]} ${time_interval[1]}
    Set Time Frame Info                     tf_name=TimeFrame2                      repeat_mode=Daily       rec_mode=${rec_mode_2}
    ...                                     interval_list=${time_interval[2]} ${time_interval[3]}
    Edit Recording Time Frame               revision=3                              camera_name=C_1         group_id=${default_storage_gp_id}

Settings For Many Cameras In Default Storage Group
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1                      repeat_mode=Daily
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=4                              camera_name=C_1 C_2             group_id=${default_storage_gp_id} ${default_storage_gp_id}

Settings For Many Cameras In Different Schedule List
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "Disconnect" Time Interval
    ${time_interval} =                      Split String 	                        ${time_interval}
    Set Time Frame Info                     tf_name=TimeFrame1                      repeat_mode=Daily
    ...                                     interval_list=${time_interval[0]} ${time_interval[1]}
    Edit Recording Time Frame               revision=4                              camera_name=C_1                 group_id=${default_storage_gp_id}
    Add Schedule List                       name=TimeCard2
    Set Time Frame Info                     tf_name=TimeFrame1                      repeat_mode=Daily
    ...                                     interval_list=${time_interval[2]} ${time_interval[3]}                   timecard_id=4
    Edit Recording Time Frame               revision=1                              tmcard_name=TimeCard2           timecard_id=4           camera_name=C_2         group_id=${default_storage_gp_id}

Settings For Many Recording Path In Storage Group
    Delete Time Frame In Schedule List
    Remove Path In Storage Group            path=${default_storage_rec_path}        gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds             10 sec       0.5 sec
    ...                                     "${default_storage_gp_id}" Recording Path "\${default_storage_rec_path}" Should Be Deleted
    Remove Directory                        ${new_storage_rec_path}
    Add Path In Storage Group               path=${new_storage_rec_path}            gp_id=${default_storage_gp_id}      revision=2
    Wait Until Keyword Succeeds             10 sec       0.5 sec
    ...                                     "${default_storage_gp_id}" Recording Path "\${new_storage_rec_path}" Should Be Added
    Create Directory                        ${default_storage_rec_path}
    Add Path In Storage Group               path=${temp_storage_rec_path}            gp_id=${default_storage_gp_id}      revision=3
    Wait Until Keyword Succeeds             10 sec       0.5 sec
    ...                                     "${default_storage_gp_id}" Recording Path "\${temp_storage_rec_path}" Should Be Added
    Insert Camera                           ref_name=${cam_ref_name}                        host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1          repeat_mode=Daily       rec_mode=Continuous
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=3                                          camera_name=C_1         group_id=${default_storage_gp_id}

Settings For Cameras In Different Storage Group
    Delete Time Frame In Schedule List
    Add Storage Group                       gp_name=${new_storage_gp}               gp_id=${new_storage_gp_id}
    Add Path In Storage Group               path=${new_storage_rec_path}            gp_id=${new_storage_gp_id}          revision=2
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Insert Camera                           ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                                     rec_gp_id=${new_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1          repeat_mode=Daily       rec_mode=Continuous
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=4          camera_name=C_1 C_2             group_id=${default_storage_gp_id} ${new_storage_gp_id}

Settings For Camera In New Storage Group
    Delete Time Frame In Schedule List
    Add Storage Group                       gp_name=${new_storage_gp}           gp_id=${new_storage_gp_id}
    Add Path In Storage Group               path=${new_storage_rec_path}        gp_id=${new_storage_gp_id}      revision=2
    Insert Camera                           ref_name=${cam_ref_name}            host_ip=${camera_ip}
    ...                                     rec_gp_id=${new_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1          repeat_mode=Daily       rec_mode=Continuous
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=3                  camera_name=C_1         group_id=${new_storage_gp_id}

Settings For 2 Different Events Trigger Continuously With Daily Time Frame
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}            host_ip=${camera_ip}        port=${motion_di_event_port}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set Time Frame Info                     tf_name=TimeFrame1                  repeat_mode=Daily           rec_mode=Event
    ...                                     stop_until_di_rising=true
    Edit Recording Time Frame               revision=3                          camera_name=C_1             group_id=${default_storage_gp_id}

Settings For DI Normal To Activated
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}            host_ip=${camera_ip}        port=${di_normal_to_activated_port}
    ...                                     rec_gp_id=${default_storage_gp_id}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1                  repeat_mode=Daily           rec_mode=Event
    ...                                     interval_list=${time_interval}      stop_until_di_rising=true
    Edit Recording Time Frame               revision=3                          camera_name=C_1             group_id=${default_storage_gp_id}

# Set Default Recording Path Variable
#     ${default_storage_rec_path} =       VASTController.Get Recording Path From Default Group
#     Set Suite Variable                  ${default_storage_rec_path}
