*** Settings ***
Variables       ../../TestLibrary/RemoteServerSettings.py
Resource        ../../Resource/ServerSettings.robot
Library         ../../TestLibrary/VideoParser/VideoParser.py                        WITH NAME       ${remote_video_parser_lib}
Library         ../../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Resource        ../../Resource/DBValueVerification.robot
Resource        ../../Resource/VideoDataVerification.robot
Test Timeout    3 min
Suite Setup     Get Recording Folder Name
Test Setup      Run Keywords        Synchronize Server Time
...             AND                 Connect To VAST Database
Test Teardown   Run Keywords        Disconnect To Database
...             AND                 Copy Recording Data If Test Failed      ${default_storage_rec_path}
...             AND                 Restore To Default Settings             ${default_storage_rec_path}

*** Variables ***
${manual_rec_time_err_range}        5 sec
${manual_rec_camera_channel}        C_1

*** Test Cases ***
Manual Recording With Schedule Mode Is No Recording
    [Setup]         Run Keywords        Connect To VAST Database
    ...             AND                 Synchronize Server Time
    ...             AND                 Settings For Camera In No Recording Schedule
    Set Recording Time Interval And Start Manual Recording "${recording_duration} sec"
    "1-${cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${cam_name}" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "55" Second And Continuous With "${frame_frequency}"

*** Keywords ***
Settings For Camera In No Recording Schedule
    Set Time Frame Info             rec_mode=None           tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Timeframe ID "4" Recording Mode Should Be "\${rec_mode_none}"
    Insert Camera                   ref_name=${cam_ref_name}                host_ip=${camera_ip}
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             "${cam_ref_name}" Should Be Added To Storage Group "${default_storage_gp_id}"

Set Recording Time Interval And Start Manual Recording "${time}"
    Manual Start Recording      ${manual_rec_camera_channel}        ${default_storage_gp_id}
    Synchronize Server Time
    ${curr_time} =              Get Current Date        result_format=%H:%M:%S          exclude_millis=yes
    Sleep                       ${time}                 recording for 1 minutes ...
    ${s_time} =                 Add Time To Time        ${manual_rec_time_err_range}    ${curr_time}
    ${e_time} =                 Add Time To Time        ${time}                         ${curr_time}
    Manual Stop Recording       ${manual_rec_camera_channel}        ${default_storage_gp_id}
    Synchronize Server Time
    Sleep                       ${manual_rec_time_err_range}
    ${timedelta} =              Convert Time            ${s_time}                       timedelta
    ${start_time} =             Convert To Integer      ${timedelta.total_seconds()}
    ${timedelta} =              Convert Time            ${e_time}                       timedelta
    ${end_time} =               Convert To Integer      ${timedelta.total_seconds()}
    Set Test Variable           ${time_interval}        ${start_time} ${end_time}
    Log                         current time : ${curr_time}                             console=yes