*** Settings ***
Resource        ../../Resource/BasicFunctions.robot

Test Timeout    5 min
Test Teardown   Copy data if fails and restore to the default settings

*** Variables ***

*** Test Cases ***
Do nothing
    Sleep           1 sec

Trigger alarm recording during continuous recording
    Insert camera simulator port 9009
    Add alarm for camera 1's motion to trigger camera 1 recording
    Sleep           60 sec

    Recording server should not crash

Stop recording server during continuous recording and then check if there is any crash dump
    Insert camera simulator port 9001
    Sleep           30 sec
    Gently stop VAST services
    Sleep           15 sec

    Recording server should not crash
    Gently Start VAST services

Stop recording server during alarm recording and then check if there is any crash dump
    Set recording mode as None
    Insert camera simulator port 9009
    Add alarm for camera 1's motion to trigger camera 1 recording
    Sleep           30 sec
    Gently stop VAST services
    Sleep           15 sec

    Recording server should not crash
    Gently Start VAST services


*** Keywords ***
