*** Settings ***
Variables       ../../TestLibrary/RemoteServerSettings.py
Resource        ../../Resource/ServerSettings.robot
Library         ../../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Resource        ../../Resource/DBValueVerification.robot
Library         OperatingSystem
Test Timeout    5 min
Suite Setup     Get Recording Folder Name
Test Setup      Run Keywords        Synchronize Server Time
...             AND                 Connect To VAST Database
Test Teardown   Run Keywords        Disconnect To Database
...             AND                 Restore To Default Settings         ${default_storage_rec_path}

*** Variables ***
${rename_storage_gp}            Rename Storage Group
${new_nas_path}                 \\\\${nas_host}\\shareB\\recording         #\\\\${nas_host}\\Users
${reserve_size}                 ${4096}
${cam_ref_name}                 ${cam_name_prefix}IP8330

*** Test Cases ***
Add Storage Group
    Add Storage Group               gp_name=${new_storage_gp}           gp_id=${new_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                ${db_retry_interval}
    ...                             Storage Group "${new_storage_gp}" Should Be Added

Delete Storage Group
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Add Storage Group               gp_name=${new_storage_gp}           gp_id=${new_storage_gp_id}
    ...         AND                 Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...         Storage Group "${new_storage_gp}" Should Be Added
    Remove Storage Group            gp_id=${new_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             Storage Group "${new_storage_gp}" Should Be Deleted

Rename Storage Group
    Rename Storage Group            gp_name=${rename_storage_gp}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             Storage Group "${rename_storage_gp}" Should Be Added

Add Path In Storage Group
    Add Path In Storage Group       path=${new_storage_rec_path}    gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${new_storage_rec_path}" Should Be Added
    [Teardown]  Run Keywords        Disconnect To Database
    ...         AND                 Restore To Default Settings     ${new_storage_rec_path}

Add Recovery Path In Storage Group
    Add Path In Storage Group       path=${new_storage_rec_path}    gp_id=${default_storage_gp_id}      recovery=True
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recovery Path "\${new_storage_rec_path}" Should Be Added
    [Teardown]  Run Keywords        Disconnect To Database
    ...         AND                 Restore To Default Settings     ${new_storage_rec_path}

Delete Storage Local Path
    Remove Path In Storage Group    path=${default_storage_rec_path}    gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${default_storage_rec_path}" Should Be Deleted

Edit Storage Local Path
    Edit Path In Storage Group      ori_path=${default_storage_rec_path}    new_path=${new_storage_rec_path}         gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                    ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${new_storage_rec_path}" Should Be Added
    [Teardown]  Run Keywords        Disconnect To Database
    ...         AND                 Restore To Default Settings         ${new_storage_rec_path}

Add Storage Network Path
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Insert Nas Server               host=${nas_host}        host_domain=${nas_host_domain}
    ...         user=${nas_user}    pwd=${nas_psw}
    ...         AND                 Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}        NAS Server "${nas_host}" Should Be Added
    Add Path In Storage Group       path=${nas_path}            gp_id=${default_storage_gp_id}      type=NetDomain
    Wait Until Keyword Succeeds     2 min                       ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${nas_path}" Should Be Added
    [Teardown]  Run Keywords        Disconnect To Database
    ...         AND                 Restore To Default Settings     ${nas_path}

Delete Storage Network Path
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Insert Nas Server               host=${nas_host}                host_domain=${nas_host_domain}
    ...         user=${nas_user}    pwd=${nas_psw}
    ...         AND                 Add Path In Storage Group       path=${nas_path}                type=NetDomain
    ...         AND                 Wait Until Keyword Succeeds     1 min                           0.5 sec
    ...         "${default_storage_gp_id}" Recording Path "\${nas_path}" Should Be Added
    Remove Path In Storage Group    path=${nas_path}                gp_id=${default_storage_gp_id}  revision=2
    Wait Until Keyword Succeeds     2 min                           0.5 sec
    ...                             "${default_storage_gp_id}" Recording Path "${nas_path}" Should Be Deleted
    [Teardown]  Run Keywords        Disconnect To Database
    ...         AND                 Restore To Default Settings     ${nas_path}

Edit Storage Network Path
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Insert Nas Server               host=${nas_host}        host_domain=${nas_host_domain}
    ...         user=${nas_user}    pwd=${nas_psw}
    ...         AND                 Add Path In Storage Group       path=${nas_path}        type=NetDomain
    ...         AND                 Wait Until Keyword Succeeds     2 min                   ${db_retry_interval}
    ...         "${default_storage_gp_id}" Recording Path "\${nas_path}" Should Be Added
    Edit Path In Storage Group      ori_path=${nas_path}            new_path=${new_nas_path}    gp_id=${default_storage_gp_id}
    ...                             revision=2                      rm_path_seq=2               type=NetDomain
    Wait Until Keyword Succeeds     2 min                           ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${new_nas_path}" Should Be Added
    [Teardown]  Run Keywords        Disconnect To Database
    ...         AND                 Restore To Default Settings     ${new_nas_path} ${nas_path}

Storage Recording Path Reserved Size
    Set Path In Storage Group       path=${default_storage_rec_path}    reserve_space=${reserve_size}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${default_storage_rec_path}" Should Be Reserved "\${reserve_size}" Bytes

Add Cameras To Storage Group
    Insert Camera                   ref_name=${cam_ref_name}        host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}" Should Be Added To Storage Group "${default_storage_gp_id}"

Delete Cameras From Storage Group
    [Setup]         Run Keywords    Connect To VAST Database
    ...             AND             Insert Camera                   ref_name=${cam_ref_name}        host_ip=${camera_ip}        rec_gp_id=${default_storage_gp_id}
    ...             AND             Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...             "${cam_ref_name}" Should Be Added To Storage Group "${default_storage_gp_id}"
    Remove Camera   cam_name=C_1    method=RemoveMedia
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}" Should Be Deleted From Storage Group "${default_storage_gp_id}"