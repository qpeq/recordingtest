*** Settings ***
Variables       ../../TestLibrary/RemoteServerSettings.py
Resource        ../../Resource/ServerSettings.robot
Library         ../../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Resource        ../../Resource/DBValueVerification.robot
Test Timeout    2 min
Suite Setup     Get Recording Folder Name
Test Setup      Run Keywords        Synchronize Server Time
...             AND                 Connect To VAST Database
Test Teardown   Run Keywords        Disconnect To Database
...             AND                 Restore To Default Settings             ${default_storage_rec_path}

*** Variables ***
${schedule_list_name}               SchList
${weekly_daily_based}               1.0 2.0 5.0 6.0
${weekly_daily_timeinterval}        0 28800 64800 86400
${daily_timeinterval}               28800 64800
${weekly_period_timeinterval}       0 86400 115200 147600 201600 234000 288000 320400 374400 406800 460800 493200 518400 561600 583200 604800
${target_timecard_id}               4

*** Test Cases ***
Add Schedule List
    Add Schedule List               name=${schedule_list_name}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             Schedule List "${schedule_list_name}" Should Be Added

Rename Schedule List
    Rename Schedule List            default_time_frame=true         tmcard_name=${schedule_list_name}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             Schedule List "${schedule_list_name}" Should Be Added

Delete Schedule List
    [Setup]     Run keywords        Connect To VAST Database
    ...         AND                 Add Schedule List               name=${schedule_list_name}
    Delete Schedule List            timecard_id=${target_timecard_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             Schedule List "${schedule_list_name}" Should Be Deleted

Add Daily Time Frame
    Set Time Frame Info             repeat_mode=Daily                   tf_name=${time_frame_name}          dl_mode=SingleDay
    ...                             interval_list=${daily_timeinterval}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Time Interval Should Be "${daily_timeinterval}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Recording Repeat Frequency Should Be Daily Settings

Add Weekly Day-Based Time Frame
    Set Time Frame Info             repeat_mode=Weekly                          tf_name=${time_frame_name}      dl_mode=SingleDay
    ...                             interval_list=${weekly_daily_timeinterval}  day=${weekly_daily_based}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Time Interval Should Be "${weekly_daily_timeinterval}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Recording Day Should Be "${weekly_daily_based}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Recording Repeat Frequency Should Be Weekly Day-based

Add Weekly Period Time Frame
    Set Time Frame Info             repeat_mode=Weekly                          tf_name=${time_frame_name}      dl_mode=Period
    ...                             interval_list=${weekly_period_timeinterval}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Time Interval Should Be "\${weekly_period_timeinterval}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Recording Repeat Frequency Should Be Weekly Period Settings

Add Camera In Schedule List
    Insert Camera                   ref_name=${cam_ref_name}    host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             "${first_camera_channel_id}" Should Be Added To Schedule List "${default_time_card_id}"

Delete Camera In Schedule List
    [Setup]                         Run keywords            Connect To VAST Database
    ...                             AND                     Insert Camera               ref_name=${cam_ref_name}
    ...                             host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}
    ...                             AND                     Wait Until Keyword Succeeds     5 sec       0.5 sec
    ...                             "${first_camera_channel_id}" Should Be Added To Schedule List "${default_time_card_id}"
    Edit Recording Time Frame       default_time_frame=true     revision=2
    Wait Until Keyword Succeeds     ${wait_for_db_query}        ${db_retry_interval}
    ...                             "${first_camera_channel_id}" Should Be Deleted From Schedule List "${default_time_card_id}"

Add Time Frame With Recording Mode None
    Set Time Frame Info             rec_mode=None           tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_none}"

Add Time Frame With Recording Mode Continous
    Set Time Frame Info             rec_mode=Continuous     tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_continuous}"

Add Time Frame With Motion Event
    Set Time Frame Info             rec_mode=Event          motion=true     tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Motion Trigger Should Be "\${1}"

Add Time Frame With Recording Mode PIR Event
    Set Time Frame Info             rec_mode=Event          pir=true            tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_pir}"

Add Time Frame With Recording Mode Tampering Event
    Set Time Frame Info             rec_mode=Event          tampering=true          tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_tampering}"

Add Time Frame With Recording Mode PPTZ Trigger Event
    Set Time Frame Info             rec_mode=Event          pptz_trigger=true       tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_pptz_trigger}"

Add Time Frame With Recording Mode Line Crossing Event
    Set Time Frame Info             rec_mode=Event          line_detection=true     tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_line_detection}"

Add Time Frame With Recording Mode Field Detection Event
    Set Time Frame Info             rec_mode=Event          field_detection=true    tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_field_detection}"

Add Time Frame With Recording Mode Digital Input Trigger State
    Set Time Frame Info             rec_mode=Event          digi_input=${di_trigger_state_value}        tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" DI Trigger Type Should Be "\${di_trigger_state_value}"

Add Time Frame With Recording Mode Digital Input Normal State
    Set Time Frame Info             rec_mode=Event          digi_input=${di_normal_state_value}         tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" DI Trigger Type Should Be "\${di_normal_state_value}"

Add Time Frame With Recording Mode Digital Input DI activated to DI normal
    Set Time Frame Info             rec_mode=Event          digi_input=0       stop_until_di_falling=true         tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_stop_until_di_falling}"

Add Time Frame With Recording Mode Digital Input DI normal to DI activated
    Set Time Frame Info             rec_mode=Event          digi_input=0       stop_until_di_rising=true        tf_name=${time_frame_name}
    Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "${target_timecard_id}" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "${target_timecard_id}" Event Type Should Be "${tag_stop_until_di_rising}"