*** Settings ***
Variables       ../../TestLibrary/RemoteServerSettings.py
Resource        ../../Resource/ServerSettings.robot
Library         ../../TestLibrary/VideoParser/VideoParser.py                        WITH NAME       ${remote_video_parser_lib}
Library         ../../TestLibrary/DBUtility/VASTDBUtility.py                        WITH NAME       ${remote_vast_db_lib}
Library         ../../TestLibrary/DBUtility/GAEADBUtility.py                        WITH NAME       ${remote_gaea_db_lib}
Library         ../../TestLibrary/VASTController/VASTController.py                  WITH NAME       ${remote_vast_controller_lib}
Resource        ../../Resource/DBValueVerification.robot
Resource        ../../Resource/VideoDataVerification.robot
Test Timeout    20 min
Suite Setup     Get Recording Folder Name
Test Setup      Run Keywords        Synchronize Server Time
...             AND                 Connect To VAST Database
Test Teardown   Run Keywords        Disconnect To Database
...             AND                 Copy Recording Data If Test Failed      ${default_storage_rec_path}
...             AND                 Restore To Default Settings             ${default_storage_rec_path}

*** Variables ***
${pre_event_time}                   20
${post_event_time}                  15
${recording_stream_idx}             2
${seamless_cam_name}                Mega Network Camera
${seamless_rec_time_err_range}      20
${aas_min_frame_rate}               1                           # unit: second

*** Test Cases ***
Seamless Recording
    [Setup]     Run Keywords                Connect To VAST Database
    ...         AND                         Synchronize Server Time
    ...         AND                         Settings For "Seamless" Recording "120" second(s)
    ...         AND                         Set View While Recording Flag
    ${stop_server_time} =                   Add Time To Time        80 sec      ${start_hour_min}:00
    ${stop_server_time} =                   Convert To Integer      ${stop_server_time}
    Wait Until Keyword Succeeds             ${wait_for_recording_timeout}       ${retry_interval}
    ...                                     Wait Util Recording Timeout         ${stop_server_time}
    VASTController.Force Stop VAST Server
    Sleep                                   15 sec
    VASTController.Start VAST Server
    Wait Until The Recording Folder Contains 3 Valid Videos

    "1-${seamless_cam_name}" In "${default_storage_rec_path}" Seamless Recording Time Interval Should Be "${time_interval}"
    "1-${seamless_cam_name}" In "${default_storage_rec_path}" Video Clip "2" Duration Should Be "${-1}" Second And Continuous With "${frame_frequency}"
    "1-${seamless_cam_name}" In "${default_storage_rec_path}" Video Clip "3" Duration Should Be "${-1}" Second And Continuous With "${frame_frequency}"
    "1-${seamless_cam_name}" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "110" Second And Continuous With "${-1}"

Recording Stream
    Insert Camera                   ref_name=${cam_ref_name}            host_ip=${camera_ip}        rec_gp_id=${default_storage_gp_id}
    ...                             rec_stream=${recording_stream_idx}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                ${db_retry_interval}
    ...                             "${cam_ref_name}" Recording Stream Index Should Be "${recording_stream_idx}"

Pre-Event And Post-Event Time
    Insert Camera                   ref_name=${cam_ref_name}            host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}
    ...                             pre_event=${pre_event_time}         post_event=${post_event_time}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                ${db_retry_interval}
    ...                             "${cam_ref_name}" Pre/Post-Event Time Should Be "${pre_event_time} ${post_event_time}"

Continuous Recording For Correct Recording Stream Index
    [Setup]                         Run Keywords                        Synchronize Server Time
    ...                             AND                                 Set Camera Recording Stream Index "${recording_stream_idx}" With Recording Mode "Continuous"
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             "1-${cam_name}" In "\${default_storage_rec_path}" Video Clip "1" Video Codec Should Be "JPEG"

Activity Adaptive Stream
    [Setup]     Run Keywords        Connect To VAST Database
    ...         AND                 Synchronize Server Time
    ...         AND                 Settings For "AAS" Recording "${recording_duration}" second(s)
    Wait Until Keyword Succeeds     ${wait_for_recording_timeout}       ${retry_interval}
    ...                             Wait Util Recording Timeout         ${end_time}
    "1-${seamless_cam_name}" In "${default_storage_rec_path}" Recording Time Interval Should Be "${time_interval}"
    "1-${seamless_cam_name}" In "${default_storage_rec_path}" Video Clip "all" Duration Should Be "${recording_duration}" Second And Continuous With "${aas_min_frame_rate}"

*** Keywords ***
Set View While Recording Flag
     Set Test Variable       ${view_while_recording}        ${True}

Set Camera Recording Stream Index "${rec_stream_idx}" With Recording Mode "${rec_mode}"
    Delete Time Frame In Schedule List
    Insert Camera                           ref_name=${cam_ref_name}    host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}
    ...                                     rec_stream=${rec_stream_idx}
    Set "Individual" Time Interval
    Set Time Frame Info                     tf_name=TimeFrame1          repeat_mode=Daily       rec_mode=${rec_mode}
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=3                                          camera_name=C_1         group_id=${default_storage_gp_id}

Settings For "${rec_type}" Recording "${duration}" second(s)
    Delete Time Frame In Schedule List
    Run Keyword If      '${rec_type}'=='Seamless'       Insert Camera   ref_name=c_${seamless_cam_name}     host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}  enable_seamless=true
    ...                 ELSE                            Insert Camera   ref_name=c_${seamless_cam_name}     host_ip=${camera_ip}    rec_gp_id=${default_storage_gp_id}  tm_shift=true
    ${date} =           Get Current Date                            result_format=datetime          exclude_millis=yes
    Run Keyword if      45 <= ${date.second} <= 59
    ...                 Sleep                       15 sec          Avoid add 1 more minutes when get current time has already rounded
    Set Individual Time Interval            ${duration}
    Set Time Frame Info                     tf_name=TimeFrame1          repeat_mode=Daily       rec_mode=Continuous
    ...                                     interval_list=${time_interval}
    Edit Recording Time Frame               revision=3                                          camera_name=C_1         group_id=${default_storage_gp_id}

"${cam_folder_name}" In "${storage_rec_path}" Seamless Recording Time Interval Should Be "${time_interval}"
    [Documentation]     Assume there are only 3 video clips,
    ...                 1st and 2nd video clips is normally recording,
    ...                 and the 3rd is from camera which losed by VAST server.
    Wait Until Keyword Succeeds         30 sec   10 sec
    ...                                 Until "${cam_folder_name}" In "\${storage_rec_path}" Seamless Recording Time Interval Should Be "${time_interval}"

Until "${cam_folder_name}" In "${storage_rec_path}" Seamless Recording Time Interval Should Be "${time_interval_expected}"
    ${gaea_db_path} =                   Get "${cam_folder_name}" GAEA DB Path In "${storage_rec_path}"
    GAEADBUtility.Connect To Database   ${gaea_db_path}
    ${time_interval} =                  Split String        ${time_interval_expected}
    ${len} =                            Get Length          ${time_interval}
    "${gaea_db_path}" Time Interval Count Shoule Be "${2}"
    ${result} =             GAEADBUtility.Get Time Interval Info
    ${date} =               Get Current Date        result_format=%Y-%m-%d      increment=${test_verification_time_increment}
    ${curr_date_epoch} =    Convert Date            ${date} 00:00:00            epoch
    ${s_time} =             Get Epoch Time          ${curr_date_epoch}          ${${time_interval[${0}]}}
    ${e_time} =             Get Epoch Time          ${curr_date_epoch}          ${${time_interval[${1}]}}
    Log                     Time Interval Settings: ${s_time} - ${e_time}       console=yes
    ${gaea_interval_s} =    Set Variable            ${result[1]['startrecordtime']}
    ${gaea_interval_e} =    Set Variable            ${result[0]['endrecordtime']}
    Log                     GAEA DB Recording Time: ${gaea_interval_s} - ${gaea_interval_e}     console=yes
    Should Be True          ${gaea_interval_s} <= ${s_time} + ${seamless_rec_time_err_range} < ${e_time} - ${seamless_rec_time_err_range} <= ${gaea_interval_e}
    ...                     GAEADBError: expected time interval should be "${s_time} + ${seamless_rec_time_err_range} ~ ${e_time} - ${seamless_rec_time_err_range}" but gaea db time interval is "${gaea_interval_s} ~ ${gaea_interval_e}" in 'timeinterval' table

Check If The Recording Folder Contains ${num} Valid Videos
    ${date} =               Get Current Date            result_format=%Y-%m-%d
    ${test_rec_path} =      Join Path                   ${default_storage_rec_path}     ${date}                 1-${seamless_cam_name}
    @{file_list} =          List Files In Directory     ${test_rec_path}                *.3gp                   True
    ${count} =              Get Length                  ${file_list}
    Run Keyword Unless      ${count} == ${num}          Run Keyword                     Fail                    The folder only contains ${count} videos, not ${num}.
    :For                    ${file}                     In                              @{file_list}
    \                       ${file_size} =              Get File Size                   ${file}
    \                       Run Keyword If              ${file_size} == 0               Run Keyword             Fail    ${file} size is 0.

Wait Until The Recording Folder Contains ${num} Valid Videos
    Wait Until Keyword Succeeds         12 min      10 sec       Check If The Recording Folder Contains ${num} Valid Videos
