*** Settings ***
Library         ../../TestLibrary/Utility/Utility.py
Library         ../../TestLibrary/CmdRunner/CmdRunner.py
Library         ../../TestLibrary/CmdRunner/ScheduleMgr.py
Library         Remote                                          http://172.18.52.12:29487
Resource        ../../Resource/BasicFunctions.robot

*** Variables ***

*** Test Cases ***
Check if there is any dump file in the remote machine
    Verify Recording Server Crash

Change recording state rapidly and repeatly
    Set recording mode as Continuous
    Sleep           10 sec
    Set recording mode as None
    Sleep           10 sec
    Set recording mode as Continuous
    Sleep           10 sec
    Set recording mode as None
    Sleep           10 sec
    Set recording mode as Continuous
    Sleep           10 sec
    Set recording mode as None

Setup Stress Test
    Set recording mode as None
    Insert camera simulator port 9011
    Insert camera simulator port 9012
    Insert camera simulator port 9013
    Insert camera simulator port 9014
    Insert camera simulator port 9015
    Insert camera simulator port 9016
    Insert camera simulator port 9017
    Insert camera simulator port 9018

    InsEvtTmcard    CMD_INS_STRESS_EVT_TMCARD

*** Keywords ***
