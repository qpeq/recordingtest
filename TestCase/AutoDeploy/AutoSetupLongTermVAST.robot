*** Settings ***
Variables                                           ../../TestLibrary/RemoteServerSettings.py
Resource                                            ../../Resource/DBValueVerification.robot
Resource                                            ../../Resource/ServerSettings.robot
Test Teardown                                       Disconnect To VAST Database

*** Variables ***
${long_term_vast_setup_path}                        \\\\172.18.101.4\\upload\\RecordingServerTest\\VAST\\VAST_setup.exe
${24hours_timeinterval}                             0 86400
${no_rec_timeinterval}                              43200 64800
${no_rec_timeinterval_weekly_daybased}              21600 72000
${network_host}                                     172.18.141.14
${network_host_domain}                              172.18.141.14
${network_rec_path}                                 \\\\172.18.141.14\\recording
${network_user}                                     sqa
${network_psw}                                      737161
${weekly_daily_timeinterval}                        0 28800 64800 86400
${weekly_period_timeinterval}                       43200 151200 172800 237600 259200 324000 345600 410400 432000 496800 518400 561600
${weekly_period_timeinterval_overlap}               43200 86400 115200 151200 201600 237600 288000 324000 374400 410400 460800 496800 518400 561600
${no_rec_weekly_period_overlap}                     64800 86400 129600 151200 216000 237600 302400 324000 388800 410400 475200 496800 540000 561600
${no_rec_weekly_period_overlap_with_day_based}      0 28800 108000 158400 194400 244800 280800 331200 367200 417600 453600 504000 583200 604800
${no_rec_daily__overlap_with_day_based}             21600 64800

*** Test Cases ***
Long-term VAST Server For 24HR Continuous Recording
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_CONTINUOUS_REC_IP}            VASTController1
    ...             AND                 Import VAST DB Remote Library               ${LT_CONTINUOUS_REC_IP}            VASTDBUtility1
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Continuous Recording For Daily Time Frame With Many Cameras

Long-term VAST Server For Delete Recording Data Older Than # Days
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_DELETE_REC_DATE}              VASTController2
    ...             AND                 Import VAST DB Remote Library               ${LT_DELETE_REC_DATE}              VASTDBUtility2
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Delete Recording Data Older Than # Days

Long-term VAST Server For Recording To Network Storage Path
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_NETWORK_STORAGE_REC_PATH}     VASTController3
    ...             AND                 Import VAST DB Remote Library               ${LT_NETWORK_STORAGE_REC_PATH}     VASTDBUtility3
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Recording To Network Storage Path

Long-term VAST Server For Weekly Day-Based Recording Time Frame
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_WEEKLY_DAY_BASED}              VASTController4
    ...             AND                 Import VAST DB Remote Library               ${LT_WEEKLY_DAY_BASED}              VASTDBUtility4
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Day-Based Time Frame With Many Cameras (00~08 + 18~24)

Long-term VAST Server For Weekly Recording Time Frame
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_WEEKLY_PERIOD_A_WEEK}          VASTController5
    ...             AND                 Import VAST DB Remote Library               ${LT_WEEKLY_PERIOD_A_WEEK}          VASTDBUtility5
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Time Frame With Many Cameras (12~24 + 00~18 + 00~12)

Long-term VAST Server For Motion Trigger Recording
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_MOTION_EVENT}                 VASTController6
    ...             AND                 Import VAST DB Remote Library               ${LT_MOTION_EVENT}                 VASTDBUtility6
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Motion Event Recording

Long-term VAST Server For Alarm Trigger Recording
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_ALARM_TRIGGER}                VASTController7
    ...             AND                 Import VAST DB Remote Library               ${LT_ALARM_TRIGGER}                VASTDBUtility7
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Alarm By Motion Trigger Recording

Long-term VAST Server For Weekly Time Frame (12-24 + 08-18 + 00-12) Overlapped With No Recording Daily Time Frmae (12-18)
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_WEEKY_OVERLAP_DAILY}          VASTController8
    ...             AND                 Import VAST DB Remote Library               ${LT_WEEKY_OVERLAP_DAILY}          VASTDBUtility8
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Time Frame Overlapped With No Recording Daily Time Frame

Long-term VAST Server For Weekly Day-Based Time Frmae (00-08 + 18-24) Overlapped With No Recording (06-20)
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_2_WEEKLY_DAY_BASED_OVERLAP}   VASTController9
    ...             AND                 Import VAST DB Remote Library               ${LT_2_WEEKLY_DAY_BASED_OVERLAP}   VASTDBUtility9
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Day-Based Time Frmae Overlapped Recording

Long-term VAST Server For Weekly Time Frame (12-24 + 08-18 + 00-12) Overlapped With No Recording (18-24 + 12-18 + 6-12)
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_2_WEEKLY_OVERLAP}             VASTController10
    ...             AND                 Import VAST DB Remote Library               ${LT_2_WEEKLY_OVERLAP}             VASTDBUtility10
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Time Frmae Overlapped Recording

Long-term VAST Server For Weekly Day-Based Time Frmae (00-08 + 18-24) Overlapped No Recording Weekly Period Time Frame (00-08 + 06-20 + 18-24)
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_WEEKLY_DAY_BASED_OVERLAP_WEEKLY}             VASTController11
    ...             AND                 Import VAST DB Remote Library               ${LT_WEEKLY_DAY_BASED_OVERLAP_WEEKLY}             VASTDBUtility11
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Day-Based Overlapped With No Recording Weekly Period Time Frame

Long-term VAST Server For Weekly Day-Based Time Frmae (00-08 + 18-24) Overlapped No Recording Daily Time Frame (06-18)
    [Setup]         Run Keywords        Import VAST Controller Remote Library       ${LT_WEEKLY_DAY_BASED_OVERLAP_DAILY}             VASTController12
    ...             AND                 Import VAST DB Remote Library               ${LT_WEEKLY_DAY_BASED_OVERLAP_DAILY}             VASTDBUtility12
    Install VAST From "${long_term_vast_setup_path}"
    Connect To VAST Database
    Settings For Weekly Day-Based Overlapped With No Recording Daily Time Frame

*** Keywords ***
Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Remove Path In Storage Group      path=${default_storage_rec_path}    gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                    ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${default_storage_rec_path}" Should Be Deleted
    Run Keyword                     ${remote_vast_controller_lib}.Add Path In Storage Group       path=${lt_general_storage_rec_path}     gp_id=${default_storage_gp_id}        revision=2
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${lt_general_storage_rec_path}" Should Be Added

Install VAST From "${vast_setup_path}"
    Run Keyword     ${remote_vast_controller_lib}.Install VAST          ${vast_setup_path}

Copy Default Database
    Run Keyword     ${remote_vast_controller_lib}.Force Stop VAST Server
    Sleep           5 sec
    Run Keyword     ${remote_vast_controller_lib}.Copy Default Database
    Run Keyword     ${remote_vast_controller_lib}.Start VAST Server

Settings For Continuous Recording For Daily Time Frame With Many Cameras
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info               repeat_mode=Daily                   dl_mode=SingleDay
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${24hours_timeinterval}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Daily Settings
    Insert Many Cameras

Settings For Delete Recording Data Older Than # Days
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Edit Storage Group    gp_name=Default Group
    ...                             reserve_time=4320       tm_base_cycle=true          revision=3
    Insert Many Cameras

Settings For Recording To Network Storage Path
    Run Keyword                     ${remote_vast_controller_lib}.Remove Path In Storage Group      path=${default_storage_rec_path}        gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                    ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${default_storage_rec_path}" Should Be Deleted
    Run Keyword                     ${remote_vast_controller_lib}.Insert Nas Server                 host=${network_host}                    host_domain=${network_host_domain}
    ...                             user=${network_user}                    pwd=${network_psw}
    Wait Until Keyword Succeeds     ${wait_for_db_query}                    ${db_retry_interval}
    ...                             NAS Server "${network_host}" Should Be Added
    Run Keyword                     ${remote_vast_controller_lib}.Add Path In Storage Group         path=${network_rec_path}                  gp_id=${default_storage_gp_id}            type=NetDomain          revision=2
    Wait Until Keyword Succeeds     2 min                       ${db_retry_interval}
    ...                             "${default_storage_gp_id}" Recording Path "\${network_rec_path}" Should Be Added
    Insert Many Cameras

Settings For Weekly Day-Based Time Frame With Many Cameras (00~08 + 18~24)
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info             repeat_mode=Weekly        dl_mode=SingleDay   interval_list=${weekly_daily_timeinterval}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${weekly_daily_timeinterval}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Weekly Day-based
    Insert Many Cameras

Settings For Weekly Time Frame With Many Cameras (12~24 + 00~18 + 00~12)
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info             repeat_mode=Weekly        dl_mode=Period      interval_list=${weekly_period_timeinterval}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${weekly_period_timeinterval}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Weekly Period Settings
    Insert Many Cameras

Settings For Motion Event Recording
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info             rec_mode=Event            motion=true
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Mode Should Be "\${rec_mode_event}"
    ...                             AND                     Timeframe ID "3" Motion Trigger Should Be "\${1}"
    Insert Many Cameras

Settings For Alarm By Motion Trigger Recording
    Delete Time Frame In Schedule List
    Set Local Recording Path
    Insert Many Cameras
    Run Keyword             ${remote_vast_controller_lib}.Set Time Frame Info       timecard_id=0       time_frame_type=alarm           repeat_mode=Daily
    ...                     rec_mode=Continuous
    ${list} =               Create List
    ${cam_entry1} =         Create List
    Append To List          ${cam_entry1}        C_1
    Append To List          ${cam_entry1}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry1}
    ${cam_entry2} =         Create List
    Append To List          ${cam_entry2}        C_2
    Append To List          ${cam_entry2}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry2}
    ${cam_entry3} =         Create List
    Append To List          ${cam_entry3}        C_3
    Append To List          ${cam_entry3}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry3}
    ${cam_entry4} =         Create List
    Append To List          ${cam_entry4}        C_4
    Append To List          ${cam_entry4}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry4}
    ${cam_entry5} =         Create List
    Append To List          ${cam_entry5}        C_5
    Append To List          ${cam_entry5}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry5}
    ${cam_entry6} =         Create List
    Append To List          ${cam_entry6}        C_6
    Append To List          ${cam_entry6}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry6}
    ${cam_entry7} =         Create List
    Append To List          ${cam_entry7}        C_7
    Append To List          ${cam_entry7}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry7}
    ${cam_entry8} =         Create List
    Append To List          ${cam_entry8}        C_8
    Append To List          ${cam_entry8}        ${default_storage_gp_id}
    Append To List          ${list}              ${cam_entry8}
    Run Keyword             ${remote_vast_controller_lib}.Add Alarm List          timecard_id=0         event_trigger_cam_name=C_1      recording_cam=${list}
    ...                     related_devices=C_1 C_2 C_3 C_4 C_5 C_6 C_7 C_8       device_ref_name=${cam_name}-9001 ${cam_name}-9002 ${cam_name}-9003 ${cam_name}-9004 ${cam_name}-9005 ${cam_name}-9006 ${cam_name}-9007 ${cam_name}-9008

Settings For Weekly Time Frame Overlapped With No Recording Daily Time Frame
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info           repeat_mode=Daily
    ...                             rec_mode=None               interval_list=${no_rec_timeinterval}
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info             repeat_mode=Weekly        dl_mode=Period      interval_list=${weekly_period_timeinterval_overlap}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${no_rec_timeinterval}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Daily Settings
    ...                             AND                     Timeframe ID "4" Recording Time Interval Should Be "${weekly_period_timeinterval_overlap}"
    ...                             AND                     Timeframe ID "4" Recording Repeat Frequency Should Be Weekly Period Settings
    Insert Many Cameras

Settings For Weekly Day-Based Time Frmae Overlapped Recording
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             rec_mode=None           dl_mode=SingleDay               interval_list=${no_rec_timeinterval_weekly_daybased}
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             dl_mode=SingleDay       interval_list=${weekly_daily_timeinterval}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${no_rec_timeinterval_weekly_daybased}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Weekly Day-based
    ...                             AND                     Timeframe ID "4" Recording Time Interval Should Be "${weekly_daily_timeinterval}"
    ...                             AND                     Timeframe ID "4" Recording Repeat Frequency Should Be Weekly Day-based
    Insert Many Cameras

Settings For Weekly Time Frmae Overlapped Recording
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             rec_mode=None           dl_mode=Period                  interval_list=${no_rec_weekly_period_overlap}
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             dl_mode=Period          interval_list=${weekly_period_timeinterval_overlap}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${no_rec_weekly_period_overlap}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Weekly Period Settings
    ...                             AND                     Timeframe ID "4" Recording Time Interval Should Be "${weekly_period_timeinterval_overlap}"
    ...                             AND                     Timeframe ID "4" Recording Repeat Frequency Should Be Weekly Period Settings
    Insert Many Cameras

Settings For Weekly Day-Based Overlapped With No Recording Weekly Period Time Frame
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             rec_mode=None           dl_mode=Period                  interval_list=${no_rec_weekly_period_overlap_with_day_based}
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             dl_mode=SingleDay          interval_list=${weekly_daily_timeinterval}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${no_rec_weekly_period_overlap_with_day_based}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Weekly Period Settings
    ...                             AND                     Timeframe ID "4" Recording Time Interval Should Be "${weekly_daily_timeinterval}"
    ...                             AND                     Timeframe ID "4" Recording Repeat Frequency Should Be Weekly Day-based
    Insert Many Cameras

Settings For Weekly Day-Based Overlapped With No Recording Daily Time Frame
    Set Local Recording Path
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Daily
    ...                             rec_mode=None           dl_mode=SingleDay               interval_list=${no_rec_daily__overlap_with_day_based}
    Run Keyword                     ${remote_vast_controller_lib}.Set Time Frame Info       repeat_mode=Weekly
    ...                             dl_mode=SingleDay          interval_list=${weekly_daily_timeinterval}
    Run Keyword                     ${remote_vast_controller_lib}.Edit Recording Time Frame
    Wait Until Keyword Succeeds     ${wait_for_db_query}    ${db_retry_interval}
    ...                             Run Keywords            Timeframe ID "3" Recording Time Interval Should Be "${no_rec_daily__overlap_with_day_based}"
    ...                             AND                     Timeframe ID "3" Recording Repeat Frequency Should Be Daily Settings
    ...                             AND                     Timeframe ID "4" Recording Time Interval Should Be "${weekly_daily_timeinterval}"
    ...                             AND                     Timeframe ID "4" Recording Repeat Frequency Should Be Weekly Day-based
    Insert Many Cameras

Insert Many Cameras
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9001            host_ip=${camera_ip}        port=9001
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9001" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9002            host_ip=${camera_ip}        port=9002
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9002" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9003            host_ip=${camera_ip}        port=9003
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9003" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9004            host_ip=${camera_ip}        port=9004
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9004" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9005            host_ip=${camera_ip}        port=9005
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9005" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9006            host_ip=${camera_ip}        port=9006
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9006" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9007            host_ip=${camera_ip}        port=9007
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9007" Should Be Added To Storage Group "${default_storage_gp_id}"
    Run Keyword                     ${remote_vast_controller_lib}.Insert Camera                   ref_name=${cam_ref_name}-9008            host_ip=${camera_ip}        port=9008
    ...                             rec_gp_id=${default_storage_gp_id}
    Wait Until Keyword Succeeds     ${wait_for_db_query}            ${db_retry_interval}
    ...                             "${cam_ref_name}-9008" Should Be Added To Storage Group "${default_storage_gp_id}"

Set up camera simulator
    Copy VirtualLiveTable.xls accordingly
    Kill camera simulator processes
    Run camera simulator

Kill camera simulator processes
    Run                 taskkill /f /im mongoose.exe
    Run                 taskkill /f /im darwin.exe

Run camera simulator
    Start Process       .\\Tool\\CameraSimulator\\bin\\run.bat

Copy VirtualLiveTable.xls accordingly
    Copy File           ./Tool/CameraSimulator/bin/${TEST NAME}.xls     ./Tool/CameraSimulator/bin/VirtualLiveTable.xls
