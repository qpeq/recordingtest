*** Settings ***
Variables                               ../../TestLibrary/RemoteServerSettings.py
Resource                                ../../Resource/ServerSettings.robot

*** Variables ***
${short_term_latest_rec_srv_path}       \\\\172.18.101.4\\upload\\RecordingServerTest\\VMSRecordingServer.exe
${long_term_latest_rec_srv_path}        \\\\172.18.101.4\\upload\\RecordingServerTest\\VMSRecordingServer.exe

*** Test Cases ***
Short-term VAST Server
    [Setup]         Import VAST Controller Remote Library       ${REMOTE_SERVER_IP}              VASTController
    Update VAST Recording Server "${short_term_latest_rec_srv_path}"

Long-term VAST Server For 24HR Continuous Recording
    [Setup]         Import VAST Controller Remote Library       ${LT_CONTINUOUS_REC_IP}             VASTController1
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Delete Recording Data Older Than # Days
    [Setup]         Import VAST Controller Remote Library       ${LT_DELETE_REC_DATE}               VASTController2
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Recording To Network Storage Path
    [Setup]         Import VAST Controller Remote Library       ${LT_NETWORK_STORAGE_REC_PATH}      VASTController3
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Day-Based Recording Time Frame
    [Setup]         Import VAST Controller Remote Library       ${LT_WEEKLY_DAY_BASED}              VASTController4
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Recording Time Frame
    [Setup]         Import VAST Controller Remote Library       ${LT_WEEKLY_PERIOD_A_WEEK}          VASTController5
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Motion Trigger Recording
    [Setup]         Import VAST Controller Remote Library       ${LT_MOTION_EVENT}                  VASTController6
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Alarm Trigger Recording
    [Setup]         Import VAST Controller Remote Library       ${LT_ALARM_TRIGGER}                 VASTController7
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Time Frame (12-24 + 08-18 + 00-12) Overlapped With No Recording Daily Time Frmae (12-18)
    [Setup]         Import VAST Controller Remote Library       ${LT_WEEKY_OVERLAP_DAILY}           VASTController8
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Day-Based Time Frmae (00-08 + 18-24) Overlapped With No Recording (06-20)
    [Setup]         Import VAST Controller Remote Library       ${LT_2_WEEKLY_DAY_BASED_OVERLAP}    VASTController9
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Time Frame (12-24 + 08-18 + 00-12) Overlapped With No Recording (18-24 + 12-18 + 6-12)
    [Setup]         Import VAST Controller Remote Library       ${LT_2_WEEKLY_OVERLAP}              VASTController10
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Day-Based Time Frmae (00-08 + 18-24) Overlapped No Recording Weekly Period Time Frame (00-08 + 06-20 + 18-24)
    [Setup]         Import VAST Controller Remote Library       ${LT_WEEKLY_DAY_BASED_OVERLAP_WEEKLY}   VASTController11
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

Long-term VAST Server For Weekly Day-Based Time Frmae (00-08 + 18-24) Overlapped No Recording Daily Time Frame (06-18)
    [Setup]         Import VAST Controller Remote Library       ${LT_WEEKLY_DAY_BASED_OVERLAP_DAILY}    VASTController12
    Update VAST Recording Server "${long_term_latest_rec_srv_path}"

*** Keywords ***
Update VAST Recording Server "${rec_path}"
    Run Keyword     ${remote_vast_controller_lib}.Update Recording Server       ${rec_path}