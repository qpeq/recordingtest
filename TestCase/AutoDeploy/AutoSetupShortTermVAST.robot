*** Settings ***
Variables       									../../TestLibrary/RemoteServerSettings.py
Resource        									../../Resource/BasicFunctions.robot
Resource                                            ../../Resource/ServerSettings.robot
Resource                                            ../../Resource/DBValueVerification.robot
Test Teardown                                       Disconnect To VAST Database

*** Test Cases ***
Short-term VAST Server
	Install VAST From The Default Location
    Copy VAST Default Settings
    [Teardown]      Log     Installation finished...
