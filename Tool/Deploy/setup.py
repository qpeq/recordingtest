from py2exe.build_exe import py2exe
from distutils.core import setup

try:
    try:
        import py2exe.mf as modulefinder
    except ImportError:
        import modulefinder
    import win32com, sys
    for p in win32com.__path__[1:]:
        modulefinder.AddPackagePath("win32com", p)
    for extra in ["win32com.shell"]: #,"win32com.mapi"
        __import__(extra)
        m = sys.modules[extra]
        for p in m.__path__[1:]:
            modulefinder.AddPackagePath(extra, p)
except ImportError:
    # no build path setup, no worries.
    pass


setup(options = {"py2exe": {
                        "compressed": True,
                        "optimize": 2,
                        'bundle_files': 1,
                        "dll_excludes": ["w9xpopen.exe", 'axscript.pyd'],
                        "includes": ['os', 'sys', 'shutil', 'ctypes', 'subprocess', 'win32api', 'win32con', 'win32com', 'winshell']
                    }},

        zipfile = None,

        console = [
        {
            "script": "simple_rpc.py",
            "dest_base" : "simple_rpc"
        }
    ],)
