import base64
import xmlrpclib as rpc

long_term_vm_ip_01 = '172.18.141.12'
long_term_vm_ip_02 = '172.18.141.13'
long_term_vm_ip_03 = '172.18.141.7'
long_term_vm_ip_04 = '172.18.141.15'
long_term_vm_ip_05 = '172.18.141.22'
long_term_vm_ip_06 = '172.18.141.23'
long_term_vm_ip_07 = '172.18.141.24'
long_term_vm_ip_08 = '172.18.141.25'
long_term_vm_ip_09 = '172.18.141.32'
long_term_vm_ip_10 = '172.18.141.33'
long_term_vm_ip_11 = '172.18.141.34'
long_term_vm_ip_12 = '172.18.141.35'
short_term_vm_ip = '172.18.141.6'

all_servers = [short_term_vm_ip, long_term_vm_ip_01, long_term_vm_ip_02, long_term_vm_ip_03, long_term_vm_ip_04,
               long_term_vm_ip_05, long_term_vm_ip_06, long_term_vm_ip_07, long_term_vm_ip_08, long_term_vm_ip_09,
               long_term_vm_ip_10, long_term_vm_ip_11, long_term_vm_ip_12]

long_term_servers = [long_term_vm_ip_01, long_term_vm_ip_02, long_term_vm_ip_03, long_term_vm_ip_04,
                     long_term_vm_ip_05, long_term_vm_ip_06, long_term_vm_ip_07, long_term_vm_ip_08, long_term_vm_ip_09,
                     long_term_vm_ip_10, long_term_vm_ip_11, long_term_vm_ip_12]

STRESS_TEST_SERVER = '172.18.52.12'

def remote_execute(ip, script_str):
    server = rpc.ServerProxy('http://%s:22222/' % ip)
    server.execute(rpc.Binary(base64.b64encode(script_str)))


def remote_execute_ex(*args):
    remote_execute(args[0], args[1])


def deploy_short_term_test():
    script = ''
    with open('deploy.py', 'r') as f:
        script = f.read()

    remote_execute(short_term_vm_ip, script)


def deploy_long_term_test():
    script = ''
    with open('deploy.py', 'r') as f:
        script = f.read()

    for ip in long_term_servers:
        remote_execute(ip, script)


def deploy_stress_test():
    script = ''
    with open('deploy_stress.py', 'r') as f:
        script = f.read()

    remote_execute(STRESS_TEST_SERVER, script)


if __name__ == '__main__':
    deploy_stress_test()

    # deploy_short_term_test()
    # deploy_long_term_test()
