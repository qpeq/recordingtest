import os
import sys
import base64
import subprocess
from SimpleXMLRPCServer import SimpleXMLRPCServer
from win32com.client import Dispatch


def create_shortcut_to_startup(name, target, working_dir):
    shell = Dispatch('WScript.Shell')
    startup_path = shell.SpecialFolders("AllUsersStartup")
    path = os.path.join(startup_path, '%s.lnk' % name)
    shortcut = shell.CreateShortcut(path)
    shortcut.Targetpath = target
    shortcut.WorkingDirectory = working_dir
    shortcut.save()
    print 'A shortcut for %s has been created to startup menu' % target


def execute(bin_obj):
    origin_script = base64.b64decode(bin_obj.data)
    print origin_script
    script_file_name = 'temp_script.py'
    with open(script_file_name, 'w') as f:
        f.write(origin_script)
    subprocess.check_call(['simple_rpc.exe', 'runscript', script_file_name])
    os.remove(script_file_name)


def run_xmlrpcserver():
    server = SimpleXMLRPCServer(("0.0.0.0", 22222), allow_none=True)
    print "Simple Python RPC server listening on port 22222..."
    server.register_function(create_shortcut_to_startup, "create_shortcut_to_startup")
    server.register_function(execute, "execute")
    server.serve_forever()



if __name__ == '__main__':
    if len(sys.argv) == 3 and sys.argv[1] == 'runscript':
        execfile(sys.argv[2])
    else:
        run_xmlrpcserver
