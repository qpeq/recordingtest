import os
import socket
import shutil
import subprocess
from win32com.client import Dispatch


DRIVE_LETTER = 'E'
ROOT_DIR_SEP = ':\\'
if not os.access(DRIVE_LETTER + ROOT_DIR_SEP, os.W_OK):
    DRIVE_LETTER = 'F'

TARGET_PATH = '%s:\\VAST_Recording_Test' % DRIVE_LETTER
CAM_SIM_PATH = '%s:\\VAST_Recording_Test\\Tool\\CameraSimulator' % DRIVE_LETTER
CAM_SIM_RUN_PATH = '%s:\\VAST_Recording_Test\\Tool\\CameraSimulator\\run.bat' % DRIVE_LETTER
TEST_FRAMEWORK_SOURCE_PATH = '\\\\172.18.101.4\\upload\\gavin\\deploy\\VAST_Recording_Test'
TEST_SERVICE_BATCH_PATH = '%s:\\VAST_Recording_Test\\short_term_test_library_startup.bat' % DRIVE_LETTER

DEFAULT_VIRTUAL_TABLE_FILENAME = 'VirtualLiveTable.xls'
VIRTUAL_TABLE_EXT = '.xls'

long_term_vm_ip_01 = '172.18.141.12'
long_term_vm_ip_02 = '172.18.141.13'
long_term_vm_ip_03 = '172.18.141.7'
long_term_vm_ip_04 = '172.18.141.15'
long_term_vm_ip_05 = '172.18.141.22'
long_term_vm_ip_06 = '172.18.141.23'
long_term_vm_ip_07 = '172.18.141.24'
long_term_vm_ip_08 = '172.18.141.25'
long_term_vm_ip_09 = '172.18.141.32'
long_term_vm_ip_10 = '172.18.141.33'
long_term_vm_ip_11 = '172.18.141.34'
long_term_vm_ip_12 = '172.18.141.35'
short_term_vm_ip = '172.18.141.6'

virtual_table_dict = {long_term_vm_ip_01: 'LT01',
                      long_term_vm_ip_02: 'LT02',
                      long_term_vm_ip_03: 'LT03',
                      long_term_vm_ip_04: 'LT04',
                      long_term_vm_ip_05: 'LT05',
                      long_term_vm_ip_06: 'LT06',
                      long_term_vm_ip_07: 'LT07',
                      long_term_vm_ip_08: 'LT08',
                      long_term_vm_ip_09: 'LT09',
                      long_term_vm_ip_10: 'LT10',
                      long_term_vm_ip_11: 'LT11',
                      long_term_vm_ip_12: 'LT12',
                      short_term_vm_ip: 'ST01'
                      }


def start_vast_testing_services():
    os.startfile(TEST_SERVICE_BATCH_PATH)


def start_camera_simulator():
    os.startfile(CAM_SIM_RUN_PATH)


def create_shortcut_to_startup(name, target, working_dir):
    shell = Dispatch('WScript.Shell')
    startup_path = shell.SpecialFolders("AllUsersStartup")
    path = os.path.join(startup_path, '%s.lnk' % name)
    shortcut = shell.CreateShortcut(path)
    shortcut.Targetpath = target
    shortcut.WorkingDirectory = working_dir
    shortcut.save()


def kill_process(process_name):
    try:
        subprocess.check_call(['taskkill', '/f', '/im', process_name])
    except:
        pass


def get_ip_address():
    return socket.gethostbyname(socket.gethostname())


def replace_virtual_live_table_for_cam_sim():
    filename = virtual_table_dict.get(get_ip_address())
    if filename is not None:
        src = os.path.join(CAM_SIM_PATH, filename + VIRTUAL_TABLE_EXT)
        dst = os.path.join(CAM_SIM_PATH, DEFAULT_VIRTUAL_TABLE_FILENAME)
        shutil.copy(src, dst)
    else:
        print get_ip_address(), virtual_table_dict


kill_process('python.exe')
kill_process('darwin.exe')
kill_process('mongoose.exe')

try:
    shutil.rmtree(TARGET_PATH)
except Exception as e:
    print e

shutil.copytree(TEST_FRAMEWORK_SOURCE_PATH, TARGET_PATH)

create_shortcut_to_startup('cam_sim', CAM_SIM_RUN_PATH, CAM_SIM_PATH)
create_shortcut_to_startup('test_services', TEST_SERVICE_BATCH_PATH, TARGET_PATH)
create_shortcut_to_startup('simple_rpc', '%s:\\simple_rpc.exe' % DRIVE_LETTER, '%s:\\' % DRIVE_LETTER)

replace_virtual_live_table_for_cam_sim()

start_camera_simulator()
start_vast_testing_services()
