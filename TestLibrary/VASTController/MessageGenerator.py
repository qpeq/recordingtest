__author__ = 'sunny.sun'

# Server information.
SEVER_INFO = ('<ServerInfo>'
                   '<IP>%(IP)s</IP>'
                   '<Port>%(Port)s</Port>'
                   '<URL>%(URL)s</URL>'  # /cfgserver.cgi, /evtserver.cgi
                   '<UserName>%(UserName)s</UserName>'
                   '<Password>%(Password)s</Password>'
                   '<ServerUserName>%(ServerUserName)s</ServerUserName>'
                   '<ServerPassword>%(ServerPassword)s</ServerPassword>'
                   '<NormalPost>False</NormalPost>'
                   '<ConnectionTimeout>30000</ConnectionTimeout>'
                   '<RequestTimeout>30000</RequestTimeout>'
                   '<EnableProxy>False</EnableProxy>'
                   '<ProxyIP>127.0.0.1</ProxyIP>'
                   '<ProxyPort>80</ProxyPort>'
                   '<ProxyUserName>1</ProxyUserName>'
                   '<ProxyPassword>1</ProxyPassword>'
              '</ServerInfo>'
              )

# ======================================================================================================================
# Recording group related.
# ======================================================================================================================
# Add/Edit storage group.
COMMAND_ADD_GROUP = ('<Command>'
                         '<CmdName>SetRecGroup</CmdName>'
                         '<CmdBody>'
                             '<SetRecGroup>'
                                 '<Rev>%(Rev)s</Rev>'
                                 '<%(Action)s>'  # AddGrp (add storage gp), SetGrp(edit storage gp)
                                     '<Seq>1</Seq>'
                                     '<GrpID>%(GrpID)s</GrpID>'
                                     '<GrpName>%(GrpName)s</GrpName>'
                                     '<Cycle>%(Cycle)s</Cycle>'
                                     '<TmBaseCycle>%(TmBaseCycle)s</TmBaseCycle>'
                                     '<ReserveTime>%(ReserveTime)s</ReserveTime>'
                                 '</%(Action)s>'
                             '</SetRecGroup>'
                         '</CmdBody>'
                     '</Command>')

# Remove storage group.
COMMAND_REMOVE_GROUP = ('<Command>'
                            '<CmdName>SetRecGroup</CmdName>'
                            '<CmdBody>'
                                '<SetRecGroup>'
                                    '<Rev>%(Rev)s</Rev>'
                                    '<RmGrp>'
                                        '<Seq>1</Seq>'
                                        '<GrpID>%(GrpID)s</GrpID>'
                                        '<Method>RemoveEntry</Method>'
                                    '</RmGrp>'
                                '</SetRecGroup>'
                            '</CmdBody>'
                        '</Command>')

# Insert NAS Server
COMMAND_INSERT_NAS_SERVER = ('<Command>'
                                 '<CmdName>InsertNASServer</CmdName>'
                                 '<CmdBody>'
                                     '<InsertNASServer>'
                                         '<Host>%(Host)s</Host>'
                                         '<HostDomain>%(HostDomain)s</HostDomain>'
                                         '<User>%(User)s</User>'
                                         '<Pwd>%(Pwd)s</Pwd>'
                                     '</InsertNASServer>'
                                 '</CmdBody>'
                             '</Command>')

# ======================================================================================================================
# Recording path related.
# ======================================================================================================================
COMMAND_GET_REC_GROUP = ('<Command>'
                               '<CmdName>GetRecGroup</CmdName>'
                               '<CmdBody>'
                                   '<GetRecGroup/>'
                               '</CmdBody>'
                           '</Command>')

# Remove recording path.
COMMAND_REMOVE_REC_PATH = ('<Command>'
                               '<CmdName>SetRecGroup</CmdName>'
                               '<CmdBody>'
                                   '<SetRecGroup>'
                                       '<Rev>%(Rev)s</Rev>'
                                       '<RmPath>'
                                           '<Seq>1</Seq>'
                                           '<GrpID>%(GrpID)s</GrpID>'
                                           '<Path>%(Path)s</Path>'
                                           '<Method>%(Method)s</Method>'  # RemoveMedia, RemoveEntry
                                       '</RmPath>'
                                   '</SetRecGroup>'
                               '</CmdBody>'
                           '</Command>')

# Add recording path.
COMMAND_SET_REC_PATH = ('<Command>'
                            '<CmdName>SetRecGroup</CmdName>'
                            '<CmdBody>'
                                '<SetRecGroup>'
                                    '<Rev>%(Rev)s</Rev>'
                                    '<%(Action)s>'  # AddPath, SetPath
                                        '<Seq>1</Seq>'
                                        '<GrpID>%(GrpID)s</GrpID>'
                                        '<Path>%(Path)s</Path>'
                                        '<ReserveSpace>%(ReserveSpace)s</ReserveSpace>'
                                        '<Type>%(Type)s</Type>'
                                        '%(RecoveryMode)s'  # <RecoveryMode>true</RecoveryMode>
                                    '</%(Action)s>'
                                '</SetRecGroup>'
                            '</CmdBody>'
                        '</Command>')

# Edit recording path.
COMMAND_EDIT_REC_PATH = ('<Command>'
                              '<CmdName>SetRecGroup</CmdName>'
                              '<CmdBody>'
                                  '<SetRecGroup>'
                                      '<Rev>%(Rev)s</Rev>'
                                      '<RmPath>'
                                          '<Seq>%(RmPathSeq)s</Seq>'
                                          '<GrpID>%(GrpID)s</GrpID>'
                                          '<Path>%(OriginalPath)s</Path>'
                                          '<Method>%(Method)s</Method>'
                                      '</RmPath>'
                                      '<AddPath>'
                                          '<Seq>%(AddPathSeq)s</Seq>'
                                          '<GrpID>%(GrpID)s</GrpID>'
                                          '<Path>%(NewPath)s</Path>'
                                          '<ReserveSpace>%(ReserveSpace)s</ReserveSpace>'
                                          '<Type>%(Type)s</Type>'
                                      '</AddPath>'
                                  '</SetRecGroup>'
                              '</CmdBody>'
                          '</Command>')

# ======================================================================================================================
# Camera related.
# ======================================================================================================================
COMMAND_INSERT_CAMERA = ('<Command>'
                             '<CmdName>InsertCam</CmdName>'
                                 '<CmdBody>'
                                     '<InsertCam>'
                                         '<RefName>%(RefName)s</RefName>'
                                         '<PseudoBrand>VIVOTEK</PseudoBrand>'
                                         '<Brand>1263818326</Brand>'
                                         '<Module>757935405</Module>'
                                         '<HostIP>%(HostIP)s</HostIP>'
                                         '<User>%(User)s</User>'
                                         '<Pwd>%(Pwd)s</Pwd>'
                                         '<Port>%(Port)s</Port>'
                                         '<HTTPSPort>443</HTTPSPort>'
                                         '<UseSSL>false</UseSSL>'
                                         '<MAC>1002D108F422</MAC>'
                                         '<Protocol>TCP</Protocol>'
                                         '<Channel>1</Channel>'
                                         '<DefViewStm>1</DefViewStm>'
                                         '<AddIntoRecGroup>%(AddIntoRecGroup)s</AddIntoRecGroup>'
                                         '<RecSettings>'
                                             '<Stream>%(RecStream)s</Stream>'
                                             '<EnableTmShift>%(EnableTmShift)s</EnableTmShift>'
                                             '<TmShiftPreEvent>10</TmShiftPreEvent>'
                                             '<MinPreBuffer>3</MinPreBuffer>'
                                             '<PreEvent>%(PreEvent)s</PreEvent>'
                                             '<PostEvent>%(PostEvent)s</PostEvent>'
                                             '<EnableSeamless>%(EnableSeamless)s</EnableSeamless>'
                                             '<EnableKeyFrameOnly>false</EnableKeyFrameOnly>'
                                         '</RecSettings>'
                                         '<VideoSettings>'
                                             '<VideoSensorModeDesc></VideoSensorModeDesc>'
                                             '<FOV>1280x720</FOV>'
                                             '<FOVPosition>0,0</FOVPosition>'
                                             '<Video>'
                                                 '<Stream>%(VideoStream)s</Stream>'
                                                 '<CodecType>H264</CodecType>'
                                                 '<Resolution>640x480</Resolution>'
                                                 '<FrameRate>30</FrameRate>'
                                                 '<VideoQuality>vbr</VideoQuality>'
                                                 '<BitRate>20000</BitRate>'
                                                 '<Quality>1</Quality>'
                                                 '<SmartStreamForegroundQuality>-1</SmartStreamForegroundQuality>'
                                                 '<SmartStreamBackgroundQuality>-1</SmartStreamBackgroundQuality>'
                                                 '<SmartStreamMaxBitRate>0</SmartStreamMaxBitRate>'
                                             '</Video>'
                                         '</VideoSettings>'
                                         '<AudioSettings>'
                                             '<Audio>'
                                                 '<Enable>false</Enable>'
                                                 '<CodecType>g711</CodecType>'
                                                 '<AACBitRate>-1</AACBitRate>'
                                                 '<GSMBitRate>-1</GSMBitRate>'
                                                 '<G726BitRate>-1</G726BitRate>'
                                                 '<Mode></Mode>'
                                             '</Audio>'
                                         '</AudioSettings>'
                                         '<RemoteFocusSettings>'
                                             '<RemoteFocus>'
                                                 '<Enable>false</Enable>'
                                                 '<Status>0.000000</Status>'
                                                 '<Position>0,0</Position>'
                                                 '<Size>0x0</Size>'
                                                 '<End>0.000000</End>'
                                             '</RemoteFocus>'
                                         '</RemoteFocusSettings>'
                                         '<UseNTPServer>true</UseNTPServer>'
                                         '<NTPSettings>'
                                             '<NTPSrv>172.18.140.21</NTPSrv>'
                                             '<NTPInterval>86400</NTPInterval>'
                                         '</NTPSettings>'
                                         '<PPTZSettings>'
                                             '<PPTZEnable>0</PPTZEnable>'
                                             '<AutoTrackingEnable>0</AutoTrackingEnable>'
                                         '</PPTZSettings>'
                                     '</InsertCam>'
                                 '</CmdBody>'
                             '</Command>')

COMMAND_INST_REAL_CAMERA = ('<Command>'
                                '<CmdName>InsertCam</CmdName>'
                                '<CmdBody>'
                                    '<InsertCam>'
                                        '<RefName>c_Mega Network Camera</RefName>'
                                        '<PseudoBrand>VIVOTEK</PseudoBrand>'
                                        '<Brand>1263818326</Brand>'
                                        '<Module>757935405</Module>'
                                        '<HostIP>172.18.203.42</HostIP>'
                                        '<User>root</User>'
                                        '<Pwd>7661737474657374</Pwd>'
                                        '<Port>80</Port>'
                                        '<HTTPSPort>443</HTTPSPort>'
                                        '<UseSSL>false</UseSSL>'
                                        '<MAC>0002D1149E2B</MAC>'
                                        '<Protocol>TCP</Protocol>'
                                        '<Channel>1</Channel>'
                                        '<DefViewStm>1</DefViewStm>'
                                        '<AddIntoRecGroup>1</AddIntoRecGroup>'
                                        '<RecSettings>'
                                            '<Stream>1</Stream>'
                                            '<EnableTmShift>%(EnableTmShift)s</EnableTmShift>'
                                            '<PreEvent>10</PreEvent>'
                                            '<PostEvent>10</PostEvent>'
                                            '<EnableSeamless>%(EnableSeamless)s</EnableSeamless>'
                                            '<EnableKeyFrameOnly>false</EnableKeyFrameOnly>'
                                        '</RecSettings>'
                                        '<VideoSettings>'
                                            '<VideoSensorModeDesc></VideoSensorModeDesc>'
                                            '<FOV>1280x1024</FOV>'
                                            '<FOVPosition>0,0</FOVPosition>'
                                            '<Video>'
                                                '<Stream>0</Stream>'
                                                '<CodecType>H264</CodecType>'
                                                '<Resolution>1280x1024</Resolution>'
                                                '<FrameRate>30.000000</FrameRate>'
                                                '<VideoQuality>cbr</VideoQuality>'
                                                '<BitRate>2048000</BitRate>'
                                                '<Quality>5</Quality>'
                                                '<SmartStreamForegroundQuality>-1</SmartStreamForegroundQuality>'
                                                '<SmartStreamBackgroundQuality>-1</SmartStreamBackgroundQuality>'
                                                '<SmartStreamMaxBitRate>0</SmartStreamMaxBitRate>'
                                            '</Video>'
                                            '<Video>'
                                                '<Stream>1</Stream>'
                                                '<CodecType>H264</CodecType>'
                                                '<Resolution>800x600</Resolution>'
                                                '<FrameRate>15.000000</FrameRate>'
                                                '<VideoQuality>vbr</VideoQuality>'
                                                '<BitRate>2048000</BitRate>'
                                                '<Quality>3</Quality>'
                                                '<SmartStreamForegroundQuality>-1</SmartStreamForegroundQuality>'
                                                '<SmartStreamBackgroundQuality>-1</SmartStreamBackgroundQuality>'
                                                '<SmartStreamMaxBitRate>0</SmartStreamMaxBitRate>'
                                            '</Video>'
                                            '<Video>'
                                                '<Stream>2</Stream>'
                                                '<CodecType>H264</CodecType>'
                                                '<Resolution>640x480</Resolution>'
                                                '<FrameRate>15.000000</FrameRate>'
                                                '<VideoQuality>vbr</VideoQuality>'
                                                '<BitRate>2048000</BitRate>'
                                                '<Quality>3</Quality>'
                                                '<SmartStreamForegroundQuality>-1</SmartStreamForegroundQuality>'
                                                '<SmartStreamBackgroundQuality>-1</SmartStreamBackgroundQuality>'
                                                '<SmartStreamMaxBitRate>0</SmartStreamMaxBitRate>'
                                            '</Video>'
                                            '<Video>'
                                                '<Stream>3</Stream>'
                                                '<CodecType>H264</CodecType>'
                                                '<Resolution>640x480</Resolution>'
                                                '<FrameRate>15.000000</FrameRate>'
                                                '<VideoQuality>vbr</VideoQuality>'
                                                '<BitRate>2048000</BitRate>'
                                                '<Quality>3</Quality>'
                                                '<SmartStreamForegroundQuality>-1</SmartStreamForegroundQuality>'
                                                '<SmartStreamBackgroundQuality>-1</SmartStreamBackgroundQuality>'
                                                '<SmartStreamMaxBitRate>0</SmartStreamMaxBitRate>'
                                            '</Video>'
                                        '</VideoSettings>'
                                        '<AudioSettings>'
                                            '<Audio>'
                                            '<Enable>true</Enable>'
                                            '<CodecType>aac4</CodecType>'
                                            '<AACBitRate>16000</AACBitRate>'
                                            '<GSMBitRate>12200</GSMBitRate>'
                                            '<G726BitRate>-1</G726BitRate>'
                                            '<Mode>pcmu</Mode>'
                                            '</Audio>'
                                        '</AudioSettings>'
                                        '<RemoteFocusSettings>'
                                            '<RemoteFocus>'
                                                '<Enable>false</Enable>'
                                                '<Status>0.000000</Status>'
                                                '<Position>0,0</Position>'
                                                '<Size>0x0</Size>'
                                                '<End>0.000000</End>'
                                            '</RemoteFocus>'
                                        '</RemoteFocusSettings>'
                                        '<UseNTPServer>true</UseNTPServer>'
                                        '<NTPSettings>'
                                            '<NTPSrv>172.18.141.6</NTPSrv>'
                                            '<NTPInterval>86400</NTPInterval>'
                                        '</NTPSettings>'
                                        '<PPTZSettings>'
                                            '<PPTZEnable>0</PPTZEnable>'
                                            '<AutoTrackingEnable>0</AutoTrackingEnable>'
                                        '</PPTZSettings>'
                                    '</InsertCam>'
                                '</CmdBody>'
                            '</Command>')

COMMAND_REMOVE_CAMERA = ('<Command>'
                             '<CmdName>RemoveCam</CmdName>'
                             '<CmdBody>'
                                 '<RemoveCam>'
                                     '<CamName>%(CamName)s</CamName>'
                                     '<Method>%(Method)s</Method>'
                                 '</RemoveCam>'
                             '</CmdBody>'
                         '</Command>')

COMMAND_DETECT_CAMERA = ('<Command>'
                            '<CmdName>DetectCam</CmdName>'
                            '<CmdBody>'
                                '<DetectCam>'
                                    '<Brand>%(Brand)s</Brand>'
                                    '<Module>%(Module)s</Module>'
                                    '<HostIP>%(HostIP)s</HostIP>'
                                    '<User>%(User)s</User>'
                                    '<Pwd>%(Pwd)s</Pwd>'
                                    '<Port>%(Port)s</Port>'
                                    '<HTTPSPort>%(HTTPSPort)s</HTTPSPort>'
                                    '<Protocol>%(Protocol)s</Protocol>'
                                '</DetectCam>'
                            '</CmdBody>'
                         '</Command>')

# ======================================================================================================================
# Manual recording related.
# ======================================================================================================================
COMMAND_MANUAL_RECORDING = ('<Command>'
                                      '<CmdName>%(Action)s</CmdName>'
                                      '<CmdBody>'
                                          '<%(Action)s>' #StartRec, StopRec
                                              '<Entry>'
                                                  '<CamName>%(CamName)s</CamName>'
												  '<GrpID>%(GrpID)s</GrpID>'
                                              '</Entry>'
                                          '</%(Action)s>'
                                      '</CmdBody>'
                                  '</Command>')

# ======================================================================================================================
# Schedule list related.
# ======================================================================================================================
COMMAND_INSERT_REC_TIMECARD = ('<Command>'
                                   '<CmdName>InsRecTmcard</CmdName>'
                                   '<CmdBody>'
                                       '<InsRecTmcard>'
                                           '<Tmcard>'
                                               '<Revision>0</Revision>'
                                               '<TmcardID>0</TmcardID>'
                                               '<Name>%(Name)s</Name>'
                                           '</Tmcard>'
                                       '</InsRecTmcard>'
                                   '</CmdBody>'
                               '</Command>')

COMMAND_DELETE_TIMECARD = ('<Command>'
                               '<CmdName>DelTmcard</CmdName>'
                               '<CmdBody>'
                                   '<DelTmcard>'
                                       '<Revision>%(Revision)s</Revision>'
                                       '<TmcardID>%(TmcardID)s</TmcardID>'
                                   '</DelTmcard>'
                               '</CmdBody>'
                           '</Command>')

COMMAND_UPDATE_TIMECARD = ('<Command>'
                               '<CmdName>UpdRecTmcard</CmdName>'
                               '<CmdBody>'
                                   '<UpdRecTmcard>'
                                       '<Tmcard>'
                                           '<Revision>%(Revision)s</Revision>'
                                           '<TmcardID>%(TmcardID)s</TmcardID>'
                                           '<Name>%(TmcardName)s</Name>'
                                           # '<Mapping>'
                                                '%(FrameInfo)s'
                                           # '</Mapping>'
                                           '%(CamList)s'
                                       '</Tmcard>'
                                   '</UpdRecTmcard>'
                               '</CmdBody>'
                           '</Command>')

TIME_FRAME_INFO = ('<Ele>'
                       '<Frame>'
                           '<Name>%(tfName)s</Name>'
                           '<Holiday>false</Holiday>'
                           '<RepeatMod>%(RepeatMod)s</RepeatMod>'
                           '<DLMod>%(DLMod)s</DLMod>'  # SingleDay, Period
                       # '<Intervals>'
                       #     '<Ele B="0" E="86400"/>'
                       # '</Intervals>'
                       # '<DayList>'
                       #     '<Ele B="1.0"/>'
                       #     '<Ele B="2.0"/>'
                       #     '<Ele B="3.0"/>'
                       #     '<Ele B="4.0"/>'
                       #     '<Ele B="5.0"/>'
                       #     '<Ele B="6.0"/>'
                       #     '<Ele B="7.0"/>'
                       # '</DayList>'
                           '%(Intervals)s'
                           '%(DayList)s'
                           '<Range>'
                           '<Period>'
                           '<TmS>%(TmS)s</TmS>'  # 2015-04-18 00:00:00.000
                           '<TmE>%(TmE)s</TmE>'  # 2100-01-01 00:00:00.000
                           '</Period>'
                           '<End>false</End>'
                           '</Range>'
                           '<RepeatFreq>1</RepeatFreq>'
                       '</Frame>'
                       '<RecMode>%(RecMode)s</RecMode>'  # Continuous
                       '<Motion>%(Motion)s</Motion>'  # true
                       '<ContMotNum>%(ContMotNum)s</ContMotNum>'  # 1
                       '<DigiInput>%(DigiInput)s</DigiInput>'  # 3
                       '<PIR>%(PIR)s</PIR>'  # true
                       '<Tampering>%(Tampering)s</Tampering>'  # true
                       '<PPTZTrigger>%(PPTZTrigger)s</PPTZTrigger>'  # true
                       '<StopUntilDIFalling>%(StopUntilDIFalling)s</StopUntilDIFalling>'  # false
                       '<StopUntilDIRising>%(StopUntilDIRising)s</StopUntilDIRising>'  # false
                       '<LineDetection>%(LineDetection)s</LineDetection>'  # false
                       '<FieldDetection>%(FieldDetection)s</FieldDetection>'  # false
                       '<Loitering>%(Loitering)s</Loitering>' # true
                   '</Ele>')

ALARM_TIME_FRAME_INFO = ('<Ele>'
                             '<Frame>'
                                 '<Name>%(tfName)s</Name>'
                                 '<Holiday>false</Holiday>'
                                 '<RepeatMod>%(RepeatMod)s</RepeatMod>'
                                 '<DLMod>%(DLMod)s</DLMod>'  # SingleDay, Period
                                 # '<Intervals>'
                                 #     '<Ele B="0" E="86400"/>'
                                 # '</Intervals>'
                                 # '<DayList>'
                                 #     '<Ele B="1.0"/>'
                                 #     '<Ele B="2.0"/>'
                                 #     '<Ele B="3.0"/>'
                                 #     '<Ele B="4.0"/>'
                                 #     '<Ele B="5.0"/>'
                                 #     '<Ele B="6.0"/>'
                                 #     '<Ele B="7.0"/>'
                                 # '</DayList>'
                                 '%(Intervals)s'
                                 '%(DayList)s'
                                 '<Range>'
                                     '<Period>'
                                         '<TmS>%(TmS)s</TmS>'  # 2015-04-18 00:00:00.000
                                         '<TmE>%(TmE)s</TmE>'  # 2100-01-01 00:00:00.000
                                     '</Period>'
                                     '<End>false</End>'
                                 '</Range>'
                                 '<RepeatFreq>1</RepeatFreq>'
                             '</Frame>'
                         '</Ele>')

# ======================================================================================================================
# Add alarm list.
# ======================================================================================================================
COMMAND_ADD_ALARM = ('<Command>'
                         '<CmdName>%(CmdName)s</CmdName>'  # InsEvtTmcard, UpdEvtTmcard
                         '<CmdBody>'
                             '<%(CmdName)s>'
                                 '<Tmcard>'
                                     '<Revision>%(Revision)s</Revision>'  # 0
                                     '<TmcardID>%(TmcardID)s</TmcardID>'  # 0
                                     '<Name>%(TmcardName)s</Name>'
                                     '<Enabled>%(TmcardEnabled)s</Enabled>'  # true
                                     '<TriggerLatency>%(TriggerLatency)s</TriggerLatency>'  # 5
                                     '<TriggerList>'
                                         '<CamEventTrigger>'
                                             '<CamName>%(EventTriggerCamName)s</CamName>'  # C_1
                                             '<EventType>Motion</EventType>'
                                             '<Motion>'
                                                 '<Index>0</Index>'
                                                 '<Percent>0</Percent>'
                                             '</Motion>'
                                             '<Rule>'
                                                 '<Latency>5</Latency>'
                                             '</Rule>'
                                         '</CamEventTrigger>'
                                     '</TriggerList>'
                                     '<ActionMode></ActionMode>'
                                     '<ActionInterval>0</ActionInterval>'
                                     '<ActionList>'
                                         '<Action>'
                                             '<Order>0</Order>'
                                             '<Recording>'
                                                 # '<Cam>%(RecordingCam)s</Cam>'  # C_1
                                                 '%(RecordingCameras)s'
                                             '</Recording>'
                                         '</Action>'
                                     '</ActionList>'
                                     '<Description>%(Description)s</Description>'
                                     '<RelatedDevices>'
                                         # '<Entry>'
                                         #     '<Device>%(RelatedDevices)s</Device>'  # C_1
                                         #     '<DeviceRefName>%(DeviceRefName)s</DeviceRefName>'  # IP8330
                                         # '</Entry>'
                                         '%(RelatedDevicesEntry)s'
                                     '</RelatedDevices>'
                                     '<EnableNotification>%(EnableNotification)s</EnableNotification>'  # true
                                     #'<Mapping>'
                                         # '<Ele>'
                                         #     '<Frame>'
                                         #         '<Name>%(TFName)s</Name>'
                                         #         '<Holiday>false</Holiday>'
                                         #         '<RepeatMod>%(TFRepeatMod)s</RepeatMod>'  # Daily
                                         #         '<DLMod>SingleDay</DLMod>'
                                         #         '<Intervals>'
                                         #             '<Ele B="35820" E="68160"/>'
                                         #         '</Intervals>'
                                         #         '<DayList>'
                                         #            '<Ele B="0"/>'
                                         #         '</DayList>'
                                         #         '<Range>'
                                         #             '<Period>'
                                         #                 '<TmS>2015-04-30 00:00:00.000</TmS>'
                                         #                 '<TmE>2015-04-30 23:59:59.000</TmE>'
                                         #             '</Period>'
                                         #             '<End>false</End>'
                                         #         '</Range>'
                                         #         '<RepeatFreq>1</RepeatFreq>'
                                         #     '</Frame>'
                                         # '</Ele>'
                                     #'</Mapping>'
                                     '%(FrameInfo)s'
                                 '</Tmcard>'
                             '</%(CmdName)s>'
                         '</CmdBody>'
                     '</Command>')


from xml.etree.ElementTree import Element, SubElement, tostring, XML
from xml.etree import ElementTree
from xml.dom import minidom
import os
import socket
from collections import defaultdict
from TestLibrary.RemoteServerSettings import REMOTE_SERVER_IP


class MessageGenerator:
    SERVER_INFO = ""
    VAST_IP = socket.gethostbyname(socket.gethostname())
    VAST_PORT = "3454"
    VAST_USERNAME = "admin"
    VAST_PASSWORD = "1"
    VAST_SERVER_USERNAME = "admin"
    VAST_SERVER_PASSWORD = "1"

    def __init__(self):
        self.cmd_info = ""
        self.tmcard_frame_info = defaultdict(list)

    def _gen_server_info(self, url='/cfgserver.cgi'):
        MessageGenerator.SERVER_INFO = ""
        if MessageGenerator.SERVER_INFO == "":
            MessageGenerator.SERVER_INFO = Element('ServerInfo')
            info = dict(IP=MessageGenerator.VAST_IP,
                        Port=MessageGenerator.VAST_PORT,
                        URL=url,
                        UserName=MessageGenerator.VAST_USERNAME,
                        Password=MessageGenerator.VAST_PASSWORD,
                        ServerUserName=MessageGenerator.VAST_SERVER_USERNAME,
                        ServerPassword=MessageGenerator.VAST_SERVER_PASSWORD)
            MessageGenerator.SERVER_INFO.extend(XML(SEVER_INFO % info))

    def _gen_cmd_info(self, cmd):
        self.cmd_info = Element('CommandInfo')
        loop_count = SubElement(self.cmd_info, 'LoopCount')
        loop_count.text = "1"
        cmd_wait_time = SubElement(self.cmd_info, 'CmdWaitTime')
        cmd_wait_time.text = "100"

        command = SubElement(self.cmd_info, 'Command')
        command.extend(cmd)

    def _gen_msg(self, cmd, server_info_url='/cfgserver.cgi'):
        self._gen_server_info(server_info_url)
        self._gen_cmd_info(cmd)

        cmd_batch_runner = Element('CommandBatchRunner')
        server_info = SubElement(cmd_batch_runner, 'ServerInfo')
        server_info.extend(MessageGenerator.SERVER_INFO)

        cmd_info = SubElement(cmd_batch_runner, 'CommandInfo')
        cmd_info.extend(self.cmd_info)

        from VASTController import VASTController
        xml_path = os.path.join(VASTController.CMD_BATCH_RUNNER_DIR, VASTController.MSG_FILE_NAME)
        ElementTree.ElementTree(cmd_batch_runner).write(xml_path, encoding='utf-8')

        self.tmcard_frame_info.clear()

    def insert_nas_server(self, **kwargs):
        host = kwargs.get('host')
        host_domain = kwargs.get('host_domain')
        user = kwargs.get('user')
        pwd = kwargs.get('pwd')

        insert_nas_svr = Element('Command')
        info = dict(Host=host,
                    HostDomain=host_domain,
                    User=user,
                    Pwd=pwd)
        insert_nas_svr.extend(XML(COMMAND_INSERT_NAS_SERVER % info))
        self._gen_msg(insert_nas_svr)

    def get_recording_path_from_default_group(self):
        get_rec_group = Element('Command')
        get_rec_group.extend(XML(COMMAND_GET_REC_GROUP))
        self._gen_msg(get_rec_group)

    def set_storage_group(self, **kwargs):
        action = kwargs.get('action')
        gp_name = kwargs.get('gp_name')
        gp_id = kwargs.get('gp_id', '1')
        revision = kwargs.get('revision', '1')
        cycle = kwargs.get('cycle', 'true')
        tm_base_cycle = kwargs.get('tm_base_cycle', 'false')
        reserve_time = kwargs.get('reserve_time', '10800')

        add_grp = Element('Command')
        info = dict(Action=action,
                    Rev=revision,
                    GrpID=gp_id,
                    GrpName=gp_name,
                    Cycle=cycle,
                    TmBaseCycle=tm_base_cycle,
                    ReserveTime=reserve_time)
        add_grp.extend(XML(COMMAND_ADD_GROUP % info))
        self._gen_msg(add_grp)

    def remove_storage_group(self, **kwargs):
        gp_id = kwargs.get('gp_id')
        revision = kwargs.get('revision', '2')

        rem_grp = Element('Command')
        info = dict(Rev=revision,
                    GrpID=gp_id)
        rem_grp.extend(XML(COMMAND_REMOVE_GROUP % info))
        self._gen_msg(rem_grp)

    def set_recording_path(self, **kwargs):
        path = kwargs.get('path')
        action = kwargs.get('action')
        gp_id = kwargs.get('gp_id', '1')
        revision = kwargs.get('revision', '1')
        reserve_space = kwargs.get('reserve_space', '2048')
        path_type = kwargs.get('type', 'Local')
        recovery = kwargs.get('recovery', 'False')

        recovery_mode = ''
        if recovery == 'True':
            recovery_mode = '<RecoveryMode>true</RecoveryMode>'
        set_path = Element('Command')
        info = dict(Rev=revision,
                    Action=action,
                    GrpID=gp_id,
                    Path=path,
                    ReserveSpace=reserve_space,
                    Type=path_type,
                    RecoveryMode=recovery_mode)
        set_path.extend(XML(COMMAND_SET_REC_PATH % info))
        self._gen_msg(set_path)

    def remove_recording_path(self, **kwargs):
        path = kwargs.get('path')
        method = kwargs.get('method', 'RemoveMedia')
        gp_id = kwargs.get('gp_id', '1')
        revision = kwargs.get('revision', '1')

        rem_path = Element('Command')
        info = dict(Rev=revision,
                    GrpID=gp_id,
                    Path=path,
                    Method=method)
        rem_path.extend(XML(COMMAND_REMOVE_REC_PATH % info))
        self._gen_msg(rem_path)

    def edit_recording_path(self, **kwargs):
        ori_path = kwargs.get('ori_path')
        new_path = kwargs.get('new_path')
        method = kwargs.get('method')
        rm_path_seq = kwargs.get('rm_path_seq', '1')
        gp_id = kwargs.get('gp_id', '1')
        revision = kwargs.get('revision', '1')
        reserve_space = kwargs.get('reserve_space', '2048')
        path_type = kwargs.get('path_type', 'Local')

        edit_path = Element('Command')
        info = dict(Rev=revision,
                    RmPathSeq=rm_path_seq,
                    GrpID=gp_id,
                    OriginalPath=ori_path,
                    Method=method,
                    AddPathSeq=str(int(rm_path_seq)+1),
                    NewPath=new_path,
                    ReserveSpace=reserve_space,
                    Type=path_type)
        edit_path.extend(XML(COMMAND_EDIT_REC_PATH % info))
        self._gen_msg(edit_path)

    def detect_camera(self, **kwargs):
        brand = kwargs.get('brand', '1263818326')
        module = kwargs.get('module', '757935405')
        host_ip = kwargs.get('host_ip')
        user = kwargs.get('user', '')
        pwd = kwargs.get('pwd', '')
        port = kwargs.get('port', '9001')
        https_port = kwargs.get('https_port', '443')
        protocol = kwargs.get('protocol', 'HTTP')

        detect_camera = Element('Command')
        info = dict(Brand=brand,
                    Module=module,
                    HostIP=host_ip,
                    User=user,
                    Pwd=pwd,
                    Port=port,
                    HTTPSPort=https_port,
                    Protocol=protocol
                    )
        detect_camera.extend(XML(COMMAND_DETECT_CAMERA % info))
        self._gen_msg(detect_camera)

    def insert_camera(self, **kwargs):
        ref_name = kwargs.get('ref_name')
        host_ip = kwargs.get('host_ip')
        rec_gp_id = kwargs.get('rec_gp_id', '')
        user = kwargs.get('user', '')
        pwd = kwargs.get('pwd', '')
        port = kwargs.get('port', '9001')
        rec_stream = kwargs.get('rec_stream', '1')
        pre_event = kwargs.get('pre_event', '10')
        post_event = kwargs.get('post_event', '10')
        enable_seamless = kwargs.get('enable_seamless', 'false')
        tm_shift = kwargs.get('tm_shift', 'false')
        video_stream = kwargs.get('video_stream', '1')

        inset_camera = Element('Command')
        info = dict(RefName=ref_name,
                    HostIP=host_ip,
                    User=user,
                    Pwd=pwd,
                    AddIntoRecGroup=rec_gp_id,
                    Port=port,
                    RecStream=rec_stream,
                    PreEvent=pre_event,
                    PostEvent=post_event,
                    EnableSeamless=enable_seamless,
                    EnableTmShift=tm_shift,
                    VideoStream=video_stream)
        inset_camera.extend(XML(COMMAND_INSERT_CAMERA % info))
        self._gen_msg(inset_camera)

    def insert_real_camera(self, **kwargs):
        tm_shift = kwargs.get('tm_shift', 'false')
        seamless = kwargs.get('seamless', 'false')

        inset_camera = Element('Command')
        info = dict(EnableTmShift=tm_shift,
                    EnableSeamless=seamless)
        inset_camera.extend(XML(COMMAND_INST_REAL_CAMERA % info))
        self._gen_msg(inset_camera)

    def remove_camera(self, **kwargs):
        cam_name = kwargs.get('cam_name')
        method = kwargs.get('method')

        remove_camera = Element('Command')
        info = dict(CamName=cam_name,
                    Method=method)
        remove_camera.extend(XML(COMMAND_REMOVE_CAMERA % info))
        self._gen_msg(remove_camera)

    def insert_rec_time_card(self, **kwargs):
        name = kwargs.get('name')

        insert_time_card = Element('Command')
        info = dict(Name=name)
        insert_time_card.extend(XML(COMMAND_INSERT_REC_TIMECARD % info))
        self._gen_msg(insert_time_card)

    def set_time_frame_info(self, **kwargs):
        time_frame_type = kwargs.get('time_frame_type', 'schedule')
        delete_time_frame = kwargs.get('delete_time_frame', 'false')
        timecard_id = kwargs.get('timecard_id', '1')
        if delete_time_frame == 'true':
            self.tmcard_frame_info[timecard_id].append('')
            return

        tf_name = kwargs.get('tf_name', 'Always')
        repeat_mode = kwargs.get('repeat_mode', 'Weekly')
        dl_mode = kwargs.get('dl_mode', 'SingleDay')
        tm_s = kwargs.get('tm_s', '2015-04-01 00:00:00.000')
        tm_e = kwargs.get('tm_e', '2100-01-01 00:00:00.000')
        rec_mode = kwargs.get('rec_mode', 'Continuous')
        motion = kwargs.get('motion', 'true')
        cont_mot_num = kwargs.get('cont_mot_num', '1')
        digi_input = kwargs.get('digi_input', '3')
        pir = kwargs.get('pir', 'true')
        tampering = kwargs.get('tampering', 'true')
        pptz_trigger = kwargs.get('pptz_trigger', 'true')
        stop_until_di_falling = kwargs.get('stop_until_di_falling', 'false')
        stop_until_di_rising = kwargs.get('stop_until_di_rising', 'false')
        line_detection = kwargs.get('line_detection', 'false')
        field_detection = kwargs.get('field_detection', 'false')
        loitering = kwargs.get('loitering', 'true')

        # '<Intervals>'
        # '<Ele B="0" E="86400"/>'
        # '</Intervals>'
        intervals = '<Intervals>'
        interval_list = kwargs.get('interval_list', '0 86400').split()
        for i in range(0, len(interval_list), 2):
            intervals += '<Ele B="' + interval_list[i] + '" E="' + interval_list[i+1] + '"/>'
        intervals += '</Intervals>'

        # '<DayList>'
        # '<Ele B="1.0"/>'
        #     '<Ele B="2.0"/>'
        #     '<Ele B="3.0"/>'
        #     '<Ele B="4.0"/>'
        #     '<Ele B="5.0"/>'
        #     '<Ele B="6.0"/>'
        #     '<Ele B="7.0"/>'
        # '</DayList>'
        day_list = ''
        day = kwargs.get('day', '1.0 2.0 3.0 4.0 5.0 6.0 7.0').split()
        if repeat_mode != 'Period' or len(day) != 0:
            day_list += '<DayList>'
            for d in day:
                day_list += '<Ele B="' + d + '"/>'
            day_list += '</DayList>'

        info = dict(tfName=tf_name,
                    RepeatMod=repeat_mode,
                    DLMod=dl_mode,
                    Intervals=intervals,
                    DayList=day_list,
                    TmS=tm_s,
                    TmE=tm_e
                    )

        time_frame = ALARM_TIME_FRAME_INFO % info
        if time_frame_type == 'schedule':
            recording_mode = dict(RecMode=rec_mode,
                                  Motion=motion,
                                  ContMotNum=cont_mot_num,
                                  DigiInput=digi_input,
                                  PIR=pir,
                                  Tampering=tampering,
                                  PPTZTrigger=pptz_trigger,
                                  StopUntilDIFalling=stop_until_di_falling,
                                  StopUntilDIRising=stop_until_di_rising,
                                  LineDetection=line_detection,
                                  FieldDetection=field_detection,
                                  Loitering=loitering)
            info.update(recording_mode)
            time_frame = TIME_FRAME_INFO % info

        self.tmcard_frame_info[timecard_id].append(time_frame)

    def update_rec_time_card(self, **kwargs):
        timecard_id = kwargs.get('timecard_id', '1')
        default_time_frame = kwargs.get('default_time_frame', 'false')
        if default_time_frame == 'true':
            self.set_time_frame_info()

        time_frame = self.tmcard_frame_info[timecard_id]
        all_time_frame = ''
        if time_frame[0] != '':
            all_time_frame = '<Mapping>'
            for tf_info in time_frame:
                all_time_frame += tf_info
            all_time_frame += '</Mapping>'

        tmcard_name = kwargs.get('tmcard_name', 'Default Schedule')
        revision = kwargs.get('revision', '1')

        # <CamList>
        # 	<Cam>
        # 		<Name>C_2</Name>
        # 	</Cam>
        # </CamList>
        cam_list = ''
        cameras = kwargs.get('camera_name', '')
        groups = kwargs.get('group_id', '')
        if cameras != '':
            cam_list += '<CamList>'
            tmp_cam_list = cameras.split()
            tmp_grp_list = groups.split()
            for i in range(len(tmp_cam_list)):
                cam_list += '<Cam><Name>' + tmp_cam_list[i] + '</Name><RecGrpID>' + tmp_grp_list[i] + '</RecGrpID></Cam>'
            cam_list += '</CamList>'

        camera_list = kwargs.get('camera_list', [])
        if len(camera_list) != 0:
            cam_list += '<CamList>'
            for entry in camera_list:
                cam_list += '<Cam><Name>' + entry[0] + '</Name><RecGrpID>' + str(entry[1]) + '</RecGrpID></Cam>'
            cam_list += '</CamList>'

        upd_rec_time_card = Element('Command')
        info = dict(Revision=revision,
                    TmcardID=timecard_id,
                    TmcardName=tmcard_name,
                    FrameInfo=all_time_frame,
                    CamList=cam_list)

        upd_rec_time_card.extend(XML(COMMAND_UPDATE_TIMECARD % info))
        print(self.prettify(upd_rec_time_card))
        self._gen_msg(upd_rec_time_card)

    def del_rec_time_card(self, **kwargs):
        revision = kwargs.get('revision', '1')
        timecard_id = kwargs.get('timecard_id', '2')
        url = kwargs.get('url', '/cfgserver.cgi')

        del_time_card = Element('Command')
        info = dict(Revision=revision,
                    TmcardID=timecard_id)
        del_time_card.extend(XML(COMMAND_DELETE_TIMECARD % info))
        self._gen_msg(del_time_card, url)

    def manual_recording(self, action, camera_name, group_id):
        manual_rec = Element('Command')
        info = dict(Action=action,
                    CamName=camera_name,
                    GrpID=group_id)
        manual_rec.extend(XML(COMMAND_MANUAL_RECORDING % info))
        self._gen_msg(manual_rec)

    def add_alarm_lists(self, **kwargs):
        default_time_frame = kwargs.get('default_time_frame', 'false')
        cmd_name = kwargs.get('cmd_name', 'InsEvtTmcard')
        revision = kwargs.get('revision', '0')  #Revision
        timecard_id = kwargs.get('timecard_id', '0')  #TmcardID
        tmcard_name = kwargs.get('tmcard_name', 'AlarmTmCardName')  #TmcardName
        tmcard_enabled = kwargs.get('tmcard_enabled', 'true')  #TmcardEnabled
        trigger_latency = kwargs.get('trigger_latency', 5)  #TriggerLatency
        event_trigger_cam_name = kwargs.get('event_trigger_cam_name', 'C_1')  #EventTriggerCamName (C_1)
        recording_cam = kwargs.get('recording_cam', ['C_1', 1])  #RecordingCam
        description = kwargs.get('description', 'Alarm Description')  #Description
        related_devices = kwargs.get('related_devices', 'C_1')  #RelatedDevices  (C_1)
        device_ref_name = kwargs.get('device_ref_name')  #DeviceRefName (IP8330)
        enable_notification = kwargs.get('enable_notification', 'true')  #EnableNotification

        if default_time_frame == 'true':
            self.set_time_frame_info()

        # '<Recording>'
        # '<Cam>%(RecordingCam)s</Cam>'
        # '</Recording>'
        recording_cameras = ""
        for cam in recording_cam:
            recording_cameras += '<Cam GrpID = "%s">' % str(cam[1]) + cam[0] + '</Cam>'

        # '<Entry>'
        #     '<Device>%(RelatedDevices)s</Device>'  # C_1
        #     '<DeviceRefName>%(DeviceRefName)s</DeviceRefName>'  # IP8330
        # '</Entry>'
        related_device_entry = ""
        for i in zip(related_devices.split(), device_ref_name.split()):
            related_device_entry += '<Entry><Device>' + i[0] + '</Device><DeviceRefName>' + i[1] + '</DeviceRefName></Entry>'

        time_frame = self.tmcard_frame_info[timecard_id]
        all_time_frame = ''
        if time_frame[0] != '':
            all_time_frame = '<Mapping>'
            for tf_info in time_frame:
                all_time_frame += tf_info
            all_time_frame += '</Mapping>'

        add_alarm_list = Element('Command')
        info = dict(Revision=revision,
                    CmdName=cmd_name,
                    TmcardID=timecard_id,
                    TmcardName=tmcard_name,
                    TmcardEnabled=tmcard_enabled,
                    TriggerLatency=trigger_latency,
                    EventTriggerCamName=event_trigger_cam_name,
                    RecordingCameras=recording_cameras,
                    Description=description,
                    RelatedDevices=related_devices,
                    DeviceRefName=device_ref_name,
                    RelatedDevicesEntry=related_device_entry,
                    EnableNotification=enable_notification,
                    FrameInfo=all_time_frame)

        add_alarm_list.extend(XML(COMMAND_ADD_ALARM % info))
        self._gen_msg(add_alarm_list, '/evtserver.cgi')

    @staticmethod
    def prettify(elem):
        """Return a pretty-printed XML string for the Element."""
        rough_string = ElementTree.tostring(elem, 'utf-8')
        reparsed = minidom.parseString(rough_string)
        return reparsed.toprettyxml(indent="  ")

if __name__ == '__main__':
    import xml.etree.cElementTree as ET
    root = ET.fromstring(COMMAND_UPDATE_TIMECARD)
    tree = ET.ElementTree(file='doc1.xml')
    for elem in root.iterfind('Mapping'):
        print elem.tag, elem.attr
