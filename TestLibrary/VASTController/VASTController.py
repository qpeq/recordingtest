import sys
import os

VAST_RF_TEST_WORKING_RELATIVE_DIR = os.path.join(os.path.dirname(__file__), '..', '..')
VAST_RF_TEST_WORKING_DIR = os.path.realpath(VAST_RF_TEST_WORKING_RELATIVE_DIR)
sys.path.append(VAST_RF_TEST_WORKING_DIR)

from TestLibrary import DebugLogger
from TestLibrary.Error import RecPathCreateError, VASTRestoreError, ServerIsNotRunningError, CmdBatchRunnerError, \
    CpyRecSrvError, RunCmdBatchRunnerError, ClearRedundantFileError
import subprocess
from MessageGenerator import MessageGenerator
import time
import _winreg
import platform
import shutil
from robot.libraries.BuiltIn import BuiltIn
from threading import Timer
import TestLibrary.RemoteServerSettings


class VASTController(object):
    CMD_BATCH_RUNNER_DIR = os.path.join(VAST_RF_TEST_WORKING_DIR, "CommandBatchRunner")
    CMD_BATCH_RUNNER_PATH = os.path.join(CMD_BATCH_RUNNER_DIR, "CommandBatchRunner.exe")
    MSG_FILE_NAME = "CommandBatchRunner.xml"

    def __init__(self):
        self.msg_gen = MessageGenerator()
        self.install_path = VASTController.get_install_path(TestLibrary.RemoteServerSettings.SERVER_VERSION)
        self.redundant_file_path = ""
        self.redundant_file_name = "file.to.create"

    def get_recording_path_from_default_group(self):
        self.msg_gen.get_recording_path_from_default_group()
        VASTController.send_msg()

    def insert_nas_server(self, **kwargs):
        DebugLogger.debug_log("Insert NAS server ...", "info")
        self.msg_gen.insert_nas_server(**kwargs)
        VASTController.send_msg()

    def add_storage_group(self, **kwargs):
        DebugLogger.debug_log("Add storage group ...", "info")
        kwargs.update({'action': 'AddGrp'})
        self.msg_gen.set_storage_group(**kwargs)
        VASTController.send_msg()

    def rename_storage_group(self, **kwargs):
        DebugLogger.debug_log("Rename storage group ...", "info")
        kwargs.update({'action': 'SetGrp'})
        self.msg_gen.set_storage_group(**kwargs)
        VASTController.send_msg()

    def edit_storage_group(self, **kwargs):
        DebugLogger.debug_log("Edit storage group ...", "info")
        kwargs.update({'action': 'SetGrp'})
        self.msg_gen.set_storage_group(**kwargs)
        VASTController.send_msg()

    def remove_storage_group(self, **kwargs):
        DebugLogger.debug_log("Remove storage group ...", "info")
        self.msg_gen.remove_storage_group(**kwargs)
        VASTController.send_msg()

    def add_path_in_storage_group(self, **kwargs):
        DebugLogger.debug_log("Add recording path in storage gp ...", "info")
        kwargs.update({'action': 'AddPath'})

        path = kwargs.get('path')
        DebugLogger.debug_log("Add recording path '%s' ..." % path, "info")
        if not os.path.exists(path):
            try:
                os.mkdir(path)
            except WindowsError:
                raise RecPathCreateError("Recording path: '%s' create failed by WindowsError!" % path)
        else:
            if len(os.listdir(path)) > 0:
                raise RecPathCreateError("Recording path: '%s' is not empty! Please choose another one as recording path." % path)

        self.msg_gen.set_recording_path(**kwargs)
        VASTController.send_msg()

    def set_path_in_storage_group(self, **kwargs):
        DebugLogger.debug_log("Set recording path in storage gp ...", "info")
        kwargs.update({'action': 'SetPath'})
        self.msg_gen.set_recording_path(**kwargs)
        VASTController.send_msg()

    def remove_path_in_storage_group(self, **kwargs):
        DebugLogger.debug_log("Remove recording path in storage gp ...", "info")
        self.msg_gen.remove_recording_path(**kwargs)
        VASTController.send_msg()

    def edit_path_in_storage_group(self, **kwargs):
        DebugLogger.debug_log("Edit recording path in storage gp ...", "info")
        kwargs.update({'method': 'RemoveMedia'})
        self.msg_gen.edit_recording_path(**kwargs)
        VASTController.send_msg()

    def detect_camera(self, **kwargs):
        DebugLogger.debug_log("Detect camera ...", "info")
        self.msg_gen.detect_camera(**kwargs)
        VASTController.send_msg()

    def insert_camera(self, **kwargs):
        DebugLogger.debug_log("Insert camera ...", "info")
        self.msg_gen.insert_camera(**kwargs)
        VASTController.send_msg()

    def insert_real_camera(self, **kwargs):
        DebugLogger.debug_log("Insert real camera ...", "info")
        self.msg_gen.insert_real_camera(**kwargs)
        VASTController.send_msg()

    def remove_camera(self, **kwargs):
        DebugLogger.debug_log("Remove camera ...", "info")
        self.msg_gen.remove_camera(**kwargs)
        VASTController.send_msg()

    def add_schedule_list(self, **kwargs):
        DebugLogger.debug_log("Add schedule list ...", "info")
        self.msg_gen.insert_rec_time_card(**kwargs)
        VASTController.send_msg()

    def rename_schedule_list(self, **kwargs):
        DebugLogger.debug_log("Rename schedule list ...", "info")
        self.msg_gen.update_rec_time_card(**kwargs)
        VASTController.send_msg()

    def delete_schedule_list(self, **kwargs):
        DebugLogger.debug_log("Delete schedule list ...", "info")
        self.msg_gen.del_rec_time_card(**kwargs)
        VASTController.send_msg()

    def delete_alarm_list(self, **kwargs):
        DebugLogger.debug_log("Delete schedule list ...", "info")
        kwargs.update({'url': '/evtserver.cgi'})
        self.msg_gen.del_rec_time_card(**kwargs)
        VASTController.send_msg()

    def set_time_frame_info(self, **kwargs):
        DebugLogger.debug_log("Set time frame info ...", "info")
        self.msg_gen.set_time_frame_info(**kwargs)

    def edit_recording_time_frame(self, **kwargs):
        DebugLogger.debug_log("Edit recording time frame ...", "info")
        self.msg_gen.update_rec_time_card(**kwargs)
        VASTController.send_msg()

    def manual_start_recording(self, camera_name, group_id):
        DebugLogger.debug_log("Manual Start Recording ...", "info")
        self.msg_gen.manual_recording('StartRec', camera_name, group_id)
        VASTController.send_msg()

    def manual_stop_recording(self, camera_name, group_id):
        DebugLogger.debug_log("Manual Stop Recording ...", "info")
        self.msg_gen.manual_recording('StopRec', camera_name, group_id)
        VASTController.send_msg()

    def add_alarm_list(self, **kwargs):
        DebugLogger.debug_log("Add alarm list ...", "info")
        self.msg_gen.add_alarm_lists(**kwargs)
        VASTController.send_msg()

    def update_alarm_list(self, **kwargs):
        DebugLogger.debug_log("Add alarm list ...", "info")
        kwargs.update({'cmd_name': 'UpdEvtTmcard'})
        self.msg_gen.add_alarm_lists(**kwargs)
        VASTController.send_msg()

    @staticmethod
    def cpy_vast_default_settings(src, des):
        if os.path.exists(des):
            try:
                shutil.rmtree(des)
            except:
                raise VASTRestoreError("Remove path: '%s' Failed!!" % des)
        shutil.copytree(src, des)

    @staticmethod
    def get_free_space_mb(folder):
        """ Return folder/drive free space (in bytes)
        """
        import ctypes
        free_bytes = ctypes.c_ulonglong(0)
        ctypes.windll.kernel32.GetDiskFreeSpaceExW(ctypes.c_wchar_p(folder), None, None, ctypes.pointer(free_bytes))
        free_space = free_bytes.value/1024/1024
        return free_space

    def stuff_full_storage_rec_path(self, path):
        reserve_size = 1 * 1024   # MB
        free_space = self.get_free_space_mb(path)
        redundant_file_create_size = free_space - reserve_size

        drive, path = os.path.splitdrive(path)
        file_path = os.path.join(drive, "\\", self.redundant_file_name)
        self.redundant_file_path = os.path.realpath(file_path)
        with open(self.redundant_file_path, "wb") as out:
            out.truncate(redundant_file_create_size * 1024 * 1024)

    @staticmethod
    def copy_rec_data(src, dst):
        if not os.path.exists(src):
            DebugLogger.debug_log("There is no recording data.", "info")
            return

        # idx = 1
        # while os.path.exists(dst):
        #     DebugLogger.debug_log("Enter while loop: dst path is %s" % dst, "info")
        #     test_name_path = dst[:dst.rfind("_")]
        #     dst = "%s_%s" % (test_name_path, idx)
        #     idx += 1
        try:
            shutil.copytree(src, dst)
        except:
            DebugLogger.debug_log("Copy recording data to '%s' failed!" % dst, "info")

    @staticmethod
    def is_vast_rec_path_exist(path):
        if os.path.exists(path):
            return True
        else:
            return False

    @staticmethod
    def get_install_path(server_version):
        reg = _winreg.ConnectRegistry(None, _winreg.HKEY_LOCAL_MACHINE)
        sub_key = ""

        bits = platform.architecture()[0]
        if bits == '64bit':  # try 32 app in 64 os first
            sub_key = r'Software\WOW6432node\VIVOTEK, Inc.\%s' % server_version
        elif bits == '32bit':
            sub_key = r'Software\VIVOTEK, Inc.\%s' % server_version

        try:
            key = _winreg.OpenKey(reg, sub_key)
        except _winreg.error:
            BuiltIn().fatal_error('QueryVASTInstallPathError: Query VAST install path Failed.')

        try:
            qry_val = _winreg.QueryValueEx(key, "INSTALL_PATH")
        except _winreg.error:
            BuiltIn().fatal_error('QueryVASTInstallPathError: Query VAST install path Failed.')
        finally:
            key.Close()
            reg.Close()

        return qry_val[0]

    @staticmethod
    def force_stop_vast_server():
        DebugLogger.debug_log("Force Stop VAST server ...", "info")
        try:
            subprocess.check_call(["taskkill", "/F", "/T", "/IM", "VMSUranusWatchDog.exe"], stdout=subprocess.PIPE)
        except:
            BuiltIn().fatal_error("ForceKillVASTError: Kill VAST server failed!!!")

        services = ["VMSBackupServer.exe", "VMSConfigurationServer.exe", "VMSEventServer.exe", "VMSStreamingServer.exe",
                    "VMSQueryServer.exe", "VMSWebServer.exe", "VMSRecordingServer.exe", "PluginServer.exe"]
        time.sleep(1)
        for service in services:
            if VASTController.check_process_alive(service):
                DebugLogger.debug_log("Warning: " + service + " still exists.", "info")
                subprocess.check_call(["taskkill", "/F", "/IM", service])

    @staticmethod
    def check_process_alive(process_name):
        cmd = 'TASKLIST', '/FI', 'imagename eq %s' % process_name
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        out = proc.communicate()[0].strip().split('\r\n')

        if len(out) > 1 and process_name[:10] in out[-1]:
            return True
        else:
            return False

    @staticmethod
    def process_exists(process_name):
        cmd = 'TASKLIST', '/FI', 'imagename eq %s' % process_name
        # shell=True hides the shell window, stdout to PIPE enables
        # communicate() to get the tasklist command result
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)

        # trimming it to the actual lines with information
        out = proc.communicate()[0].strip().split('\r\n')

        # if TASKLIST returns single line without processname: it's not running
        if len(out) > 1 and process_name[:10] in out[-1]:
            DebugLogger.debug_log('process "%s" is running!' % process_name, "info")
            return True
        else:
            DebugLogger.debug_log('stdout: %s' % out[0], "info")
            DebugLogger.debug_log('process "%s" is NOT running!' % process_name, "info")
            return False

    @staticmethod
    def start_vast_server():
        install_path = VASTController.get_install_path(TestLibrary.RemoteServerSettings.SERVER_VERSION)
        DebugLogger.debug_log("Restart VAST server ...", "info")
        vms_watch_dog = os.path.join(install_path, "Server", "VMSUranusWatchDog.exe")
        try:
            subprocess.check_call([vms_watch_dog, '-start'], stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE)
        except:
            BuiltIn().fatal_error("StartVASTError: Restart VAST server is Failed!!!")
        VASTController.check_service_state()

    @staticmethod
    def stop_vast_server():
        install_path = VASTController.get_install_path(TestLibrary.RemoteServerSettings.SERVER_VERSION)
        DebugLogger.debug_log("Stop VAST server ...", "info")
        vms_watch_dog = os.path.join(install_path, "Server", "VMSUranusWatchDog.exe")
        try:
            subprocess.check_call([vms_watch_dog, '-stop'], stdin=subprocess.PIPE,
                                  stdout=subprocess.PIPE)
        except:
            BuiltIn().fatal_error("StopVASTError: Stop VAST server is Failed!!!")

    @staticmethod
    def check_service_state():
        timer = Timer(300, None)
        timer.start()
        for p in ["VMSBackupServer.exe", "VMSConfigurationServer.exe", "VMSEventServer.exe", "VMSQueryServer.exe",
                  "VMSRecordingServer.exe", "VMSStreamingServer.exe", "VMSUranusWatchDog.exe", "VMSWebServer.exe"]:
            is_running = VASTController.process_exists(p)
            while not is_running:
                if timer.is_alive():
                    time.sleep(2)
                    is_running = VASTController.process_exists(p)
                else:
                    raise ServerIsNotRunningError("'%s' is not ready!" % p)

        if TestLibrary.RemoteServerSettings.SERVER_VERSION == "VAST":
            is_running = VASTController.process_exists("PluginServer.exe")
            while not is_running:
                if timer.is_alive():
                    time.sleep(2)
                    is_running = VASTController.process_exists(p)
                else:
                    raise ServerIsNotRunningError("'%s' is not ready!" % p)
        timer.cancel()

    def clear_storage_rec_path(self):
        if os.path.isfile(self.redundant_file_path):
            try:
                os.remove(self.redundant_file_path)
            except:
                raise ClearRedundantFileError("Clear redundant file failed.")

    def restore_to_default_settings(self, *rec_path):
        DebugLogger.debug_log("Restore to default settings ...", "info")
        VASTController.force_stop_vast_server()
        # Avoid copy file failed.
        time.sleep(5)

        # Delete content in storage gp default path.
        DebugLogger.debug_log("Delete Content In Storage Group Default Path ...", "info")
        DebugLogger.debug_log("recording path : %s" % rec_path, "info")
        rec_path = ''.join(rec_path).split()
        for path in rec_path:
            if os.path.exists(path):
                try:
                    shutil.rmtree(path)
                except:
                    BuiltIn().fatal_error("VASTRestoreError: Remove path '%s' Failed!!" % path)
            DebugLogger.debug_log("Remove path : %s" % path, "info")
            os.mkdir(path)

        # Restore default settings.
        DebugLogger.debug_log("Restore default settings ...", "info")
        gaea_db_src = os.path.join(VAST_RF_TEST_WORKING_DIR, "VASTDefaultSettings", "GaeaDB")
        gaea_db_des = os.path.join(self.install_path, "Server", "GaeaDB")
        VASTController.cpy_vast_default_settings(gaea_db_src, gaea_db_des)

        sqlite_src = os.path.join(VAST_RF_TEST_WORKING_DIR, "VASTDefaultSettings", "sqlite")
        sqlite_des = os.path.join(self.install_path, "sqlite")
        VASTController.cpy_vast_default_settings(sqlite_src, sqlite_des)

        hint_src = os.path.join(VAST_RF_TEST_WORKING_DIR, "VASTDefaultSettings", "Hint.dat")
        hint_des = os.path.join(self.install_path, "Server", "Hint.dat")
        shutil.copyfile(hint_src, hint_des)

        pwd_src = os.path.join(VAST_RF_TEST_WORKING_DIR, "VASTDefaultSettings", "PWD.dat")
        pwd_des = os.path.join(self.install_path, "Server", "PWD.dat")
        shutil.copyfile(pwd_src, pwd_des)

        # Restart VAST server.
        VASTController.start_vast_server()

        # Avoid next message send failed.
        time.sleep(10)

    @staticmethod
    def send_msg():
        time.sleep(5)
        o_file_path = os.path.join(VASTController.CMD_BATCH_RUNNER_DIR, "std.out")
        i_file_path = os.path.join(VASTController.CMD_BATCH_RUNNER_DIR, "std.in")
        std_out_file = file(o_file_path, 'w')
        std_in_file = file(i_file_path, 'w')

        is_execute = False
        while is_execute is False:
            p = subprocess.Popen(VASTController.CMD_BATCH_RUNNER_PATH, stdin=std_in_file, stdout=std_out_file)

            # It is necessary to press any key to return.
            timer = Timer(5, p.kill)
            timer.start()
            DebugLogger.debug_log("communicate start...", "info")
            p.communicate("\r\n")
            DebugLogger.debug_log("communicate end...", "info")
            if timer.is_alive():
                timer.cancel()
                is_execute = True
            else:
                try:
                    DebugLogger.debug_log("CommandBatchRunner Failed!!!", "info")
                    subprocess.check_call(["taskkill", "/F", "/IM", "WerFault.exe"], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
                    time.sleep(2)
                except:
                    DebugLogger.debug_log("Tunnel Establish Failure!", "info")
                    time.sleep(2)

        ret_code = p.wait()
        std_out_file.close()
        std_in_file.close()

        output = ""
        with open(o_file_path, "r") as file_stdout:
            output = file_stdout.read().replace('\n', ' ')

        import re
        m = re.search('(?<=Command Response Code: )(?P<response_code>[0-9a-fA-F]+)', output)
        response_code = m.group('response_code')

        if ret_code:
            raise RunCmdBatchRunnerError('CommadBatchRunner return code is {0}'.format(ret_code))
        elif response_code != '0':
            raise CmdBatchRunnerError('CommandBatchRunner response code is not 0. ({0} get)!!!'.format(response_code))

    @staticmethod
    def update_recording_server(latest_rec_srv_path):
        VASTController.stop_vast_server()
        time.sleep(3)

        install_path = VASTController.get_install_path(TestLibrary.RemoteServerSettings.SERVER_VERSION)
        rec_srv_path = os.path.join(install_path, "Server", "VMSRecordingServer.exe")
        try:
            shutil.copyfile(latest_rec_srv_path, rec_srv_path)
        except:
            raise CpyRecSrvError("Copy VMSRecordingServer.exe failed.")
        VASTController.start_vast_server()
        time.sleep(3)

    def install_vast(self, vast_setup_path):
        VASTController.stop_vast_server()
        time.sleep(10)

        install_batch_dir = os.path.join(VAST_RF_TEST_WORKING_DIR, "Tool", "VAST_Auto_Installation")
        curr_working_dir = os.getcwd()
        os.chdir(install_batch_dir)

        exe_name = os.path.basename(vast_setup_path)
        install_version = 'VAST' if 'VAST' in exe_name else 'ST7501'
        install_exe = '%s_Installer.exe' % install_version

        import TestLibrary.RemoteServerSettings
        is_same_version = True if TestLibrary.RemoteServerSettings.SERVER_VERSION == install_version else False
        if 'VAST' in exe_name and is_same_version or 'ST7501' in exe_name and not is_same_version:
            uninstall_exe = 'VAST_Uninstaller.exe'
            uninstall_version = 'VAST'
        elif 'VAST' in exe_name and not is_same_version or 'ST7501' in exe_name and is_same_version:
            uninstall_exe = 'ST7501_Uninstaller.exe'
            uninstall_version = 'ST7501'
        subprocess.check_call(uninstall_exe)

        cmd = [install_exe, "-p", vast_setup_path, "-l", "ENU", "-t", "Standard"]
        subprocess.check_call(cmd)
        os.chdir(curr_working_dir)

        if not is_same_version:
            remote_settings_file = os.path.join(VAST_RF_TEST_WORKING_DIR, 'TestLibrary', 'RemoteServerSettings.py')
            import fileinput
            for line in fileinput.input(remote_settings_file, inplace=True):
                print line.rstrip().replace("'"+uninstall_version, "'"+install_version)
            reload(TestLibrary.RemoteServerSettings)
            self.install_path = VASTController.get_install_path(TestLibrary.RemoteServerSettings.SERVER_VERSION)

        VASTController.check_service_state()

    @staticmethod
    def copy_default_database():
        DebugLogger.debug_log("Copy default settings ...", "info")
        install_path = VASTController.get_install_path(TestLibrary.RemoteServerSettings.SERVER_VERSION)
        default_settings_dir = os.path.join(VAST_RF_TEST_WORKING_DIR, "VASTDefaultSettings")

        gaea_db_src = os.path.join(install_path, "Server", "GaeaDB")
        gaea_db_dst = os.path.join(default_settings_dir, "GaeaDB")
        VASTController.cpy_vast_default_settings(gaea_db_src, gaea_db_dst)

        sqlite_src = os.path.join(install_path, "sqlite")
        sqlite_dst = os.path.join(default_settings_dir, "sqlite")
        VASTController.cpy_vast_default_settings(sqlite_src, sqlite_dst)

        hint_src = os.path.join(install_path, "Server", "Hint.dat")
        hint_dst = os.path.join(default_settings_dir, "Hint.dat")
        shutil.copyfile(hint_src, hint_dst)

        pwd_src = os.path.join(install_path, "Server", "PWD.dat")
        pwd_dst = os.path.join(default_settings_dir, "PWD.dat")
        shutil.copyfile(pwd_src, pwd_dst)

    @staticmethod
    def find_fail_data_backup_dir(dst):
        idx = 0
        tmp_str = "%s_%s"
        result = tmp_str % (dst, idx)
        while os.path.exists(result):
            idx += 1
            result = tmp_str % (dst, idx)

        return result


if __name__ == '__main__':
    import sys
    from robotremoteserver import RobotRemoteServer
    from TestLibrary.RemoteServerSettings import VAST_CONTROLLER_PORT

    RobotRemoteServer(VASTController(), host="0.0.0.0", port=VAST_CONTROLLER_PORT, *sys.argv[1:])
