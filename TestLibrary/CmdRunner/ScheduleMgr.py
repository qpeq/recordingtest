from io import BytesIO
from datetime import datetime
import xml.etree.cElementTree as ET
from __init__ import *


XML_STR = '''\
<Tmcard>
	<Revision>1</Revision>
	<TmcardID>1</TmcardID>
	<Name>Default Schedule</Name>
	<Mapping>
		<Ele>
			<Frame>
				<Name>Always</Name>
				<DLMod>SingleDay</DLMod>
				<RepeatMod>Weekly</RepeatMod>
				<DayList>
					<Ele B="1.0" E="7.0" />
				</DayList>
				<Intervals>
					<Ele B="0" E="86400" BDay="0" />
				</Intervals>
				<Holiday>false</Holiday>
				<Range>
					<Period>
						<TmS>2017-8-10 00:00:00</TmS>
						<TmE>2100-01-01 00:00:00</TmE>
					</Period>
					<End>false</End>
				</Range>
				<RepeatFreq>1</RepeatFreq>
			</Frame>
			<RecMode>Continuous</RecMode>
			<Motion>true</Motion>
			<ContMotNum>1</ContMotNum>
			<ExtEvent>false</ExtEvent>
			<DigiInput>3</DigiInput>
			<PIR>true</PIR>
			<Tampering>true</Tampering>
			<PPTZTrigger>true</PPTZTrigger>
			<StopUntilDIFalling>false</StopUntilDIFalling>
			<StopUntilDIRising>false</StopUntilDIRising>
			<LineDetection>false</LineDetection>
			<FieldDetection>false</FieldDetection>
			<Loitering>false</Loitering>
		</Ele>
	</Mapping>
</Tmcard>'''

class Timeframe(object):
    @staticmethod
    def get_daily_next_record_period(period_minutes):
        now_time = datetime.now()
        print 'now_time:', now_time

        start_h = now_time.hour
        start_m = now_time.minute
        start_s = 0
        if now_time.second > 30:
            start_m += 2
        else:
            start_m += 1

        start_period = start_h * 3600 + start_m * 60 + start_s
        end_period = start_period + period_minutes * 60
        return (start_period, end_period)

    @staticmethod
    def get_next_daily_timeframe(peroid_minutes, name='whatever'):
        period = Timeframe.get_daily_next_record_period(peroid_minutes)

        dic = dict()
        dic['Name'] = name
        dic['B'] = period[0]
        dic['E'] = period[1]
        result = TIMEFRAME_DAILY % dic
        return result

    @staticmethod
    def edit_rec_daily_timeframe(xml_str, **kwargs):
        pass

class Timecard(object):
    def AddTimeframe(self, timeframe):
        pass

class ScheduleMgr(object):
    @staticmethod
    def list_timecard_id(xml_str):
        pass

    @staticmethod
    def increase_revision(xml_str):
        root = ET.fromstring(xml_str)
        rev_elem = root.find('Revision')
        rev = int(rev_elem.text)
        rev_elem.text = str(rev + 1)

        buf = BytesIO()
        ET.ElementTree(root).write(buf, encoding='utf-8', xml_declaration=False) 
        result = buf.getvalue()
        return result

    @staticmethod
    def remove_all_timeframes(xml_str):
        root = ET.fromstring(xml_str)
        root.remove(root.find('Mapping'))
        buf = BytesIO()
        ET.ElementTree(root).write(buf, encoding='utf-8', xml_declaration=False) 
        result = buf.getvalue()
        return result

    @staticmethod
    def set_recording_mode_to_1st_timeframe_of_timecard(timecard_xml_str, recMode):
        root = ET.fromstring(timecard_xml_str)
        map_elem = root.find('Mapping')
        ele_elem = map_elem.find('Ele')
        rec_mode_elem = ele_elem.find('RecMode')
        rec_mode_elem.text = recMode

        buf = BytesIO()
        ET.ElementTree(root).write(buf, encoding='utf-8', xml_declaration=False) 
        result = buf.getvalue()
        return result

    @staticmethod
    def set_daily_recording_period(timecard_xml_str, period_minutes):
        period = Timeframe.get_daily_next_record_period(int(period_minutes))
        root = ET.fromstring(timecard_xml_str)
        mapping = root.find('Mapping')
        ele = mapping.find('Ele')
        mapping.remove(ele)
        
        mapping.append(ET.fromstring(REC_ELE_TIMEFRAME_DAILY % period))

        buf = BytesIO()
        ET.ElementTree(root).write(buf, encoding='utf-8', xml_declaration=False) 
        result = buf.getvalue()
        return result        

    @staticmethod
    def get_two_cam_alarm_trigger_record_each_other_timecard(**kwargs):
        pass

if __name__ == '__main__':
    ScheduleMgr.set_daily_recording_period(XML_STR, 1)
