import xml.etree.cElementTree as ET
from time import sleep
from cmdrunner import send_cmd, send_cmd_evt
from __init__ import *

IS_STATUS_RECORDING = ['Continuous', 'Manual', 'Event']

class CmdRunner(object):
    @staticmethod
    def InsertCam(**kwargs):
        if not kwargs.get('RefName'):
            kwargs['RefName'] = 'IP8330'

        if not kwargs.get('HostIP'):
            kwargs['HostIP'] = '127.0.0.1'

        if not kwargs.get('Port'):
            kwargs['Port'] = '9001'

        if not kwargs.get('AddIntoRecGroup'):
            kwargs['AddIntoRecGroup'] = '1'

        if not kwargs.get('User'):
            kwargs['User'] = ''
        
        if not kwargs.get('Pwd'):
            kwargs['Pwd'] = ''
        
        if not kwargs.get('RecStream'):
            kwargs['RecStream'] = '1'
        
        if not kwargs.get('PreEvent'):
            kwargs['PreEvent'] = '10'

        if not kwargs.get('PostEvent'):
            kwargs['PostEvent'] = '10'
        
        if not kwargs.get('EnableSeamless'):
            kwargs['EnableSeamless'] = 'false'

        if not kwargs.get('EnableTmShift'):
            kwargs['EnableTmShift'] = 'false'
        
        if not kwargs.get('VideoStream'):
            kwargs['VideoStream'] = '1'

        xml_msg = CMD_INSERT_CAMERA % kwargs
        xml_result = send_cmd('InsertCam', xml_msg)
        res = ET.fromstring(xml_result)
        camName = res.find('Name').text
        return camName

    @staticmethod
    def IsCamRecording(camName):
        xml_msg = CMD_GET_CAM_STATUS % camName
        xml_result = send_cmd('GetCamStatus', xml_msg)

        cam_status = ET.fromstring(xml_result)
        cam = cam_status[0]
        if cam.find('RecState').text in IS_STATUS_RECORDING:
            return True
        return False

    @staticmethod
    def GetRecTmcard():
        xml_result = send_cmd('GetRecTmcard', CMD_GET_REC_TMCARD)
        xml_result = xml_result[17:-20]
        return xml_result

    @staticmethod
    def UpdRecTmcard(xml_str):
        xml_str = xml_str.replace("GrpID", "RecGrpID")
        xml_msg = CMD_UPD_REC_TMCARD % xml_str
        xml_result = send_cmd('UpdRecTmcard', xml_msg)
        return xml_result

    @staticmethod
    def InsEvtTmcard(cmd_name):
        cmd = globals().get(cmd_name, None)
        if cmd:
            print(cmd)
            send_cmd_evt('InsEvtTmcard', cmd)

    @staticmethod
    def auto_send_cmds(cmd_list):
        for cmd in cmd_list:
            root = ET.fromstring(cmd)
            send_cmd(root.tag, cmd)
            sleep(3)


if __name__ == '__main__':
    import cmds.stress_alarm_trigger
    # CmdRunner.auto_send_cmds(cmds.stress_alarm_trigger.CMDS)
    CmdRunner.InsEvtTmcard('CMD_INS_EVT_TMCARD_CAM1_TRIGGERS_ITSELF')
