
CMDS = [
'''\
<InsEvtTmcard>
	<Tmcard>
		<Revision>%(Revision)s</Revision>
		<TmcardID>0</TmcardID>
		<Name>test</Name>
		<Enabled>true</Enabled>
		<TriggerLatency>5</TriggerLatency>
		<TriggerList>
			<CamEventTrigger>
				<CamName>%(Cam1)s</CamName>
				<EventType>Motion</EventType>
				<Motion>
					<Index>0</Index>
					<Percent>0</Percent>
				</Motion>
				<Rule>
					<Latency>5</Latency>
				</Rule>
			</CamEventTrigger>
			<CamEventTrigger>
				<CamName>%(Cam1)s</CamName>
				<EventType>Motion</EventType>
				<Motion>
					<Index>1</Index>
					<Percent>0</Percent>
				</Motion>
				<Rule>
					<Latency>5</Latency>
				</Rule>
			</CamEventTrigger>
			<CamEventTrigger>
				<CamName>%(Cam1)s</CamName>
				<EventType>Motion</EventType>
				<Motion>
					<Index>2</Index>
					<Percent>0</Percent>
				</Motion>
				<Rule>
					<Latency>5</Latency>
				</Rule>
			</CamEventTrigger>
			<CamEventTrigger>
				<CamName>%(Cam2)s</CamName>
				<EventType>Motion</EventType>
				<Motion>
					<Index>0</Index>
					<Percent>0</Percent>
				</Motion>
				<Rule>
					<Latency>5</Latency>
				</Rule>
			</CamEventTrigger>
			<CamEventTrigger>
				<CamName>%(Cam2)s</CamName>
				<EventType>Motion</EventType>
				<Motion>
					<Index>1</Index>
					<Percent>0</Percent>
				</Motion>
				<Rule>
					<Latency>5</Latency>
				</Rule>
			</CamEventTrigger>
			<CamEventTrigger>
				<CamName>%(Cam2)s</CamName>
				<EventType>Motion</EventType>
				<Motion>
					<Index>2</Index>
					<Percent>0</Percent>
				</Motion>
				<Rule>
					<Latency>5</Latency>
				</Rule>
			</CamEventTrigger>
		</TriggerList>
		<ActionMode/>
		<ActionInterval>0</ActionInterval>
		<ActionList>
			<Action>
				<Order>0</Order>
				<Recording>
					<Cam GrpID="%(GrpID)s">%(Cam1)s</Cam>
					<Cam GrpID="%(GrpID)s">%(Cam2)s</Cam>
				</Recording>
			</Action>
		</ActionList>
		<RelatedDevices>
			<Entry>
				<Device>%(Cam1)s</Device>
				<DeviceRefName>IP8330</DeviceRefName>
			</Entry>
			<Entry>
				<Device>%(Cam2)s</Device>
				<DeviceRefName>IP8330</DeviceRefName>
			</Entry>
		</RelatedDevices>
		<EnableNotification>true</EnableNotification>
		<Mapping>
			<Ele>
				<Frame>
					<Name>Always</Name>
					<Holiday>false</Holiday>
					<RepeatMod>Weekly</RepeatMod>
					<DLMod>SingleDay</DLMod>
					<Intervals>
						<Ele B="0" E="86400"/>
					</Intervals>
					<DayList>
						<Ele B="1.0"/>
						<Ele B="2.0"/>
						<Ele B="3.0"/>
						<Ele B="4.0"/>
						<Ele B="5.0"/>
						<Ele B="6.0"/>
						<Ele B="7.0"/>
					</DayList>
					<Range>
						<Period>
							<TmS>2008-12-08 00:00:00.000</TmS>
							<TmE>2035-01-01 00:00:00.000</TmE>
						</Period>
						<End>false</End>
					</Range>
					<RepeatFreq>1</RepeatFreq>
				</Frame>
			</Ele>
		</Mapping>
	</Tmcard>
</InsEvtTmcard>
'''
]