import sys
import os
VAST_RF_TEST_WORKING_RELATIVE_DIR = os.path.join(os.path.dirname(__file__), '..', '..')
VAST_RF_TEST_WORKING_DIR = os.path.realpath(VAST_RF_TEST_WORKING_RELATIVE_DIR)
sys.path.append(VAST_RF_TEST_WORKING_DIR)

from DBUtility import DBUtility
from TestLibrary import DebugLogger


class GAEADBUtility(DBUtility):
    def __init__(self):
        DBUtility.__init__(self)

    def get_time_interval_info(self):
        sql_cmd = "SELECT * FROM timeinterval"
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_event_info(self, condition):
        sql_cmd = "SELECT * FROM event WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_media_file_disk_map_info(self, condition):
        sql_cmd = "SELECT * FROM mediafilediskmap WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_all_interval_recording_video_file_list(self, interval_cnt, storage_rec_path, camera_folder_name):
        file_list = list()
        for i in range(1, interval_cnt + 1):
            condition = "intervalserial=%i and filesize!=0" % i
            result = self.get_media_file_disk_map_info(condition)
            if len(result) == 0:
                pass
            one_interval_file_list = ""
            for itm in result:
                path = os.path.join(storage_rec_path, camera_folder_name, itm['pathname'])
                one_interval_file_list += " %s" % path
            file_list.append(one_interval_file_list.strip())
        return file_list

if __name__ == "__main__":
    import sys
    from robotremoteserver import RobotRemoteServer
    from TestLibrary.RemoteServerSettings import GAEA_DB_UTILITY_REMOTE_SERVER_PORT

    RobotRemoteServer(GAEADBUtility(), host="0.0.0.0", port=GAEA_DB_UTILITY_REMOTE_SERVER_PORT, *sys.argv[1:])
