import sys
import os

VAST_RF_TEST_WORKING_RELATIVE_DIR = os.path.join(os.path.dirname(__file__), '..', '..')
VAST_RF_TEST_WORKING_DIR = os.path.realpath(VAST_RF_TEST_WORKING_RELATIVE_DIR)
sys.path.append(VAST_RF_TEST_WORKING_DIR)

import os
from DBUtility import DBUtility
from TestLibrary import DebugLogger
from TestLibrary.Error import QueryVASTInstallPathError
import _winreg
import platform
import TestLibrary.RemoteServerSettings


class VASTDBUtility(DBUtility):
    def __init__(self):
        DBUtility.__init__(self)

    def set_db_path(self, path):
        reload(TestLibrary.RemoteServerSettings)
        reg = _winreg.ConnectRegistry(None, _winreg.HKEY_LOCAL_MACHINE)
        sub_key = ""

        bits = platform.architecture()[0]
        if bits == '64bit':  # try 32 app in 64 os first
            sub_key = r'Software\WOW6432node\VIVOTEK, Inc.\%s' % TestLibrary.RemoteServerSettings.SERVER_VERSION
        elif bits == '32bit':
            sub_key = r'Software\VIVOTEK, Inc.\%s' % TestLibrary.RemoteServerSettings.SERVER_VERSION

        try:
            key = _winreg.OpenKey(reg, sub_key)
        except _winreg.error:
            raise QueryVASTInstallPathError('Query VAST install path Failed.')

        try:
            qry_val = _winreg.QueryValueEx(key, "DB_PATH")
        except _winreg.error:
            raise QueryVASTInstallPathError('Query VAST install path Failed.')
        finally:
            key.Close()
            reg.Close()

        vast_db_name = '%s.db' % TestLibrary.RemoteServerSettings.SERVER_VERSION
        vast_db_path = os.path.join(qry_val[0], vast_db_name)
        self.db_path = vast_db_path

    def get_recording_path_info(self, condition):
        sql_cmd = "SELECT * FROM RecordPath WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_cameramng_info(self, condition):
        sql_cmd = "SELECT * FROM CameraMng WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_channel_info(self, condition):
        sql_cmd = "SELECT * FROM ChannelInfo WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_schedule_timecard_info(self, condition):
        sql_cmd = "SELECT * FROM ScheTimecard WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_schedule_rec_timeframe_info(self, condition):
        sql_cmd = "SELECT * FROM ScheRecTimeframe WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_rec_event_type(self, time_frame_id):
        result = self.get_schedule_rec_timeframe_info(time_frame_id)

        extra_param = result[0]['ExtraParam']
        event_tag_value = {}
        for i in range(0, len(extra_param), 6):
            event_tag_value[ord(extra_param[i])] = ord(extra_param[i + 2])
        return event_tag_value

    def get_schedule_timeframe_info(self, condition):
        sql_cmd = "SELECT * FROM ScheTimeframe WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_day_entry_info(self, condition):
        sql_cmd = "SELECT * FROM ScheDayEntryInfo WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_schedule_time_interval_info(self, condition):
        sql_cmd = "SELECT * FROM ScheTimeInterval WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_schedule_rec_camera_list_info(self, condition):
        sql_cmd = "SELECT * FROM ScheRecCamList WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_alarm_info(self, condition):
        sql_cmd = "SELECT * FROM Alarm WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_schedule_event_trigger_info(self, condition):
        sql_cmd = "SELECT * FROM ScheEvtTrigger WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_alarm_related_device_info(self, condition):
        sql_cmd = "SELECT * FROM AlarmRelatedDevice WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

    def get_nas_server_info(self, condition):
        sql_cmd = "SELECT * FROM NASServer WHERE %s" % condition
        result = self.execute_sql_cmd(sql_cmd)
        return result

if __name__ == "__main__":
    import sys
    from robotremoteserver import RobotRemoteServer
    from TestLibrary.RemoteServerSettings import VAST_DB_UTILITY_REMOTE_SERVER_PORT

    RobotRemoteServer(VASTDBUtility(), host="0.0.0.0", port=VAST_DB_UTILITY_REMOTE_SERVER_PORT, *sys.argv[1:])
