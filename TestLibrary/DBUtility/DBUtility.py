import sqlite3
from TestLibrary.Error import DBConnectionError, DBExecuteError
from TestLibrary import DebugLogger


def dict_factory(cursor, row):
    d = {}
    for idx, col in enumerate(cursor.description):
        d[col[0]] = row[idx]
    return d


class DBUtility(object):
    def __init__(self):
        self.db_path = None
        self.conn = None

    def connect_to_database(self, path=None):
        DebugLogger.debug_log("connect to db path: %s" % path, "info")
        DebugLogger.debug_log("connect to self.db_path: %s" % self.db_path, "info")
        if self.db_path == path and path is not None and self.conn is not None:  # avoid redundant connection
            return

        self.set_db_path(path)
        try:
            if self.conn is not None:
                self.conn.close()
            self.conn = sqlite3.connect(self.db_path)
            self.conn.row_factory = dict_factory
        except sqlite3.Error as e:
            DebugLogger.debug_log("An error occurred:", "info", e.args[0])
            return None

    def disconnect_to_database(self):
        DebugLogger.debug_log("Disconnect to db!!!", "info")
        if self.conn is None:
            return
        self.conn.close()
        self.conn = None

    def set_db_path(self, path):
        self.db_path = path

    def get_row_count_by_value(self, table_name, column, value):
        if self.conn is None:
            raise DBConnectionError('DB Connection is not be created!!!')
        try:
            c = self.conn.cursor()
            sql_cmd = "SELECT count(*) FROM %s WHERE %s='%s'" % (table_name, column, value)
            c.execute(sql_cmd)
            row_count = c.fetchone()['count(*)']
        except sqlite3.Error as e:
            raise DBExecuteError('An error occurred: ' + e.args[0])

        return row_count

    def get_row_count_by_table(self, table_name):
        if self.conn is None:
            raise DBConnectionError('DB Connection is not be created!!!')
        try:
            c = self.conn.cursor()
            sql_cmd = "SELECT COUNT(*) FROM " + table_name
            c.execute(sql_cmd)
            row_count = c.fetchone()['COUNT(*)']
        except sqlite3.Error as e:
            raise DBExecuteError('An error occurred: ' + e.args[0])

        return row_count

    def execute_sql_cmd(self, sql_cmd):
        if self.conn is None:
            raise DBConnectionError('DB Connection is not be created!!!')
        try:
            c = self.conn.cursor()
            c.execute(sql_cmd)
            result = c.fetchall()
        except sqlite3.Error as e:
            raise DBExecuteError('An error occurred: ' + e.args[0])

        return result