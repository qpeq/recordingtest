import sys
import os

VAST_RF_TEST_WORKING_RELATIVE_DIR = os.path.join(os.path.dirname(__file__), '..', '..')
VAST_RF_TEST_WORKING_DIR = os.path.realpath(VAST_RF_TEST_WORKING_RELATIVE_DIR)
sys.path.append(VAST_RF_TEST_WORKING_DIR)

import subprocess
from TestLibrary.Error import NoVideoClipError, VideoIOStreamError, VideoDurationError, VideoFrameRateError, RunVideoParserError, VideoCodecError, DBConnectionError
from TestLibrary import DebugLogger

ERROR_IOSTREAM = -1
ERROR_DURATION = -2
ERROR_FRAME_RATE = -3


class VideoParser(object):
    VIDEO_PARSER_EXECUTOR_DIR = os.path.join(VAST_RF_TEST_WORKING_DIR, "VideoParserExecutor")
    VIDEO_PARSER_EXECUTOR_PATH = os.path.join(VIDEO_PARSER_EXECUTOR_DIR, "VideoParserExecutor.exe")
    PARSE_VIDEO_WHILE_RECORDING_DIR = os.path.join(VAST_RF_TEST_WORKING_DIR, "ParseVideoWhileRecording")
    PARSE_VIDEO_WHILE_RECORDING_PATH = os.path.join(PARSE_VIDEO_WHILE_RECORDING_DIR, "ParseVideoWhileRecording.exe")

    def __init__(self):
        self.file_list = ""
        self.err_msg = ""

    @staticmethod
    def switch_video_codec(tag):
        video_codec = None
        if tag == 1:
            video_codec = 'JPEG'
        elif tag == 2:
            video_codec = 'H263'
        elif tag == 4:
            video_codec = 'MP4'
        elif tag == 8:
            video_codec = 'H264'
        return video_codec

    def is_video_codec_expected(self, video_codec_expected):
        if self.file_list == "":
            raise NoVideoClipError("Target video File is not given.")

        cmd = "%s --video_codec %s" % (VideoParser.VIDEO_PARSER_EXECUTOR_PATH, self.file_list)
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        ret_code = p.wait()
        video_codec = self.switch_video_codec(ret_code)

        if video_codec_expected == video_codec:
            return True
        else:
            import re
            output, err = p.communicate()
            m = re.search('(?<=Response Error Msg: )(?P<response_msg>[\w :\\\.-]+)', output)
            response_msg = m.group('response_msg')
            DebugLogger.debug_log("Response Error Msg: %s" % response_msg, "info")
            if ret_code == ERROR_IOSTREAM:
                raise VideoIOStreamError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
            elif ret_code == ERROR_DURATION:
                raise VideoDurationError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
            elif ret_code == ERROR_FRAME_RATE:
                raise VideoFrameRateError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
            else:
                raise VideoCodecError("Video Codec: %s is not ecpected!" % ret_code)

    def set_file_list(self, path, clip):
        if not os.path.exists(path):
            raise DBConnectionError("DB is not be created!!!")

        unsort_video_files = [f for f in os.listdir(path) if os.path.splitext(f)[1] == ".3gp"]
        video_files = sorted(unsort_video_files, key=lambda num: int(num.split("_")[0]))
        if clip != "all":
            video_files = [f for f in video_files if f.startswith(clip + "_")]
            if len(video_files) == 0:
                raise NoVideoClipError("There is no video clips under recording path.")

        abs_video_file_path = ['"' + os.path.join(path, f) + '"' for f in video_files]
        self.file_list = ' '.join(abs_video_file_path)
        for i in self.file_list.split():
            DebugLogger.debug_log("Video File List: %s" % i, "info")

    def set_specific_file_list(self, many_file_path):
        file_path = many_file_path.split()
        video_files = list()

        for path in file_path:
            if not os.path.exists(path):
                raise DBConnectionError("DB is not be created!!!")
            video_files += [os.path.join(path, f) for f in os.listdir(path) if os.path.splitext(f)[1] == ".3gp"]

        self.file_list = ' '.join(video_files)
        DebugLogger.debug_log("Video File List: %s" % self.file_list, "info")

    def set_given_file_list(self, file_list):
        self.file_list = file_list

    def is_video_frame_expected(self, duration=-1, frequency=-1, save_file_list=False, view_while_recording=False):
        if self.file_list == "":
            raise NoVideoClipError("Target video File is not given.")

        if view_while_recording is True:
            cmd = VideoParser.PARSE_VIDEO_WHILE_RECORDING_PATH
        else:
            cmd = VideoParser.VIDEO_PARSER_EXECUTOR_PATH

        if duration != -1:
            cmd += " -d %s" % duration
        if frequency != -1:
            cmd += " -f %s" % frequency

        if save_file_list:
            video_file_list_path = os.path.join(VideoParser.VIDEO_PARSER_EXECUTOR_DIR, "video_file_list.txt")
            f = open(video_file_list_path, 'w')
            f.write(self.file_list)
            f.close()
            cmd += " -p %s" % video_file_list_path
        else:
            cmd += " -i %s" % self.file_list

        DebugLogger.debug_log("Video Parser Cmd : %s" % cmd, "info")
        p = subprocess.Popen(cmd, stdin=subprocess.PIPE, stdout=subprocess.PIPE)

        ret_code = p.wait()
        if ret_code:
            import re
            output, err = p.communicate()
            m = re.search('(?<=Response Error Msg: )(?P<response_msg>[\w :\\\.-]+)', output)
            response_msg = m.group('response_msg')
            DebugLogger.debug_log("Response Error Msg: %s" % response_msg, "info")
            if ret_code == ERROR_IOSTREAM:
                raise VideoIOStreamError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
            elif ret_code == ERROR_DURATION:
                raise VideoDurationError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
            elif ret_code == ERROR_FRAME_RATE:
                raise VideoFrameRateError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
            else:
                raise RunVideoParserError("Command: '%s'. Error msg: '%s'" % (cmd, response_msg))
        return True

if __name__ == '__main__':
    from robotremoteserver import RobotRemoteServer
    from TestLibrary.RemoteServerSettings import VIDEO_PARSER_EXECUTOR

    RobotRemoteServer(VideoParser(), host="0.0.0.0", port=VIDEO_PARSER_EXECUTOR, *sys.argv[1:])
