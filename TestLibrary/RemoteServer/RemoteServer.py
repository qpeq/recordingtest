import os
import sys
from robotremoteserver import RobotRemoteServer

TEST_ROOT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
sys.path.append(TEST_ROOT_PATH)

WIN_PROG_PATH6432 = 'C:\\Program Files (x86)\\VIVOTEK Inc\\VAST\\Server\\Logs\\RecordingServer'
WIN_PROG_PATH32 = 'C:\\Program Files\\VIVOTEK Inc\\VAST\\Server\\Logs\\RecordingServer'


class RemoteServer(object):
    @staticmethod
    def get_recording_log_path():
        if os.path.exists(WIN_PROG_PATH6432):
            return WIN_PROG_PATH6432
        else:
            return WIN_PROG_PATH32

    def verify_recording_server_crash(self):
        log_path = RemoteServer.get_recording_log_path()
        files = os.listdir(log_path)
        for f in files:
            if f.endswith('dmp'):
                raise Exception('There is at least one crash dump of recording server!')


if __name__ == '__main__':
    RobotRemoteServer(RemoteServer(), host="0.0.0.0", port=29487)
    # r = RemoteServer()
    # r.update_the_test_repository()
