
class GAEADBError(Exception):
    pass


class VASTDBError(Exception):
    pass


class DBConnectionError(Exception):
    pass


class DBExecuteError(Exception):
    pass


class RecPathCreateError(Exception):
    pass


class RecPathDeleteError(Exception):
    pass


class VASTRestoreError(Exception):
    pass


class ServerIsNotRunningError(Exception):
    pass


class CmdBatchRunnerError(Exception):
    pass


class RunCmdBatchRunnerError(Exception):
    pass


class CpyRecSrvError(Exception):
    pass


class ClearRedundantFileError(Exception):
    pass


class ForceKillVASTError(Exception):
    pass


class StartVASTError(Exception):
    pass


class StopVASTError(Exception):
    pass


class QueryVASTInstallPathError(Exception):
    pass


class NoVideoClipError(Exception):
    pass


class RunVideoParserError(Exception):
    pass


class VideoIOStreamError(Exception):
    pass


class VideoDurationError(Exception):
    pass


class VideoFrameRateError(Exception):
    pass


class VideoCodecError(Exception):
    pass


class VideoIOStreamError(Exception):
    pass


class VideoDurationError(Exception):
    pass


class VideoFrameRateError(Exception):
    pass