import os
import sys
import shutil
import _winreg
import distutils.dir_util
import subprocess
from time import sleep
from threading import Timer
from platform import architecture

DEFAULT_RECORDING_PATH = 'D:\\recording'
TEST_ROOT_PATH = os.path.dirname(os.path.dirname(os.path.dirname(__file__)))
VAST_SETTING_BACKUP_PATH = os.path.join(TEST_ROOT_PATH, 'VASTDefaultSettings')
WIN_PROG_PATH6432 = 'C:\\Program Files (x86)\\VIVOTEK Inc\\VAST\\Server\\Logs\\RecordingServer'
WIN_PROG_PATH32 = 'C:\\Program Files\\VIVOTEK Inc\\VAST\\Server\\Logs\\RecordingServer'
VAST_SERVICES = ["VMSBackupServer.exe", "VMSConfigurationServer.exe", "VMSEventServer.exe", "VMSQueryServer.exe",
                  "VMSRecordingServer.exe", "VMSStreamingServer.exe", "VMSUranusWatchDog.exe", "VMSWebServer.exe"]
VAST_CHILD_SERVICES = ["VMSBackupServer.exe", "VMSConfigurationServer.exe", "VMSEventServer.exe", "VMSStreamingServer.exe",
                    "VMSQueryServer.exe", "VMSWebServer.exe", "VMSRecordingServer.exe", "PluginServer.exe"]


class Utility(object):
    def __init__(self):
        pass

    @staticmethod
    def get_vast_install_path():
        sub_key = ''
        bits = architecture()[0]
        if bits == '64bit':  # try 32 app in 64 os first
            sub_key = r'Software\WOW6432node\VIVOTEK, Inc.\VAST'
        elif bits == '32bit':
            sub_key = r'Software\VIVOTEK, Inc.\VAST'

        try:
            reg = _winreg.ConnectRegistry(None, _winreg.HKEY_LOCAL_MACHINE)
            key = _winreg.OpenKey(reg, sub_key)
            qry_val = _winreg.QueryValueEx(key, "INSTALL_PATH")
        except _winreg.error:
            raise Exception('Error: Query VAST install path Failed.')
        finally:
            key.Close()
            reg.Close()

        return qry_val[0]

    @staticmethod
    def get_recording_log_path():
        if os.path.exists(WIN_PROG_PATH6432):
            return WIN_PROG_PATH6432
        else:
            return WIN_PROG_PATH32

    @staticmethod
    def recording_server_should_not_have_any_crash_dump():
        log_path = Utility.get_recording_log_path()
        files = os.listdir(log_path)
        for f in files:
            if f.endswith('dmp'):
                raise Exception('There is at least one crash dump of recording server!')

    @staticmethod
    def install_vast(vast_setup_path):
        Utility.stop_vast_services()
        sleep(6)

        install_batch_dir = os.path.join(TEST_ROOT_PATH, "Tool", "VAST_Auto_Installation")
        os.chdir(install_batch_dir)

        subprocess.check_call('VAST_Uninstaller.exe')

        cmd = ['VAST_Installer.exe', "-p", vast_setup_path, "-l", "ENU", "-t", "Standard"]
        subprocess.check_call(cmd)

    @staticmethod
    def is_process_alive(process_name):
        cmd = 'TASKLIST', '/FI', 'imagename eq %s' % process_name
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)
        out = proc.communicate()[0].strip().split('\r\n')

        if len(out) > 1 and process_name[:10] in out[-1]:
            return True
        else:
            return False

    @staticmethod
    def start_vast_services():
        install_path = Utility.get_vast_install_path()
        vms_watch_dog = os.path.join(install_path, "Server", "VMSUranusWatchDog.exe")
        try:
            subprocess.check_call([vms_watch_dog, '-start'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
        except:
            raise Exception("Error: Restart VAST services failed!!!")
        Utility.check_service_state()

    @staticmethod
    def stop_vast_services(force=True):
        if force:
            print('stop_vast_services(True)')
            try:
                subprocess.check_call(["taskkill", "/F", "/T", "/IM", "VMSUranusWatchDog.exe"], stdout=subprocess.PIPE)
            except:
                raise Exception("Error: Kill VAST server failed!!!")

            sleep(1)
            for service in VAST_CHILD_SERVICES:
                if Utility.is_process_alive(service):
                    print("Warning: %s still exists." % service)
                    subprocess.check_call(["taskkill", "/F", "/IM", service])
        else:
            print('stop_vast_services(False)')
            install_path = Utility.get_vast_install_path()
            vms_watch_dog = os.path.join(install_path, 'Server', 'VMSUranusWatchDog.exe')
            try:
                subprocess.check_call([vms_watch_dog, '-stop'], stdin=subprocess.PIPE, stdout=subprocess.PIPE)
            except:
                raise Exception('Error: Stop VAST services failed!!!')

    @staticmethod
    def process_exists(process_name):
        cmd = 'TASKLIST', '/FI', 'imagename eq %s' % process_name
        proc = subprocess.Popen(cmd, stdout=subprocess.PIPE)

        out = proc.communicate()[0].strip().split('\r\n')

        if len(out) > 1 and process_name[:10] in out[-1]:
            print('process "%s" is running!' % process_name)
            return True
        else:
            print('stdout: %s' % out[0])
            print('process "%s" is NOT running!' % process_name)
            return False

    @staticmethod
    def check_service_state():
        timer = Timer(300, None)
        timer.start()
        for p in VAST_SERVICES:
            is_running = Utility.process_exists(p)
            while not is_running:
                if timer.is_alive():
                    sleep(2)
                    is_running = Utility.process_exists(p)
                else:
                    raise Exception("'%s' is not ready!" % p)

        is_running = Utility.process_exists("PluginServer.exe")
        while not is_running:
            if timer.is_alive():
                sleep(2)
                is_running = Utility.process_exists(p)
            else:
                raise Exception("'%s' is not ready!" % p)
        timer.cancel()

    @staticmethod
    def rmtree_and_copytree(src, des):
        if os.path.exists(des):
            try:
                shutil.rmtree(des)
            except Exception as e:
                raise Exception("Remove path: '%s' Failed!!" % des)
        shutil.copytree(src, des)

    @staticmethod
    def backup_vast_settings():
        install_path = Utility.get_vast_install_path()

        gaea_db_src = os.path.join(install_path, 'Server', 'GaeaDB')
        gaea_db_des = os.path.join(VAST_SETTING_BACKUP_PATH, 'GaeaDB')
        distutils.dir_util.copy_tree(gaea_db_src, gaea_db_des)

        sqlite_src = os.path.join(install_path, 'sqlite')
        sqlite_des = os.path.join(VAST_SETTING_BACKUP_PATH, 'sqlite')
        distutils.dir_util.copy_tree(sqlite_src, sqlite_des)

        hint_src = os.path.join(install_path, 'Server', 'Hint.dat')
        hint_dst = os.path.join(VAST_SETTING_BACKUP_PATH, 'Hint.dat')
        shutil.copyfile(hint_src, hint_dst)

        pwd_src = os.path.join(install_path, 'Server', 'PWD.dat')
        pwd_dst = os.path.join(VAST_SETTING_BACKUP_PATH, 'PWD.dat')
        shutil.copyfile(pwd_src, pwd_dst)

    @staticmethod
    def restore_vast_to_default_settings():
        Utility.stop_vast_services()
        sleep(5)

        rec_path = ''.join(DEFAULT_RECORDING_PATH).split()
        for path in rec_path:
            if os.path.exists(path):
                try:
                    shutil.rmtree(path)
                except:
                    raise Exception("Error: Remove path '%s' Failed!!" % path)
            print("Remove path : %s" % path)
            os.mkdir(path)

        install_path = Utility.get_vast_install_path()
        gaea_db_src = os.path.join(VAST_SETTING_BACKUP_PATH, 'GaeaDB')
        gaea_db_des = os.path.join(install_path, 'Server', 'GaeaDB')
        Utility.rmtree_and_copytree(gaea_db_src, gaea_db_des)

        sqlite_src = os.path.join(VAST_SETTING_BACKUP_PATH, 'sqlite')
        sqlite_des = os.path.join(install_path, 'sqlite')
        Utility.rmtree_and_copytree(sqlite_src, sqlite_des)

        hint_src = os.path.join(VAST_SETTING_BACKUP_PATH, 'Hint.dat')
        hint_des = os.path.join(install_path, 'Server', 'Hint.dat')
        shutil.copyfile(hint_src, hint_des)

        pwd_src = os.path.join(VAST_SETTING_BACKUP_PATH, 'PWD.dat')
        pwd_des = os.path.join(install_path, 'Server', 'PWD.dat')
        shutil.copyfile(pwd_src, pwd_des)

        Utility.start_vast_services()
        sleep(10)


if __name__ == '__main__':
    if sys.argv[1] == 'backup':
        Utility.backup_vast_settings()
    elif sys.argv[1] == 'restore':
        Utility.restore_vast_to_default_settings()
    elif os.path.exists(sys.argv[1]):
        Utility.install_vast(sys.argv[1])