__author__ = 'sunny.sun'

DEBUG = True

from robot.output import librarylogger


def debug_log(msg, level, html=False, also_console=True):
	if DEBUG:
		librarylogger.write(msg, level, html)
		if also_console:
			librarylogger.console(msg)